import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { NgModule,CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LocationStrategy, HashLocationStrategy, NgSwitch } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { AppComponent } from './app.component';
import { routing } from './routing';
import { ChartistModule } from 'ng-chartist';
import { Angular2CsvModule } from 'angular2-csv';


import { loginComponent } from './pages/login/login.component';
import { dashboardSheardComponent } from './pages/dashboardSheard/dashboardSheard.component';

////// services //////////
// import {OrdersService} from './services/ordersService';
import { AuthService } from './services/auth.service';
// import {SharedModule} from './modules/shared/shared.module';
import { TranslationModule } from './modules/translation.module';
import { WebService } from './services/webService';
import { UtilitiesService } from './services/utilities.service';
import { SharedService } from './services/shared.service';
import { SharedModule } from './modules/shared/shared.module';
import { SystemAccountsService } from './services/system-accounts.service';
// import {CustomerInfoComponent} from './pages/accounts/customer-info/customer-info.component';

// import {PromptDialogComponent} from './modules/shared/prompt-dialog/prompt-dialog.component';



@NgModule({
  declarations: [
    AppComponent, loginComponent, dashboardSheardComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    Angular2CsvModule,
    routing,
    HttpModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    ChartistModule,
    // MatTableModule,
    SharedModule,
    TranslationModule
  ],
  schemas:  [ CUSTOM_ELEMENTS_SCHEMA ],
  providers: [{
    provide: LocationStrategy,
    useClass: HashLocationStrategy
  }, AuthService, WebService, UtilitiesService, SharedService, SystemAccountsService],
  bootstrap: [AppComponent]
})
export class AppModule {
}

