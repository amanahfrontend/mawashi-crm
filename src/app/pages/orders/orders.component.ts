import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Params} from '@angular/router';
import {OrdersFilterService} from '../../services/orders-filter.service';
import {SharedService} from '../../services/shared.service';

// import {PageEvent} from '@angular/material';
// import { Angular2Csv } from 'angular2-csv/Angular2-csv';


@Component({
  moduleId: module.id,
  selector: 'app-orders',
  styleUrls: ['./orders.component.css'],
  templateUrl: 'orders.component.html'
})

export class ordersComponent implements OnInit {
  type: string;
  // typeApp: boolean;
  // typeSales: boolean;
  filteredOrders: any[] = [];
  allOrders: any[] = [];
  pagination: any = {};
  // userCountryId: number;
  // filterObjView: any = {};
  // filterObj: any;
  // filterResetObj: any;
  // paymentTypes: any[];
  // deliveryTypes: any[];
  // allOrderStatus: any[];
  pageSize: number;
  count: number;
  pageNumber: number = 0;
  currentFilterObj: any;

  // pageEvent: PageEvent;

  constructor(private activatedRoute: ActivatedRoute, private ordersFilterService: OrdersFilterService, private sharedService: SharedService) {
  }

  ngOnInit() {
    this.activatedRoute.params
      .subscribe((params: Params) => {
        this.type = params.type;
      });
  }

  pageEvent(e) {
    console.log(e);
    this.currentFilterObj.pageInfo.pageNo = ++e.pageIndex;
    this.getFilterOrders(this.currentFilterObj);
  }

  resetFilterViewValues(filterObj) {
    // this.filterObjView = {};
    this.getFilterOrders(filterObj);
  }

 /* exportOrders() {
    let exportData = [];
    exportData.push({
      'Order Code': 'Order Code',
      'Delivery Type': 'Delivery Type',
      'Delivery Date': 'Delivery Date',
      'Payment Message': 'Payment Message',
      'Platform': 'Platform',
      'Price': 'Price',
    });
    this.filteredOrders.map((item) => {
      exportData.push({
        'Order Code': item.code,
        'Delivery Type': item.deliveryType,
        'Delivery Date': new Date(item.deliveryDate).toDateString(),
        'Payment Message': item.paymentMessage,
        'Platform': item.platform || '-',
        'Price': item.price
      });
    });
    return new Angular2Csv(exportData, 'Order Report', {
      showLabels: true
    });
  }*/

  getFilterOrders(filterObj) {
    console.log(filterObj);
    this.pageNumber = 0;
    this.currentFilterObj = filterObj;
    console.log("Log this filter");
    console.log(filterObj);
    this.ordersFilterService.filterOrders(filterObj)
      .subscribe((res) => {
        console.log(res);
        this.allOrders = res.data.data;
        this.pageSize = res.data.pageSize;
        this.count = res.data.count;
        console.log(this.allOrders);
        this.filteredOrders = this.allOrders.slice();
      });
  }


}


