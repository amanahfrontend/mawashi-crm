import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Params} from '@angular/router';
import {OrdersService} from '../../../services/ordersService';
import {Location} from '@angular/common';
import {AuthService} from '../../../services/auth.service';
import {AccountsService} from '../../../services/accounts.service';
import {SharedService} from '../../../services/shared.service';

@Component({
  selector: 'app-order-details',
  templateUrl: './order-details.component.html',
  styleUrls: ['./order-details.component.scss']
})
export class OrderDetailsComponent implements OnInit {
  items: any[];
  price: number = 0;
  currency: number;
  orderDetails: any;
  customerDetails: any;
  governorate: any;
  deliveryNote: any;
  deliveryDate: any;
  createdDate: any;
  createdUserName: any;
  type: string;
  typeApp: boolean;
  typeSales: boolean;

  constructor(private activatedRoute: ActivatedRoute, private orderService: OrdersService, private locationService: Location, private authService: AuthService, private sharedService: SharedService) {
  }

  ngOnInit() {
    this.currency = this.authService.currentUserData().currency;
    this.activatedRoute.params
      .subscribe((params: Params) => {
          console.log(params.cartId);
          this.type = params.type;
          this.typeApp = this.type == 'app';
          this.typeSales = this.type == 'sales';
          this.orderService.getOrderDetailsByCartId(params.cartId)
            .subscribe((res) => {
                console.log(res.data);
                this.orderDetails = res.data;
                this.items = this.orderDetails.cartItems;
                this.price = this.orderDetails.price;
                this.orderService.getOrdersDetailsById(res.data.fK_Order_Id)
                  .subscribe((res) => {
                    console.log(res);
                    this.governorate = res.data.governorateName;
                    this.deliveryNote = res.data.deliveryNote;
                    this.deliveryDate = res.data.deliveryDate;
                    this.createdDate = res.data.createdDate;
                    this.createdUserName = res.data.createdUserName;
                  });
                if (this.typeApp) {
                  this.sharedService.getAccountDetails(this.orderDetails.fK_Customer_Id)
                    .subscribe((res) => {
                      console.log(res);
                      this.customerDetails = res.data;
                    });
                } else if (this.typeSales) {
                  this.sharedService.getCompanyDetailsById(this.orderDetails.fK_Customer_Id)
                    .subscribe((res) => {
                      console.log(res);
                      this.customerDetails = res.data;
                    });
                }
              },
              err => {
                alert('error in server');
              });
        },
        err => {

        });
  }

  back() {
    this.locationService.back();
  }

}
