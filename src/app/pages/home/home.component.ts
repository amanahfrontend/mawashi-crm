import {Component, OnDestroy, OnInit} from '@angular/core';
// import {FormsModule} from '@angular/forms';
// import {ActivatedRoute, Router} from '@angular/router';
// import * as $ from 'jquery';
import * as Chartist from 'chartist';
import {HomeService} from '../../services/home.service';
import {Subscription} from 'rxjs/Subscription';
import {OrdersService} from '../../services/ordersService';
import {SharedService} from '../../services/shared.service';
import {MatDialog, MatDialogRef} from '@angular/material';
import {ChangeStatusModalComponent} from '../../modules/change-status-modal/change-status-modal.component';


@Component({
  moduleId: module.id,
  selector: 'app-home',
  templateUrl: 'home.component.html'
})

export class homeComponent implements OnInit, OnDestroy {
  states: any = {};
  customersCount: number;

  getAppReviewStateSubscription: Subscription;
  getCustomersCountSubscription: Subscription;
  getTodayOrderStateSubscription: Subscription;
  usersReviewChartLabels = [];
  usersReviewChartSeries = [];
  latestOrders: any[] = [];
  dailyOrders: any[] = [];
  admin: boolean;
  superAdmin: boolean;
  customerService: boolean;
  logistics: boolean;
  sales: boolean;
  accountant: boolean;
  guest: boolean;
  production: boolean;
  getOrderByRoleSubscription: Subscription;
  currentUserRoles: string;
  filterTerms: any = {};
  totalOrdersCount: number = 0;

  constructor(private homeService: HomeService, private orderService: OrdersService, private sharedService: SharedService) {
  }

  startAnimationForLineChart(chart) {
    let seq: any, delays: any, durations: any;
    seq = 0;
    delays = 80;
    durations = 500;
    console.log(chart);
    chart.on('draw', function (data) {
      console.log(data.type);
      if (data.type === 'line' || data.type === 'area') {
        console.log(data);
        data.element.animate({
          d: {
            begin: 600,
            dur: 700,
            from: data.path.clone().scale(1, 0).translate(0, data.chartRect.height()).stringify(),
            to: data.path.clone().stringify(),
            easing: Chartist.Svg.Easing.easeOutQuint
          }
        });
      } else if (data.type === 'point') {
        seq++;
        console.log(data);
        data.element.animate({
          opacity: {
            begin: seq * delays,
            dur: durations,
            from: 0,
            to: 1,
            easing: 'ease'
          }
        });
      }
    });

    seq = 0;
  };

  startAnimationForBarChart(chart) {
    let seq2: any, delays2: any, durations2: any;
    console.log(chart);
    seq2 = 0;
    delays2 = 80;
    durations2 = 500;
    chart.on('draw', function (data) {
      if (data.type === 'bar') {
        seq2++;
        data.element.animate({
          opacity: {
            begin: seq2 * delays2,
            dur: durations2,
            from: 0,
            to: 1,
            easing: 'ease'
          }
        });
      }
    });

    seq2 = 0;
  };

  getTodayOrderState() {
    this.getTodayOrderStateSubscription = this.homeService.getTodayOrderState()
      .subscribe((res) => {
        // console.log(res.data);
        this.states = res.data;
      });
  }

  getCustomersCount() {
    this.getCustomersCountSubscription = this.homeService.getCustomersCount()
      .subscribe((res) => {
        console.log(res.data);
        let currentUserCountry = this.sharedService.currentUserData().fk_Country_Id;
        res.data.map((data) => {
          console.log(data.country);
          console.log(currentUserCountry);
          if (currentUserCountry == data.country) {
            this.customersCount = data.customerCount;
          }
        });
      });
  }

  getAppReviewState() {
    return new Promise((resolve, reject) => {
      this.getAppReviewStateSubscription = this.homeService.getAppReviewState()
        .subscribe((res) => {
          let highestValue = 0;
          console.log(res.data);
          let reviews = res.data;
          reviews.map((review) => {
            if (highestValue < review.count) {
              highestValue = review.count;
            }
            if (review.name) {
              this.usersReviewChartLabels.push(review.name);
              this.usersReviewChartSeries.push(review.count);
            }
          });
          resolve(highestValue);
        });
    });
  }

  getAllOrders() {
    this.orderService.getAllOrders(this.filterTerms)
      .subscribe((res) => {
        console.log(res.data);
        this.totalOrdersCount = res.data.count;
        this.latestOrders = res.data.data;
      });
  }

  getLastSevenDaysOrders() {
    this.homeService.getLastSevenDaysOrders()
      .subscribe((res) => {
        this.dailyOrders = res.data;
        console.log(this.dailyOrders);
        let highestOrdersCount = 0,
          dailyOrdersLabels = [], dailyOrdersSeries = [];
        this.dailyOrders
          .map((day) => {
            if (day.ordersCount > highestOrdersCount) {
              highestOrdersCount = day.ordersCount;
            }
            console.log(new Date(day.day).getDate() + '/' + new Date(day.day).getMonth());
            dailyOrdersLabels.push(new Date(day.day).getDate() + '/' + (new Date(day.day).getMonth() + 1));
            dailyOrdersSeries.push(day.ordersCount);
          });
        console.log(highestOrdersCount);
        var optionswebsiteViewsChart = {
          axisX: {
            showGrid: false
          },
          low: 0,
          high: highestOrdersCount + 10,
          chartPadding: {top: 0, right: 5, bottom: 0, left: 0}
        };

        var responsiveOptions: any[] = [
          ['screen and (max-width: 640px)', {
            seriesBarDistance: 5,
            axisX: {
              labelInterpolationFnc: function (value) {
                return value[0];
              }
            }
          }]
        ];

        var dataDailyOrdersChart = {
          labels: dailyOrdersLabels,
          series: [
            dailyOrdersSeries
          ]
        };

        var websiteViewsChart = new Chartist.Bar('#dailySalesChart', dataDailyOrdersChart, optionswebsiteViewsChart, responsiveOptions);

        //start animation for the Emails Subscription Chart
        this.startAnimationForBarChart(websiteViewsChart);

      });
  }

  getOrderByRole(role) {
    console.log(role);
    if (role.toLowerCase() == 'driver') {
      this.homeService.getDriverOrders(this.filterTerms)
        .subscribe((res) => {
          console.log(res);
          this.totalOrdersCount += res.data.count;
          this.latestOrders = this.latestOrders.concat(res.data.data);
          console.log(this.latestOrders);
        });
      this.homeService.getDriverSalesOrders(this.filterTerms)
        .subscribe((res) => {
          console.log(res);
          this.totalOrdersCount += res.data.count;
          this.latestOrders = this.latestOrders.concat(res.data.data);
          console.log(this.latestOrders);
        });
    } else {
      this.homeService.getOrderByRole(role, this.filterTerms)
        .subscribe((res) => {
          console.log(res);
          this.totalOrdersCount = res.data.count;
          this.latestOrders = res.data.data;
        });
    }
  }

  filterOrders(filterTerms) {
    this.filterTerms = filterTerms;
    console.log(this.filterTerms);
    this.getOrders();
  }

  navigatePages(pageData) {
    this.filterTerms.pageInfo.pageNo = pageData.pageNo;
    // this.filterTerms.pageSize = pageData.pageSize;
    console.log(this.filterTerms);
    console.log(pageData);
    this.getOrders();
  }

  getOrders() {
    /******* get view data ********/
    if (this.admin || this.superAdmin) {
      this.getAllOrders();
    } else {
      this.getOrderByRole(this.currentUserRoles[0]);
    }
  }

  ngOnInit() {

    /******************************/
    // roles and permissions
    /******************************/

    this.currentUserRoles = this.sharedService.currentUserData().roles;

    this.admin = this.currentUserRoles.includes('Admin');
    this.superAdmin = this.currentUserRoles.includes('Superadmin');
    this.customerService = this.currentUserRoles.includes('CustomerService');
    this.accountant = this.currentUserRoles.includes('Accountant');
    this.sales = this.currentUserRoles.includes('Sales');
    this.guest = this.currentUserRoles.includes('Guest');
    this.production = this.currentUserRoles.includes('Production');
    this.logistics = this.currentUserRoles.includes('Logistics');


    this.getOrders();

    this.getTodayOrderState();
    this.getCustomersCount();
    this.getLastSevenDaysOrders();
    this.getAppReviewState()
      .then((highestCount: any) => {
        var optionswebsiteViewsChart = {
          axisX: {
            showGrid: false
          },
          low: 0,
          high: highestCount + 10,
          chartPadding: {top: 0, right: 5, bottom: 0, left: 0}
        };

        var responsiveOptions: any[] = [
          ['screen and (max-width: 640px)', {
            seriesBarDistance: 5,
            axisX: {
              labelInterpolationFnc: function (value) {
                return value[0];
              }
            }
          }]
        ];

        var datawebsiteViewsChart = {
          labels: this.usersReviewChartLabels,
          series: [
            this.usersReviewChartSeries
          ]
        };

        var websiteViewsChart = new Chartist.Bar('#websiteViewsChart', datawebsiteViewsChart, optionswebsiteViewsChart, responsiveOptions);

        //start animation for the Emails Subscription Chart
        this.startAnimationForBarChart(websiteViewsChart);

      });


    // /*****************************/
    // const dataDailySalesChart: any = {
    //   labels: ['M', 'T', 'W', 'T', 'F', 'S', 'S'],
    //   series: [
    //     [12, 17, 7, 17, 23, 18, 38]
    //   ]
    // };
    //
    // const optionsDailySalesChart: any = {
    //   lineSmooth: Chartist.Interpolation.cardinal({
    //     tension: 0
    //   }),
    //   low: 0,
    //   high: 50, // creative tim: we recommend you to set the high sa the biggest value + something for a better look
    //   chartPadding: {top: 0, right: 0, bottom: 0, left: 0},
    // };
    //
    // var dailySalesChart = new Chartist.Line('#dailySalesChart', dataDailySalesChart, optionsDailySalesChart);
    //
    // this.startAnimationForLineChart(dailySalesChart);

    //
    // /* ----------==========     Completed Tasks Chart initialization    ==========---------- */
    //
    // const dataCompletedTasksChart: any = {
    //   labels: ['12am', '3pm', '6pm', '9pm', '12pm', '3am', '6am', '9am'],
    //   series: [
    //     [230, 750, 450, 300, 280, 240, 200, 190]
    //   ]
    // };
    //
    // const optionsCompletedTasksChart: any = {
    //   lineSmooth: Chartist.Interpolation.cardinal({
    //     tension: 0
    //   }),
    //   low: 0,
    //   high: 1000, // creative tim: we recommend you to set the high sa the biggest value + something for a better look
    //   chartPadding: {top: 0, right: 0, bottom: 0, left: 0}
    // };
    //
    // var completedTasksChart = new Chartist.Line('#completedTasksChart', dataCompletedTasksChart, optionsCompletedTasksChart);
    //
    // // start animation for the Completed Tasks Chart - Line Chart
    // this.startAnimationForLineChart(completedTasksChart);
    //

    /* ----------==========     Website Subscription Chart initialization    ==========---------- */

  }

  ngOnDestroy() {
    this.getAppReviewStateSubscription && this.getAppReviewStateSubscription.unsubscribe();
    this.getCustomersCountSubscription && this.getCustomersCountSubscription.unsubscribe();
    this.getTodayOrderStateSubscription && this.getTodayOrderStateSubscription.unsubscribe();
    this.getOrderByRoleSubscription && this.getOrderByRoleSubscription.unsubscribe();
  }

}


