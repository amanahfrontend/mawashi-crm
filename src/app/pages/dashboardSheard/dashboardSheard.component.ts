import { Component, OnInit, ViewChild, AfterViewInit, DoCheck } from '@angular/core';
import { Location, PopStateEvent } from '@angular/common';
import 'rxjs/add/operator/filter';
import { Router, NavigationEnd, NavigationStart, ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import PerfectScrollbar from 'perfect-scrollbar';
import { TranslateService } from '@ngx-translate/core';
import { AuthService } from '../../services/auth.service';
import { SharedService } from '../../services/shared.service';
import { ResetPasswordModalComponent } from '../../modules/system-accounts/reset-password-modal/reset-password-modal.component';
import { MatDialog } from '@angular/material';

declare const $: any;

@Component({
  selector: 'app-dashboardSheard',
  templateUrl: './dashboardSheard.component.html',
})

export class dashboardSheardComponent implements OnInit, DoCheck {
  private _router: Subscription;
  private lastPoppedUrl: string;
  private yScrollStack: number[] = [];
  toggleSideNav: boolean;
  isCustomerServiceRole: boolean;
  isAccountantRole: boolean;
  isProducerRole: boolean;
  isLogistecsRole: boolean;
  isAdminRole: boolean;
  isSuperAdminRole: boolean;
  isSalesRole: boolean;
  isGuestRole: boolean;

  constructor(public location: Location, private router: Router, private translate: TranslateService, private route: ActivatedRoute, private _authService: AuthService, private sharedService: SharedService, private dialog: MatDialog) {
    translate.addLangs(['en', 'ar']);
    translate.setDefaultLang('en');
    translate.use(localStorage.getItem('lang'));
  }

  isAr: boolean = false;
  userData: any = {};

  ngOnInit() {
    this.setUserRole();

    this.switchLanguage(localStorage.getItem('lang'));

    this._authService.isAuth.subscribe((value) => {
      // console.log(value);
      if (value) {
        this.userData = this._authService.currentUserData();
        // this.router.navigate(['/mawashi-crm/home']);
      }
    });

    const elemMainPanel = <HTMLElement>document.querySelector('.main-panel');
    const elemSidebar = <HTMLElement>document.querySelector('.sidebar .sidebar-wrapper');
    // console.log(elemMainPanel, elemSidebar);
    this.location.subscribe((ev: PopStateEvent) => {
      this.lastPoppedUrl = ev.url;
    });
    this.router.events.subscribe((event: any) => {
      if (event instanceof NavigationStart) {
        if (event.url != this.lastPoppedUrl) {
          this.yScrollStack.push(window.scrollY);
        }
      } else if (event instanceof NavigationEnd) {
        if (event.url == this.lastPoppedUrl) {
          this.lastPoppedUrl = undefined;
          window.scrollTo(0, this.yScrollStack.pop());
        } else {
          window.scrollTo(0, 0);
        }
      }
    });
    this._router = this.router.events.filter(event => event instanceof NavigationEnd).subscribe((event: NavigationEnd) => {
      // if (elemMainPanel && elemSidebar) {
      elemMainPanel.scrollTop = 0;
      elemSidebar.scrollTop = 0;
      // }
    });
    if (window.matchMedia(`(min-width: 960px)`).matches && !this.isMac()) {
      if (elemMainPanel && elemSidebar) {
        let ps = new PerfectScrollbar(elemMainPanel);
        ps = new PerfectScrollbar(elemSidebar);
      }
    }
  }


  setUserRole() {
    // this.sharedService.currentUserRole
    //   .subscribe(() => {
    this.sharedService.currentUserRole
      .subscribe(() => {
        console.log(this.sharedService.isLogistecsRole);
        this.isCustomerServiceRole = this.sharedService.isCustomerServiceRole;
        this.isAccountantRole = this.sharedService.isAccountantRole;
        this.isProducerRole = this.sharedService.isProducerRole;
        this.isLogistecsRole = this.sharedService.isLogistecsRole;
        this.isAdminRole = this.sharedService.isAdminRole;
        this.isSuperAdminRole = this.sharedService.isSuperAdminRole;
        this.isSalesRole = this.sharedService.isSalesRole;
        this.isGuestRole = this.sharedService.isGuestRole;
      });
    // });
  }

  switchLanguage(lang: string) {
    if (lang == 'ar') {
      this.isAr = true;
      localStorage.setItem('lang', 'ar');
      this.translate.use('ar');
      $('body').addClass('rtl-active');
      $('#theme-stylesheet').after('<link id="styleAr" rel="stylesheet" href="./assets/css/bootstrap-rtl.css" type="text/css" />');
    }
    else {
      localStorage.setItem('lang', 'en');
      this.translate.use('en');
      $('body').removeClass('rtl-active');
      $('#styleAr').remove();
      this.isAr = false;
    }
  }

  ngDoCheck() {
    this.toggleSideNav = this._authService.isAuth.value;
  }

  openChangePasswordModal() {
    let dialogRef = this.dialog.open(ResetPasswordModalComponent, {
      data: this.sharedService.currentUserData(),
      width: '500px',
      height: '250px'
    });
    dialogRef.afterClosed()
      .subscribe((result) => {

      });

  }

  logOut() {
    this._authService.logOut();
  }

  ngAfterViewInit() {
    this.runOnRouteChange();
  }

  isMaps(path) {
    var titlee = this.location.prepareExternalUrl(this.location.path());
    titlee = titlee.slice(1);
    if (path == titlee) {
      return false;
    }
    else {
      return true;
    }
  }

  runOnRouteChange(): void {
    if (window.matchMedia(`(min-width: 960px)`).matches && !this.isMac()) {
      const elemMainPanel = <HTMLElement>document.querySelector('.main-panel');
      // if (elemMainPanel) {
      const ps = new PerfectScrollbar(elemMainPanel);
      ps.update();
      // }
    }
  }

  isMac(): boolean {
    let bool = false;
    if (navigator.platform.toUpperCase().indexOf('MAC') >= 0 || navigator.platform.toUpperCase().indexOf('IPAD') >= 0) {
      bool = true;
    }
    return bool;
  }
}
