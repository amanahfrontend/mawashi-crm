import {Component, OnInit} from '@angular/core';
// import {FormsModule} from '@angular/forms';
import {Router} from '@angular/router';
import {AuthService} from '../../services/auth.service';
import {SharedService} from '../../services/shared.service';

// import * as $ from 'jquery';


@Component({
  moduleId: module.id,
  selector: 'app-login',
  templateUrl: 'login.component.html',
  styleUrls: ['./login.component.css']
})

export class loginComponent implements OnInit {
  logInData: any = {};

  constructor(private router: Router, private authService: AuthService, private sharedService: SharedService) {
  }

  ngOnInit() {
     this.authService.logOut();
  }

  logIn() {
    this.authService.logIn(this.logInData)
      .then(() => {
        this.sharedService.setCurrentUserRole();
        this.router.navigate(['/mawashi-crm/home']);
      });
  }
}


