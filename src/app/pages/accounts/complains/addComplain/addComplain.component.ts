import {Component, OnInit, Inject} from '@angular/core';
// import {FormsModule} from '@angular/forms';
// import {ActivatedRoute, Router} from '@angular/router';
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import {AccountsService} from '../../../../services/accounts.service';


@Component({
  moduleId: module.id,
  selector: 'app-addComplain',
  templateUrl: 'addComplain.component.html'
})

export class AddComplainComponent implements OnInit {
  complainData: any = {};
  ratingScale: any[] = [];
  showError: boolean;
  
  constructor(public dialogRef: MatDialogRef<AddComplainComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any, private accountService: AccountsService) {
  }
  
  ngOnInit() {
    console.log(this.data);
    this.accountService.getRatingScale()
      .subscribe((res) => {
          console.log(res);
          this.ratingScale = res.data;
        },
        err => {
        
        });
  }
  
  closeModal() {
    console.log('close');
    this.dialogRef.close();
  }
  
  postComplain() {
    this.complainData.fK_Reviewer_Id = this.data.accountId;
    this.accountService.postComplain(this.complainData)
      .subscribe(() => {
          this.closeModal();
        },
        err => {
          this.showError = true;
        });
  }
  
}


