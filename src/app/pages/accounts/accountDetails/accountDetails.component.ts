import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {AccountsService} from '../../../services/accounts.service';
import {CreateCustomerFormComponent} from '../../../modules/accounts/create-customer-form/create-customer-form.component';


@Component({
  moduleId: module.id,
  selector: 'app-accountDetails',
  templateUrl: 'accountDetails.component.html',
  styleUrls: ['accountDetails.component.scss']
})

export class accountDetailsComponent implements OnInit {
  accountId: any;
  accountDetails: any;
  type: string;
  typeApp: boolean;
  typeSales: boolean;
  
  // @ViewChild(CreateCustomerFormComponent) createCustomerFormComponent: CreateCustomerFormComponent;
  
  constructor(private activatedRoute: ActivatedRoute, private accountsService: AccountsService) {
  }
  
  ngOnInit() {
    this.activatedRoute.params.subscribe((param: Params) => {
      this.accountId = +param.accountId;
      this.type = param.type;
      this.typeApp = this.type == 'app';
      this.typeSales = this.type == 'sales';
      this.getAccountDetails();
    });
  }
  
  getAccountDetails() {
    console.log(this.type);
    if (this.typeApp) {
      this.accountsService.getAccountDetails(this.accountId)
        .subscribe((res) => {
            this.accountDetails = res.data;
            // console.log(this.accountDetails);
          },
          err => {
          
          });
    } else if (this.typeSales) {
      this.accountsService.getCompanyDetailsById(this.accountId)
        .subscribe((res) => {
            this.accountDetails = res.data;
            // console.log(this.accountDetails);
          },
          err => {
          
          });
    }
  }
  
}


