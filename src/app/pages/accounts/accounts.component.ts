import { Component, OnDestroy, OnInit, ViewContainerRef } from "@angular/core";
import { MatDialog } from "@angular/material";
// import {FormsModule} from '@angular/forms';
import { ActivatedRoute, Params, Router } from "@angular/router";
// import {Subscribable} from 'rxjs/Observable';
import { Subscription } from "rxjs/Subscription";
import { ForgetPasswordModalComponent } from "../../modules/accounts/forget-password-modal/forget-password-modal.component";
// import {AddComplainComponent} from '../accounts/complains/addComplain/addComplain.component';
// import * as $ from 'jquery';
// import {ordersService} from '../../services/ordersService';
import { AccountsService } from "../../services/accounts.service";
import { SharedService } from "../../services/shared.service";
//import { ToastsManager } from 'ng2-toastr';


@Component({
  moduleId: module.id,
  selector: "app-accounts",
  styleUrls: ["accounts.component.css"],
  templateUrl: "accounts.component.html"
})
export class accountsComponent implements OnInit, OnDestroy {

  // complainsDialog: MatDialogRef<complainsComponent>;
  // addComplainDialog: MatDialogRef<AddComplainComponent>;
  orders: any = [];
  phoneNumber: number;
  toggleAddCustomer: boolean = false;
  toggleLoading: boolean;
  customers: any[] = [];
  getAllCustomersSubscription: Subscription;
  searchPhoneNumberSubscription: Subscription;
  type: string;
  typeSales: boolean;
  typeApp: boolean;
  itemToDeleteId: string;

  constructor(private dialog: MatDialog, private router: Router, private accountsService: AccountsService, private activatedRoute: ActivatedRoute, private sharedService: SharedService, vcr: ViewContainerRef) {
    //this.toastr.setRootViewContainerRef(vcr);

  }

  ngOnInit() {
    this.activatedRoute.params
      .subscribe((params: Params) => {
        this.type = params.type;
        if (params.type == "app") {
          this.typeApp = true;
          this.typeSales = false;
        } else if (params.type == "sales") {
          this.typeSales = true;
          this.typeApp = false;
        }
      });
    //this.getAllCustomers();
  }

  ngOnDestroy() {
    this.getAllCustomersSubscription && this.getAllCustomersSubscription.unsubscribe();
    this.searchPhoneNumberSubscription && this.searchPhoneNumberSubscription.unsubscribe();
  }

  getAllCustomers() {
    this.toggleLoading = true;
    this.toggleAddCustomer = false;
    let countryId = this.sharedService.currentUserData().fk_Country_Id;
    console.log(countryId);
    if (this.typeApp) {
      this.getAllCustomersSubscription = this.accountsService.getAllCustomersByCountryId(countryId)
        .subscribe((res) => {
          if (res.data) {
            console.log(res.data);
            this.customers = res.data;
          }
          this.toggleLoading = false;
        });
    } else if (this.typeSales) {
      this.getAllCustomersSubscription = this.accountsService.getAllCompanyByCountryId(countryId)
        .subscribe((res) => {
          if (res.data) {
            console.log(res.data);
            this.customers = res.data;
          }
          this.toggleLoading = false;
        });
    }
  }

  viewDetails(customerId) {
    if (this.typeApp) {
      this.router.navigate(["mawashi-crm/accounts/" + customerId, { type: "app" }]);
    } else if (this.typeSales) {
      this.router.navigate(["mawashi-crm/companies-accounts/" + customerId, { type: "sales" }]);
    }
  }

  deleteCompany(companyId) {
    this.toggleLoading = true;
    if (companyId > 0) {
      this.accountsService.deleteCompanyById(companyId)
        .subscribe((res) => {
          if (res.data) {
          }
          this.toggleLoading = false;
        });
    }
  }

  selectItemToDelete(id): void {
    this.itemToDeleteId = id;
  }

  deleteSelectedItem(): void {
    let thisComponent = this;
    if (this.itemToDeleteId == "") {
      //thisComponent.toastr.warning("No Item Selected", '');
      return;
    }

    this.accountsService.deleteCompanyById(thisComponent.itemToDeleteId).subscribe(
      function (response) {
        ////thisComponent.toastr.success("Deleted Successfully", '');

        //  delete object from collection
        let selectedObject = thisComponent.customers.find(o => o.id == thisComponent.itemToDeleteId);
        let index = thisComponent.customers.indexOf(selectedObject);
        if (index > -1)
          thisComponent.customers.splice(index, 1);
      },
      function (error) {
        //thisComponent.toastr.error(error, '');

      },
      function () {

      });
  }

  searchPhoneNumber() {
    this.toggleLoading = true;

    if (this.typeApp) {
      this.searchPhoneNumberSubscription = this.accountsService.searchAccount(this.phoneNumber).subscribe(
        (res) => {
          console.log(res);
          if (res.data) {
            this.customers = res.data;
          } else if (!res.data || !res.data.length) {
            this.toggleAddCustomer = true;
          }
          this.toggleLoading = false;
        });
    } else if (this.typeSales) {
      let currentUserCountry = this.sharedService.currentUserData().country_Code;
      if (currentUserCountry.toLowerCase() === "uae") {

        this.accountsService.searchUAECompany(this.phoneNumber)
          .subscribe((res) => {
            // console.log(res);
            if (res.data) {
              this.customers = res.data;
            } else {
              this.toggleAddCustomer = true;
            }
            this.toggleLoading = false;
          });

      } else if (currentUserCountry.toLowerCase() == "kuwait") {
        this.accountsService.searchKWCompany(this.phoneNumber)
          .subscribe((res) => {
            // console.log(res);
            if (res.data) {
              this.customers = res.data;
            } else {
              this.toggleAddCustomer = true;
            }
            this.toggleLoading = false;
          });
      }
    }
  }

  forgetPassword(userName) {
    let dialogRef = this.dialog.open(ForgetPasswordModalComponent, {
      data: { userName: userName },
      closeOnNavigation: true,
      disableClose: true,
      minWidth: "500px"
    });

  }

}


