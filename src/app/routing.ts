import {RouterModule, Routes} from '@angular/router';
import {ModuleWithProviders} from '@angular/core';

import {loginComponent} from './pages/login/login.component';
import {ContactUsComponent} from './modules/shared/contact-us/contact-us.component';
import {dashboardSheardComponent} from './pages/dashboardSheard/dashboardSheard.component';

export const appRoutes: Routes = [
  {path: 'login', component: loginComponent},
  {
    path: 'mawashi-crm', component: dashboardSheardComponent, children: [
      {path: 'system-accounts', loadChildren: './modules/system-accounts/system-accounts.module#SystemAccountsModule'},
      {path: 'home', loadChildren: './modules/home.module#HomeModule'},
      {path: 'accounts', loadChildren: './modules/accounts/accounts.module#AccountsModule'},
      {path: 'companies-accounts', loadChildren: './modules/accounts/accounts.module#AccountsModule'},
      {path: 'orders-app', loadChildren: './modules/orders.module#OrdersModule'},
      {path: 'orders-sales', loadChildren: './modules/orders.module#OrdersModule'},
      {path: 'stock', loadChildren: './modules/stock/stock.module#StockModule'},
      {path: 'administrations', loadChildren: './modules/administrations/administrations.module#AdministrationsModule'},
      {path: 'complains', loadChildren: './modules/complains/complains.module#ComplainsModule'},
      {path: 'invoices', loadChildren: './modules/invoices/invoices.module#InvoicesModule'},
      {path: 'announcement', loadChildren: './modules/announcement/announcement.module#AnnouncementModule'},

      {path: '', redirectTo: 'login', pathMatch: 'full'}
    ]
  },
  {path: '', redirectTo: 'mawashi-crm/home', pathMatch: 'full'}
];

export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);

