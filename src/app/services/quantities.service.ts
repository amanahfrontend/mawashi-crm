import {Injectable} from '@angular/core';
import {UtilitiesService} from './utilities.service';
import {WebService} from './webService';
import {Http} from '@angular/http';

@Injectable()
export class QuantitiesService {
  
  constructor(private utilitiesService: UtilitiesService, private webService: WebService, private http: Http) {
  }
  
  getAllQuantities() {
    return this.http.get(this.webService.apiUrls().getAllQuantitiesUrl, this.webService.jwt())
      .map(response => response.json())
      .catch((err) => {
        return this.utilitiesService.err(err);
      });
  }
  
  addQuantity(quantity) {
    return this.http.post(this.webService.apiUrls().addQuanityUrl, quantity, this.webService.jwt())
      .map(response => response.json())
      .catch((err) => {
        return this.utilitiesService.err(err);
      });
  }
  
  updateQuantity(quantity) {
    return this.http.put(this.webService.apiUrls().updateQuanityUrl, quantity, this.webService.jwt())
      .map(response => response.json())
      .catch((err) => {
        return this.utilitiesService.err(err);
      });
  }
  
  deleteQuantity(id) {
    return this.http.delete(this.webService.apiUrls().deleteQuanityUrl + id, this.webService.jwt())
      .map(response => response.json())
      .catch((err) => {
        return this.utilitiesService.err(err);
      });
  }
  
}
