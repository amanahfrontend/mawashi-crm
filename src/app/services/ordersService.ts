import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs';

import 'rxjs/add/operator/map';
import { WebService } from './webService';
import { AuthService } from './auth.service';
import { UtilitiesService } from './utilities.service';
import { SharedService } from './shared.service';

@Injectable()

export class OrdersService {
  constructor(private http: Http, private webService: WebService, private utilitiesService: UtilitiesService, private sharedService: SharedService) {
  }

  // private url = 'assets/data.json';

  getAllOrders(filterTerms) {
    // console.log(this.webService.apiUrls().getAllOrdersUrl);
    return this.http.post(this.webService.apiUrls().getAllOrdersUrl, filterTerms, this.webService.jwt())
      .map(response => response.json())
      .catch(this.utilitiesService.err);
  }

  getOrdersDetailsById(id) {
    // console.log(this.webService.apiUrls().getAllOrdersUrl);
    return this.http.get(this.webService.apiUrls().getOrderDetailsByIdUrl + id, this.webService.jwt())
      .map(response => response.json())
      .catch(this.utilitiesService.err);
  }

  getOrdersByCustomerId(id) {
    return this.http.get(this.webService.apiUrls().getOrdersByCustomerId + id, this.webService.jwt())
      .map(response => response.json())
      .catch(this.utilitiesService.err);
  }

  getOrdersByCompanyId(id) {
    return this.http.get(this.webService.apiUrls().getOrdersByCompanyId + id, this.webService.jwt())
      .map(response => response.json())
      .catch(this.utilitiesService.err);
  }

  getOrdersByCreatedUserName(name) {
    return this.http.get(this.webService.apiUrls().getOrdersByCreatedUserName + name, this.webService.jwt())
      .map(response => response.json())
      .catch(this.utilitiesService.err);
  }

  getOrderDetailsByCartId(cartId) {
    return this.http.get(this.webService.apiUrls().getOrderDetailsByCartIdUrl + cartId, this.webService.jwt())
      .map(response => response.json())
      .catch(this.utilitiesService.err);
  }


  getAllActiveCategory() {
    return this.http.get(this.webService.apiUrls().getAllActiveCategoryUrl, this.webService.jwt())
      .map(response => response.json())
      .catch(this.utilitiesService.err);
  }

  getAllSalesActiveCategory() {
    return this.http.get(this.webService.apiUrls().getAllSalesActiveCategoryUrl, this.webService.jwt())
      .map(response => response.json())
      .catch(this.utilitiesService.err);
  }

  getActiveItemsByCategoryId(categoryId) {
    return this.http.get(this.webService.apiUrls().getAllActiveItemByCategoryUrl + categoryId, this.webService.jwt())
      .map(response => response.json());
  }

  getDeactiveItemsByCategoryId(categoryId) {
    return this.http.get(this.webService.apiUrls().getDeactiveItemsByCategoryIdUrl + categoryId, this.webService.jwt())
      .map(response => response.json());
  }

  getItemDetails(itemId) {
    return this.http.get(this.webService.apiUrls().getItemDetailsByCategoryUrl + itemId, this.webService.jwt())
      .map(response => response.json());
  }

  submitCart(cart) {
    return this.http.post(this.webService.apiUrls().postSubmitCartUrl, cart, this.webService.jwt())
      .map(response => response.json())
      .catch(this.utilitiesService.err);
  }

  submitSalesCart(cart) {
    return this.http.post(this.webService.apiUrls().submitSalesCart, cart, this.webService.jwt())
      .map(response => response.json())
      .catch(this.utilitiesService.err);
  }

  printAllOrders(searchObject) {
    return this.http.post(this.webService.apiUrls().printAllOrdersURL, searchObject, this.webService.jwt())
      .map(response => response.json())
      .catch(this.utilitiesService.err);
  }


  updateSalesOrderItems(salesOrder) {
    return this.http.post(this.webService.apiUrls().updateSalesOrderItemsURL, salesOrder, this.webService.jwtCRM())
      .map(response => response.json())
      .catch(this.utilitiesService.err);
  }

  updateAppOrderItems(appOrder) {
    return this.http.post(this.webService.apiUrls().updateAppOrderItemsURL, appOrder, this.webService.jwtCRM())
      .map(response => response.json())
      .catch(this.utilitiesService.err);
  }

  updateCart(cart) {
    return this.http.post(this.webService.apiUrls().updateOrderItems, cart, this.webService.jwtCRM())
      .map(response => response.json())
      .catch(this.utilitiesService.err);
  }

  getCurrentCart(id) {
    return this.http.get(this.webService.apiUrls().getCurrentCartUrl + id, this.webService.jwt())
      .map(response => response.json())
      .catch(this.utilitiesService.err);
  }

  getCurrentSalesCart(id) {
    return this.http.get(this.webService.apiUrls().getCurrentSalesCartUrl + id, this.webService.jwt())
      .map(response => response.json())
      .catch(this.utilitiesService.err);
  }

  getDeliveryTypes(id) {
    return this.http.get(this.webService.apiUrls().getDeliveryTypes + id, this.webService.jwt())
      .map(response => response.json())
      .catch(this.utilitiesService.err);
  }

  getPaymentMethods(deliveryTypeId) {
    return this.http.get(this.webService.apiUrls().getPaymentMethods + deliveryTypeId, this.webService.jwt())
      .map(response => response.json())
      .catch(this.utilitiesService.err);
  }

  getSalesPaymentMethod() {
    let countryCode = this.sharedService.currentUserData().country_Code;
    let url = '';
    if (countryCode.toLowerCase() == 'uae') {
      url = this.webService.apiUrls().paymentMethodsSalesUrl + 'UAE';
    } else {
      url = this.webService.apiUrls().paymentMethodsSalesUrl + 'KW';
    }
    if (url) {
      return this.http.get(url, this.webService.jwt())
        .map(response => response.json())
        .catch(this.utilitiesService.err);
    }
  }

  getAccountAddresses(accountId) {
    return this.http.get(this.webService.apiUrls().getCustomerAddressesUrl + accountId, this.webService.jwt())
      .map(response => response.json())
      .catch(this.utilitiesService.err);
  }

  getCompanyAddress(companyId) {
    let countryId = this.sharedService.currentUserData().fk_Country_Id;
    return this.http.get(this.webService.apiUrls().getCompanyAddressUrl(companyId, countryId), this.webService.jwt())
      .map(response => response.json())
      .catch(this.utilitiesService.err);
  }

  getLocationDates(deliveryData) {
    return this.http.post(this.webService.apiUrls().getLocationDatesUrl, deliveryData, this.webService.jwt())
      .map(response => response.json())
      .catch(this.utilitiesService.err);
  }

  getDeliveryFees() {
    return this.http.get(this.webService.apiUrls().getDeliveryFeesUrl, this.webService.jwt())
      .map(response => response.json())
      .catch(this.utilitiesService.err);
  }

  submitOrder(orderObj) {
    return this.http.post(this.webService.apiUrls().postSubmitOrderUrl, orderObj, this.webService.jwtCRM())
      .map(response => response.json())
      .catch(this.utilitiesService.err);
  }

  submitCompanyOrder(orderObj) {
    return this.http.post(this.webService.apiUrls().postSubmitCompanyOrderUrl, orderObj, this.webService.jwtCRM())
      .map(response => response.json())
      .catch(this.utilitiesService.err);
  }

  moveToNextStep(id) {
    return this.http.get(this.webService.apiUrls().moveToNextStepUrl + id, this.webService.jwtCRM())
      .map(response => response.json())
      .catch(this.utilitiesService.err);
  }

  assignOrderToDriver(obj) {
    return this.http.post(this.webService.apiUrls().assignOrderToDriverUrl, obj, this.webService.jwtCRM())
      .map(response => response.json())
      .catch(this.utilitiesService.err);
  }

  postEmail(obj) {
    return this.http.post(this.webService.apiUrls().postEmailUrl, obj, this.webService.jwtCRM())
      .map(response => response.json())
      .catch(this.utilitiesService.err);
  }

  getMeasurements() {
    return this.http.get(this.webService.apiUrls().getMeasurementsUrl, this.webService.jwt())
      .map(res => res.json());
  }

  postExportOrdersToExcel(exportTerms) {
    return this.http.post(this.webService.apiUrls().exportOrdersToExcelUrl, exportTerms, this.webService.jwt())
      .map(res => res.json());
  }

  postExportSalesOrdersToExcel(exportTerms) {
    return this.http.post(this.webService.apiUrls().exportSalesOrdersToExcelUrl, exportTerms, this.webService.jwt())
      .map(res => res.json());
  }

}
