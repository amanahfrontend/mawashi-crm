import {Injectable} from '@angular/core';
import {UtilitiesService} from './utilities.service';
import {WebService} from './webService';
import {Http} from '@angular/http';
import {SharedService} from './shared.service';

@Injectable()
export class SystemAccountsService {
  
  constructor(private utilitiesService: UtilitiesService, private webService: WebService, private http: Http, private sharedService: SharedService) {
  }
  
  getAllSystemAccounts() {
    let countryId = this.sharedService.currentUserData().fk_Country_Id;
    return this.http.get(this.webService.apiUrls().getAllSystemAccountsUrl + countryId, this.webService.jwt())
      .map(response => response.json())
      .catch(this.utilitiesService.err);
  }
  
  deleteByUserId(userId) {
    return this.http.delete(this.webService.apiUrls().deleteByUserIdUrl + userId, this.webService.jwt())
      .map(response => response.json())
      .catch(this.utilitiesService.err);
  }
  
  updateAccount(accountData) {
    return this.http.put(this.webService.apiUrls().updateSystemAccountUrl, accountData, this.webService.jwt())
      .map(response => response.json());
  }
  
  addNewAccount(accountData) {
    return this.http.post(this.webService.apiUrls().addSystemAccountUrl, accountData, this.webService.jwt())
      .map(response => response.json());
  }
  
  resetPassword(resetPasswordData) {
    return this.http.post(this.webService.apiUrls().resetPasswordUrl, resetPasswordData, this.webService.jwt())
      .map(response => response.json());
  }
  
}
