import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';
import { WebService } from './webService';
import { AuthService } from './auth.service';
import { UtilitiesService } from './utilities.service';
import { SharedService } from './shared.service';

@Injectable({
  providedIn: 'root'
})
export class AnnouncementService {

  constructor(private http: Http, private webService: WebService, private utilitiesService: UtilitiesService) {
  }

  notify(body) {    
    console.log(this.webService.jwt());
    
    return this.http.post(this.webService.apiUrls().sendAnnouncementUrl, body, this.webService.jwt())
      .map(response => response.json())
      .catch(this.utilitiesService.err);
  }

}
