import {Injectable} from '@angular/core';
import {Http} from '@angular/http';
import {WebService} from './webService';

@Injectable()
export class ComplainsService {
  
  constructor(private http: Http, private webService: WebService) {
  }
  
  getAllComplains(paginationObj) {
    return this.http.post(this.webService.apiUrls().getComplainsUrl, paginationObj, this.webService.jwt())
      .map((res) => res.json());
    
  }
  
}
