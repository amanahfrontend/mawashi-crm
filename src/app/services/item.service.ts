import {Injectable} from '@angular/core';
import {Http} from '@angular/http';
import {WebService} from './webService';
import {UtilitiesService} from './utilities.service';

@Injectable({providedIn:'root'})

export class ItemService {
  
  constructor(private http: Http, private webService: WebService, private utilitiesService: UtilitiesService) {
  }
  
  getAllActiveItems(categoryId) {
    return this.http.get(this.webService.apiUrls().getActiveItemsByCategoryIdUrl + categoryId, this.webService.jwt())
      .map(response => response.json())
      .catch((err) => {
        return this.utilitiesService.err(err);
      });
  }
  
  getAllDeactiveItems(categoryId) {
    return this.http.get(this.webService.apiUrls().getDeactiveItemsByCategoryIdUrl + categoryId, this.webService.jwt())
      .map(response => response.json())
      .catch((err) => {
        return this.utilitiesService.err(err);
      });
  }
  
  addUpdateItem(item) {
    // item.cuttingIds = [ 2, 3];
    return this.http.post(this.webService.apiUrls().AdddUpdateItemUrl, item, this.webService.jwt())
      .map(response => response.json())
      .catch((err) => {
        return this.utilitiesService.err(err);
      });
  }
  
  getItemDetails(itemId) {
    return this.http.get(this.webService.apiUrls().getItemDetailsByCategoryUrl + itemId, this.webService.jwt())
      .map(response => response.json())
      .catch(this.utilitiesService.err);
  }
  
  setItemActive(id) {
    return this.http.get(this.webService.apiUrls().setItemActiveUrl + id, this.webService.jwt())
      .map(response => response.json())
      .catch((err) => {
        return this.utilitiesService.err(err);
      });
  }
  
  setItemDeactive(id) {
    return this.http.get(this.webService.apiUrls().setItemDeactiveUrl + id, this.webService.jwt())
      .map(response => response.json())
      .catch((err) => {
        return this.utilitiesService.err(err);
      });
  }
  
  deleteItem(id) {
    return this.http.delete(this.webService.apiUrls().deleteItemUrl + id, this.webService.jwt())
      .map(response => response.json())
      .catch((err) => {
        return this.utilitiesService.err(err);
      });
  }
  
  
}
