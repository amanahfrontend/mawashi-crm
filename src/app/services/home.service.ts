import {Injectable} from '@angular/core';
import {Http} from '@angular/http';
import {WebService} from './webService';
import {UtilitiesService} from './utilities.service';
import {Observable} from 'rxjs/Observable';
import {SharedService} from './shared.service';

@Injectable()
export class HomeService {
  
  constructor(private http: Http, private webService: WebService, private utilitiesService: UtilitiesService, private sharedService: SharedService) {
  }
  
  getTodayOrderState() {
    return this.http.get(this.webService.apiUrls().getTodayOrderState, this.webService.jwt())
      .map(response => response.json())
      .catch(this.utilitiesService.err);
  }
  
  getCustomersCount() {
    return this.http.get(this.webService.apiUrls().getCustomersCount, this.webService.jwt())
      .map(response => response.json())
      .catch(this.utilitiesService.err);
  }
  
  getAppReviewState() {
    return this.http.get(this.webService.apiUrls().getAppReviewStateUrl, this.webService.jwt())
      .map(response => response.json())
      .catch(this.utilitiesService.err);
  }
  
  getOrderByRole(role, filterTerms) {
    // return new Promise(() => {
    console.log(role.toLowerCase() == 'logistics');
    let url = '';
    if (role.toLowerCase() == 'customerservice') {
      url = this.webService.apiUrls().getCustomerServiceOrders;
    } else if (role.toLowerCase() == 'sales') {
      let userName = this.sharedService.currentUserData().name;
      url = this.webService.apiUrls().getOrdersByCreatedUserName + userName;
    } else if (role.toLowerCase() == 'production') {
      let userId = this.sharedService.currentUserData().userId;
      url = this.webService.apiUrls().getProducerOrders + userId;
    } else if (role.toLowerCase() == 'logistics') {
      console.log('found url');
      url = this.webService.apiUrls().getLogisticsOrders;
    }
    console.log(url);
    if (url) {
      return this.http.post(url, filterTerms, this.webService.jwt())
        .map(response => response.json())
        .catch(this.utilitiesService.err);
    } else {
      return Observable.create(() => {
      
      });
    }
    
    // });
  }
  
  
  getDriverOrders(filterTerms) {
    let driverId = this.sharedService.currentUserData().userId;
    return this.http.post(this.webService.apiUrls().getDriverOrdersUrl + driverId, filterTerms, this.webService.jwt())
      .map(res => res.json());
  }
  
  getDriverSalesOrders(filterTerms) {
    return this.http.post(this.webService.apiUrls().getDriverSalesOrdersUrl, filterTerms, this.webService.jwt())
      .map(res => res.json());
  }
  
  postChangeOrderStatus(newStatusObj) {
    return this.http.post(this.webService.apiUrls().postOrderStatusUrl, newStatusObj, this.webService.jwt())
      .map(response => response.json())
      .catch(this.utilitiesService.err);
  }
  
  postChangeOrderLocation(newStatusObj) {
    return this.http.post(this.webService.apiUrls().postOrderLocationUrl, newStatusObj, this.webService.jwt())
      .map(response => response.json())
      .catch(this.utilitiesService.err);
  }
  
  postAssignOrderToProducer(newStatusObj) {
    return this.http.post(this.webService.apiUrls().postAssignOrderToProducerUrl, newStatusObj, this.webService.jwt())
      .map(response => response.json())
      .catch(this.utilitiesService.err);
  }
  
  getContactUs(paginationObj) {
    return this.http.post(this.webService.apiUrls().contactUsUrl, paginationObj, this.webService.jwt())
      .map(response => response.json())
      .catch(this.utilitiesService.err);
  }
  
  getLastSevenDaysOrders() {
    return this.http.get(this.webService.apiUrls().lastSevenDaysChartUrl, this.webService.jwt())
      .map(response => response.json())
      .catch(this.utilitiesService.err);
  }
  
  postDeliveryDate(deliveryDateObj) {
    return this.http.put(this.webService.apiUrls().saveDeliveryDateUrl, deliveryDateObj, this.webService.jwt())
      .map(response => response.json())
      .catch(this.utilitiesService.err);
  }
  
}
