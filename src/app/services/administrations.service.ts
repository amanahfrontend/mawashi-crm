import { Injectable } from '@angular/core';
import { WebService } from './webService';
import { Http, Response } from '@angular/http';
import 'rxjs/add/operator/map';
import { UtilitiesService } from './utilities.service';
import { Observable } from 'rxjs/Observable';
import { SharedService } from './shared.service';

@Injectable()
export class AdministrationsService {

  constructor(private webService: WebService, private http: Http, private utilitiesService: UtilitiesService, private sharedService: SharedService) {
  }

  getAdministrationItemData(itemName) {
    let url = '';
    if (itemName == 'Charity Places') {
      url = this.webService.apiUrls().getDonationAddressUrl;
    } else if (itemName == 'Pickup Places') {
      url = this.webService.apiUrls().getPickupAddressesUrl;
    } else if (itemName == 'Eid Dates') {
      url = this.webService.apiUrls().getEidDatesUrl;
    } else if (itemName == 'Pickup Dates') {
      url = this.webService.apiUrls().getPickupDatesUrl;
    } else if (itemName == 'Donation Dates') {
      url = this.webService.apiUrls().getDonationDatesUrl;
    } else if (itemName == 'Promo Code') {
      url = this.webService.apiUrls().getPromoCodeUrl;
    } else if (itemName == 'Delivery Fees') {
      url = this.webService.apiUrls().getCityFeesUrl;
    } else if (itemName == 'Ads') {
      let id = this.sharedService.currentUserData().fk_Country_Id;
      url = this.webService.apiUrls().getAdsUrl + id;
    } else if (itemName == 'Settings') {
      url = this.webService.apiUrls().getSettingsUrl;
    } else if (itemName == 'Payment Method') {
      url = this.webService.apiUrls().getAllPaymentMethodsUrl;
    }
    else if (itemName == 'FAQ') {
      url = this.webService.apiUrls().getFaqUrl;
    }
    console.log(url);
    if (url) {
      return this.http.get(url, this.webService.jwt())
        .map((response: Response) => response.json());
    } else {
      return Observable.create(() => {
      });
    }
  }

  deleteAdministrationItemData(itemName, id?, data?) {
    let url = '';
    if (itemName == 'Charity Places') {
      url = this.webService.apiUrls().deleteDonationAddressUrl + id;
    } else if (itemName == 'Pickup Places') {
      url = this.webService.apiUrls().deletePickupLocationUrl + id;
    } else if (itemName == 'Eid Dates') {
      url = this.webService.apiUrls().deleteEidDateUrl + id;
    } else if (itemName == 'Pickup Dates') {
      url = this.webService.apiUrls().deletePickupDateUrl + id;
    } else if (itemName == 'Donation Dates') {
      url = this.webService.apiUrls().deleteDonationDateUrl + id;
    } else if (itemName == 'Promo Code') {
      url = this.webService.apiUrls().deletePromoCodeUrl + id;
    } else if (itemName == 'Delivery Fees') {
      url = this.webService.apiUrls().deleteDeliveryFeesUrl + id;
    } else if (itemName == 'Ads') {
      url = this.webService.apiUrls().deleteAdsUrl;
      return this.http.put(url, data, this.webService.jwt())
        .map((response: Response) => response.json());
    }
    console.log(url);
    if (url) {
      return this.http.delete(url, this.webService.jwt())
        .map((response: Response) => response.json());
    } else {
      return Observable.create(() => {
      });
    }
  }

  addAdministrationItemData(itemName, itemData) {
    let url = '';
    if (itemName == 'Charity Places') {
      url = this.webService.apiUrls().addDonationAddressUrl;
    } else if (itemName == 'Pickup Places') {
      url = this.webService.apiUrls().addPickupLocationUrl;
    } else if (itemName == 'Eid Dates') {
      url = this.webService.apiUrls().addEidDateUrl;
    } else if (itemName == 'Pickup Dates') {
      url = this.webService.apiUrls().addPickupDateUrl;
    } else if (itemName == 'Donation Dates') {
      url = this.webService.apiUrls().addDonationDateUrl;
    } else if (itemName == 'Promo Code') {
      url = this.webService.apiUrls().addPromoCodeUrl;
    } else if (itemName == 'Delivery Fees') {
      url = this.webService.apiUrls().addDeliveryFeesUrl;
    } else if (itemName == 'Ads') {
      url = this.webService.apiUrls().addAdsUrl;
    } else if (itemName == 'Ads') {
      url = this.webService.apiUrls().addAdsUrl;
    }
    else if (itemName == 'FAQ') {
      url = this.webService.apiUrls().addFAQUrl;
    }
    console.log(url);
    if (url) {
      return this.http.post(url, itemData, this.webService.jwt())
        .map((response: Response) => response.json());
    } else {
      return Observable.create(() => {
      });
    }
  }

  editAdministrationItemData(itemName, itemData) {
    let url = '';
    if (itemName == 'Charity Places') {
      url = this.webService.apiUrls().editDonationAddressUrl;
    } else if (itemName == 'Pickup Places') {
      url = this.webService.apiUrls().editPickupLocationUrl;
    } else if (itemName == 'Eid Dates') {
      url = this.webService.apiUrls().editEidDateUrl;
    } else if (itemName == 'Pickup Dates') {
      url = this.webService.apiUrls().editPickupDateUrl;
    } else if (itemName == 'Donation Dates') {
      url = this.webService.apiUrls().editDonationDateUrl;
    } else if (itemName == 'Promo Code') {
      url = this.webService.apiUrls().editPromoCodeUrl;
    } else if (itemName == 'Delivery Fees') {
      url = this.webService.apiUrls().editDeliveryFeesUrl;
    } else if (itemName == 'Settings') {
      url = this.webService.apiUrls().editSettingsUrl;
    }
    console.log(url);
    if (url) {
      return this.http.put(url, itemData, this.webService.jwt())
        .map((response: Response) => response.json());
    } else {
      return Observable.create(() => {
      });
    }
  }

  togglePaymentMethodActivation(isActivated, id) {
    let url = '';
    if (isActivated) {
      url = this.webService.apiUrls().deActivatePaymentMethodUrl;
    } else {
      url = this.webService.apiUrls().activatePaymentMethodUrl;
    }

    if (url) {
      return this.http.get(url + id, this.webService.jwt())
        .map((response: Response) => response.json());

    }
  }

}
