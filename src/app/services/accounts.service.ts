import { Injectable } from '@angular/core';
import { WebService } from './webService';
import { Http, Response } from '@angular/http';

import { UtilitiesService } from './utilities.service';
import { SharedService } from './shared.service';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class AccountsService {

  constructor(private webService: WebService, private http: Http, private utilitiesService: UtilitiesService, private sharedService: SharedService) {
  }

  searchAccount(phoneNumber) {
    let url = '';
    let countryCode = this.sharedService.currentUserData().country_Code;
    if (countryCode == 'UAE') {
      url = this.webService.apiUrls().getSearchAccountUAEUrl;
    } else {
      url = this.webService.apiUrls().getSearchAccountKWUrl;
    }
    if (url) {
      return this.http.get(url + phoneNumber, this.webService.jwt()
      ).map((res: Response) => res.json())
        .catch(this.utilitiesService.err);
    } else {
      return new Observable();
    }
  }

  getAllCustomers() {
    return this.http.get(this.webService.apiUrls().getAllCustomers, this.webService.jwt()).map((res: Response) => {
      return res.json();
    })
      .catch(this.utilitiesService.err);
  }

  getAllCustomersByCountryId(id) {
    return this.http.get(this.webService.apiUrls().getAllCustomersByCountryId + id, this.webService.jwt()).map((res: Response) => {
      return res.json();
    })
      .catch(this.utilitiesService.err);
  }


  getCompanyDetailsById(id) {
    return this.http.get(this.webService.apiUrls().getCompanyByIdUrl + id, this.webService.jwt()).map((res: Response) => {
      return res.json();
    })
      .catch(this.utilitiesService.err);
  }

  deleteCompanyById(id) {
    return this.http.delete(this.webService.apiUrls().deleteCompanyUrl + id, this.webService.jwt())
      .map(response => response.json())
      .catch((err) => {
        return this.utilitiesService.err(err);
      });
  }
  postNewCustomerData(newCustomerData) {
    return this.http.post(this.webService.apiUrls().postNewCustomerData, newCustomerData, this.webService.jwt()).map((res: Response) => {
      return res.json();
    })
      .catch(this.utilitiesService.err);
  }

  postNewCompanyData(newCompanyData) {
    return this.http.post(this.webService.apiUrls().postNewCompanyData, newCompanyData, this.webService.jwt()).map((res: Response) => {
      return res.json();
    })
      .catch(this.utilitiesService.err);
  }

  updateCustomerData(CustomerData) {
    return this.http.put(this.webService.apiUrls().updateCustomerData, CustomerData, this.webService.jwt()).map((res: Response) => {
      return res.json();
    }).catch(this.utilitiesService.err);
  }

  updateCompany(CustomerData) {
    return this.http.put(this.webService.apiUrls().updateCompanyUrl, CustomerData, this.webService.jwt()).map((res: Response) => {
      return res.json();
    }).catch(this.utilitiesService.err);
  }

  getAccountDetails(accountId) {
    return this.http.get(this.webService.apiUrls().getAccountDetailsUrl + accountId, this.webService.jwt()).map((res: Response) => {
      return res.json();
    }).catch(this.utilitiesService.err);
  }

  getAllCustomerComplains(accountId, paginationObj) {
    return this.http.post(this.webService.apiUrls().getAllCustomerComplainsUrl + accountId, paginationObj, this.webService.jwt()).map((res: Response) => {
      return res.json();
    }).catch(this.utilitiesService.err);
  }

  postComplain(complainData) {
    return this.http.post(this.webService.apiUrls().postComplainUrl, complainData, this.webService.jwt()).map((res: Response) => {
      return res.json();
    }).catch(this.utilitiesService.err);
  }

  getRatingScale() {
    return this.http.get(this.webService.apiUrls().getRatingScaleUrl, this.webService.jwt()).map((res: Response) => {
      return res.json();
    }).catch(this.utilitiesService.err);
  }

  getAllCompany() {
    return this.http.get(this.webService.apiUrls().getAllCompanyUrl, this.webService.jwt()).map((res: Response) => {
      return res.json();
    }).catch(this.utilitiesService.err);
  }

  getAllCompanyByCountryId(id) {
    return this.http.get(this.webService.apiUrls().getAllCompanyByCountryIdUrl + id, this.webService.jwt()).map((res: Response) => {
      return res.json();
    }).catch(this.utilitiesService.err);
  }

  searchUAECompany(text) {
    return this.http.get(this.webService.apiUrls().searchUAECompanyUrl + text, this.webService.jwt()).map((res: Response) => {
      return res.json();
    }).catch(this.utilitiesService.err);
  }

  searchKWCompany(text) {
    return this.http.get(this.webService.apiUrls().searchKWCompanyUrl + text, this.webService.jwt()).map((res: Response) => {
      return res.json();
    }).catch(this.utilitiesService.err);
  }

  resetPassword(userData) {
    return this.http.post(this.webService.apiUrls().resetPasswordUsersUrl, userData, this.webService.jwt()).map((res: Response) => {
      return res.json();
    }).catch(this.utilitiesService.err);
  }

}
