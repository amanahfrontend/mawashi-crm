import { Injectable } from '@angular/core';
import { UtilitiesService } from './utilities.service';
import { WebService } from './webService';
import { Http } from '@angular/http';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

@Injectable({
  providedIn: 'root'
})

export class CategoryService {
  // currentCategoryState: BehaviorSubject<any> = new BehaviorSubject({type: 'app', state: 'active'});


  constructor(private utilitiesService: UtilitiesService, private webService: WebService, private http: Http) {
  }

  getCategoryTypes() {
    return this.http.get(this.webService.apiUrls().getCategoryTypesUrl, this.webService.jwt())
      .map(response => response.json())
      .catch((err) => {
        return this.utilitiesService.err(err);
      });
  }

  getAllAppParent() {
    return this.http.get(this.webService.apiUrls().getAllAppParentUrl, this.webService.jwt())
      .map(response => response.json())
      .catch((err) => {
        return this.utilitiesService.err(err);
      });
  }

  getAllSalesParent() {
    return this.http.get(this.webService.apiUrls().getAllSalesParentUrl, this.webService.jwt())
      .map(response => response.json())
      .catch((err) => {
        return this.utilitiesService.err(err);
      });
  }


  getAllCategory() {
    return this.http.get(this.webService.apiUrls().getAllCategoryUrl, this.webService.jwt())
      .map(response => response.json())
      .catch((err) => {
        return this.utilitiesService.err(err);
      });
  }

  getAppActiveCategory() {
    return this.http.get(this.webService.apiUrls().getAllAppActiveCategoryUrl, this.webService.jwt())
      .map(response => response.json())
      .catch((err) => {
        return this.utilitiesService.err(err);
      });

  }

  getAppActiveSubCategory(parentId) {
    return this.http.get(this.webService.apiUrls().getAllAppActiveSubCategoryUrl + parentId, this.webService.jwt())
      .map(response => response.json())
      .catch((err) => {
        return this.utilitiesService.err(err);
      });

  }

  getSalesActiveSubCategory(parentId) {
    return this.http.get(this.webService.apiUrls().getAllSalesActiveSubCategoryUrl + parentId, this.webService.jwt())
      .map(response => response.json())
      .catch((err) => {
        return this.utilitiesService.err(err);
      });

  }


  getAppDeactiveCategory() {
    return this.http.get(this.webService.apiUrls().getAllAppDeactiveCategoryUrl, this.webService.jwt())
      .map(response => response.json())
      .catch((err) => {
        return this.utilitiesService.err(err);
      });

  }

  getSalesDeactiveCategory() {
    return this.http.get(this.webService.apiUrls().getAllSalesDeactiveCategoryUrl, this.webService.jwt())
      .map(response => response.json())
      .catch((err) => {
        return this.utilitiesService.err(err);
      });

  }

  getSalesActiveCategory() {
    return this.http.get(this.webService.apiUrls().getAllSalesActiveCategoryUrl, this.webService.jwt())
      .map(response => response.json())
      .catch((err) => {
        return this.utilitiesService.err(err);
      });
  }

  deleteCategory(id) {
    return this.http.delete(this.webService.apiUrls().deleteCategoryUrl + id, this.webService.jwt())
      .map(response => response.json())
      .catch((err) => {
        return this.utilitiesService.err(err);
      });
  }

  postNewCategory(category) {
    return this.http.post(this.webService.apiUrls().postNewCategoryUrl, category, this.webService.jwt())
      .map(response => response.json())
      .catch((err) => {
        return this.utilitiesService.err(err);
      });
  }

  updateCategory(category) {
    return this.http.put(this.webService.apiUrls().updateCategoryUrl, category, this.webService.jwt())
      .map(response => response.json())
      .catch((err) => {
        return this.utilitiesService.err(err);
      });
  }

  setCategoryActive(id) {
    return this.http.get(this.webService.apiUrls().setCategoryActiveUrl + id, this.webService.jwt())
      .map(response => response.json())
      .catch((err) => {
        return this.utilitiesService.err(err);
      });
  }

  setCategoryDeActive(id) {
    return this.http.get(this.webService.apiUrls().setCategoryDeactiveUrl + id, this.webService.jwt())
      .map(response => response.json())
      .catch((err) => {
        return this.utilitiesService.err(err);
      });
  }

  getAllCategoryConfig() {
    return this.http.get(this.webService.apiUrls().getAllCategoryConfigUrl, this.webService.jwt())
      .map(response => response.json())
      .catch((err) => {
        return this.utilitiesService.err(err);
      });
  }

  getAllOrderTypes() {
    return this.http.get(this.webService.apiUrls().getAllOrderTypeSortUrl, this.webService.jwt())
      .map(response => response.json())
      .catch((err) => {
        return this.utilitiesService.err(err);
      });
  }

}
