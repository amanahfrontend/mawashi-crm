import {Injectable} from '@angular/core';
import {Http} from '@angular/http';
import {WebService} from './webService';
import {UtilitiesService} from './utilities.service';

@Injectable()
export class OrdersFilterService {
  
  constructor(private http: Http, private webService: WebService, private utilitiesService: UtilitiesService) {
  }
  
  filterOrders(filterObj) {
    return this.http.post(this.webService.apiUrls().filterOrdersUrl, filterObj, this.webService.jwt())
      .map(response => response.json())
      .catch(this.utilitiesService.err);
  }
  
}
