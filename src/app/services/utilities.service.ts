import {Injectable} from '@angular/core';
import {WebService} from './webService';
import {Http, Response} from '@angular/http';
import {Observable} from '../../../node_modules/rxjs';
import {AuthService} from './auth.service';

// declare var $: any;

@Injectable()
export class UtilitiesService {
  passwordRegex;
  emailRegex;
  phoneRegex;

  constructor(private webService: WebService, private http: Http) {
    this.passwordRegex = /^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{6,16}$/;

    this.emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    this.phoneRegex = /^[0-9]+$/;
  }

  getAllCountries() {
    return this.http.get(this.webService.apiUrls().getAllCountryUrl, this.webService.jwt()).map((res: Response) => {
      return res.json();
    });
  }

  getCountryCode(id) {
    return new Promise((resolve, reject) => {
      this.getAllCountries()
        .subscribe((countries) => {
            console.log(countries);
            // res.map((country) => {
            for (let i = 0, l = countries.length; i < l; i++) {
              if (countries[i].id == id) {
                resolve(countries[i].name);
              }
            }
          },
          err => {
            alert('server error');
          });
    });
  }


  convertDatetoNormal(date) {
    date = new Date(`${date}`);
    let year = date.getFullYear();
    let month = date.getMonth() + 1;
    let day = date.getDate();

    if (day < 10) {
      day = '0' + day;
    }
    if (month < 10) {
      month = '0' + month;
    }
    let formatedDate = `${day}/${month}/${year}`;
    //console.log(formatedDate);
    return formatedDate;
  }

  getBase64FromFile(file) {
    return new Promise((resolve, reject) => {
      if (file) {
        let reader = new FileReader();
        let base64file;

        reader.onload = (readerEvt) => {
          let binaryStringFileReader = <FileReader>readerEvt.target;
          let binaryString =<string> binaryStringFileReader.result;
          base64file = btoa(binaryString);
          console.log(base64file);

          /*********** collect the json object *************/
          let jsonObject = {
            'fileName': file.name,
            'fileContent': base64file
          };

          resolve(jsonObject);

        };

        let x = reader.readAsBinaryString(file);
      }
      /* Not sure about the importance of this libe :( */
      //console.log(x);
    });
  }

  /*error function when service error 401 call logout*/
  err(err) {
    console.log(err.status);
    return Observable.create(() => {
      if (err.status == '401') {
        // this.authService.logOut();
      } else {
        alert('server error, please try again');
      }
    }, () => {
    });
  };

  convertToLocalUTCLikeFormat(day) {
    let todayDate = new Date(day);
    let dd = todayDate.getDate().toString();
    let mm = (todayDate.getMonth() + 1).toString();

    let yyyy = todayDate.getFullYear();
    if (Number(dd) < 10) {
      dd = '0' + dd.toString();
    }
    if (Number(mm) < 10) {
      mm = '0' + mm.toString();
    }
    return yyyy + '-' + mm + '-' + dd + 'T00:00:00.00Z';
  }

}
