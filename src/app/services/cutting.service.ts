import {Injectable} from '@angular/core';
import {WebService} from './webService';
import {UtilitiesService} from './utilities.service';
import {Http} from '@angular/http';

@Injectable()
export class CuttingService {
  
  constructor(private webService: WebService, private utilitiesService: UtilitiesService, private http: Http) {
  }
  
  getAllCutting() {
    return this.http.get(this.webService.apiUrls().getAllCuttingUrl, this.webService.jwt())
      .map(response => response.json())
      .catch((err) => {
        return this.utilitiesService.err(err);
      });
  }
  
  addCutting(cuttingObj) {
    return this.http.post(this.webService.apiUrls().addCuttingUrl, cuttingObj, this.webService.jwt())
      .map(response => response.json())
      .catch((err) => {
        return this.utilitiesService.err(err);
      });
  }
  
  updateCutting(cuttingObj) {
    return this.http.put(this.webService.apiUrls().updateCuttingUrl, cuttingObj, this.webService.jwt())
      .map(response => response.json())
      .catch((err) => {
        return this.utilitiesService.err(err);
      });
  }
  
  deleteCutting(id) {
    return this.http.delete(this.webService.apiUrls().deleteCuttingUrl + id, this.webService.jwt())
      .map(response => response.json())
      .catch((err) => {
        return this.utilitiesService.err(err);
      });
  }
  
}
