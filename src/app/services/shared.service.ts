import { Injectable } from '@angular/core';
import { WebService } from './webService';
import { UtilitiesService } from './utilities.service';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

@Injectable()
export class SharedService {
  currentUserRole = new BehaviorSubject('');
  isCustomerServiceRole: boolean;
  isAccountantRole: boolean;
  isProducerRole: boolean;
  isLogistecsRole: boolean;
  isAdminRole: boolean;
  isSuperAdminRole: boolean;
  isSalesRole: boolean;
  isGuestRole: boolean;

  constructor(private webService: WebService, private utilitiesService: UtilitiesService, private http: Http) {
    this.setCurrentUserRole();
  }

  getAllGovernorate() {
    return this.http.get(this.webService.apiUrls().getAllGovernorateUrl, this.webService.jwt())
      .map(response => response.json())
      .catch(this.utilitiesService.err);
  }

  setCurrentUserRole(role?) {
    this.setUserRoleFlag();
    if (role) {
      console.log(role);
      this.currentUserRole.next(role);
    } else {
      // console.log(this.currentUserData().roles[0]);
      this.currentUserRole.next((this.currentUserData() && this.currentUserData().roles[0]) || '');
    }
  }

  setUserRoleFlag() {
    // return new Promise((resolve, reject) => {
    this.currentUserRole.subscribe((currentUserRole) => {
      console.log(currentUserRole);
      this.isCustomerServiceRole = currentUserRole.toLowerCase() == 'customerservice';
      this.isSalesRole = currentUserRole.toLowerCase() == 'sales';
      this.isAdminRole = currentUserRole.toLowerCase() == 'admin';
      this.isSuperAdminRole = currentUserRole.toLowerCase() == 'superadmin';
      this.isProducerRole = currentUserRole.toLowerCase() == 'production';
      this.isLogistecsRole = currentUserRole.toLowerCase() == 'logistics';
      this.isAccountantRole = currentUserRole.toLowerCase() == 'accountant';
      this.isGuestRole = currentUserRole.toLowerCase() == 'guest';
      // resolve();
      // });
    });
  }

  currentUserData() {
    return JSON.parse(window.localStorage.getItem('currentUser'));
  }

  removeAddress(id) {
    return this.http.delete(this.webService.apiUrls().deleteAddressUrl + id, this.webService.jwt())
      .map(response => response.json())
      .catch(this.utilitiesService.err);
  }

  removeCompanyAddress(id) {
    return this.http.delete(this.webService.apiUrls().deleteCompanyAddressUrl + id, this.webService.jwt())
      .map(response => response.json())
      .catch(this.utilitiesService.err);
  }


  saveAddress(addressPost) {
    return this.http.post(this.webService.apiUrls().addAddressUrl, addressPost, this.webService.jwt())
      .map(response => response.json())
      .catch(this.utilitiesService.err);
  }

  saveCompanyAddress(addressPost) {
    return this.http.post(this.webService.apiUrls().addCompanyAddressUrl, addressPost, this.webService.jwt())
      .map(response => response.json())
      .catch(this.utilitiesService.err);
  }

  locationByPACIOrMakany(number) {
    let countryCode = this.currentUserData().country_Code.toLowerCase();
    if (countryCode == 'uae' && number.length == 10) {
      return this.http.get(this.webService.apiUrls().getLocationByMakanyUrl + number, this.webService.jwt())
        .map(response => response.json());
    } else if (countryCode == 'kuwait' && number.length >= 8) {
      return this.http.get(this.webService.apiUrls().getLocationByPACIUrl + number, this.webService.jwt())
        .map(response => response.json())
        .catch((err) => {
          return this.utilitiesService.err(err);
        });
    }
  }

  getAllRoles() {
    console.log(this.webService.apiUrls().getAllRoles);
    return this.http.get(this.webService.apiUrls().getAllRoles, this.webService.jwt())
      .map(response => response.json())
      .catch(this.utilitiesService.err);
  }

  getAllOrderStatus() {
    return this.http.get(this.webService.apiUrls().getAllOrderStatusUrl, this.webService.jwt())
      .map(response => response.json())
      .catch(this.utilitiesService.err);
  }

  getAllOrderTypes() {
    return this.http.get(this.webService.apiUrls().getAllOrderTypeUrl, this.webService.jwt())
      .map(response => response.json())
      .catch(this.utilitiesService.err);
  }

  getAccountDetails(accountId) {
    return this.http.get(this.webService.apiUrls().getAccountDetailsUrl + accountId, this.webService.jwt()).map((res: Response) => {
      return res.json();
    }).catch(this.utilitiesService.err);
  }

  getCompanyDetailsById(id) {
    return this.http.get(this.webService.apiUrls().getCompanyByIdUrl + id, this.webService.jwt()).map((res: Response) => {
      return res.json();
    })
      .catch(this.utilitiesService.err);
  }

  postSMS(SMSobj) {
    let countryCode = this.currentUserData().country_Code;
    let url = '';
    if (countryCode == 'UAE') {
      url = this.webService.apiUrls().postSMSUAEUrl;
    } else {
      url = this.webService.apiUrls().postSMSKWUrl;
    }
    return this.http.post(url, SMSobj, this.webService.jwt())
      .map(response => response.json())
      .catch(this.utilitiesService.err);
  }

  getAllDeliveryTypes() {
    return this.http.get(this.webService.apiUrls().getAllDeliveryTypesUrl, this.webService.jwt())
      .map(response => response.json())
      .catch(this.utilitiesService.err);
  }

  getAllPaymentMethods() {
    return this.http.get(this.webService.apiUrls().getAllPaymentMethodsUrl, this.webService.jwt())
      .map(response => response.json())
      .catch(this.utilitiesService.err);
  }

  getAllPaymentMethodsApp() {
    return this.http.get(this.webService.apiUrls().getAllPaymentMethodsUrlApp, this.webService.jwt())
      .map(response => response.json())
      .catch(this.utilitiesService.err);
  }

  getAllPaymentMethodsSales() {
    return this.http.get(this.webService.apiUrls().getAllPaymentMethodsUrlSales, this.webService.jwt())
      .map(response => response.json())
      .catch(this.utilitiesService.err);
  }

  getAllDrivers() {
    return this.http.get(this.webService.apiUrls().getAllDriversUrl, this.webService.jwt())
      .map(response => response.json())
      .catch(this.utilitiesService.err);
  }

  getAllProducers() {
    return this.http.get(this.webService.apiUrls().getAllProducersUrl, this.webService.jwt())
      .map(response => response.json())
      .catch(this.utilitiesService.err);
  }


  getFilteredInvoices(filterObj) {
    return this.http.post(this.webService.apiUrls().getFilteredInvoices, filterObj, this.webService.jwt())
      .map(res => res.json());
  }


}
