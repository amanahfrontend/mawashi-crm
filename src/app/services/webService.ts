import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions } from '@angular/http';

@Injectable()

// {{BaseURL}}/locationapi/v1/paci/GetLocationByPaci?paciNumber=90254388

export class WebService {
  loggedInUserData = JSON.parse(localStorage.getItem('currentUser'));
  private _countryCode = () => {
    let countryCode = this.loggedInUserData && this.loggedInUserData.country_Code;
    if (countryCode && (countryCode.toLowerCase() == 'kuwait')) {
      countryCode = 'kw';
    }
    return countryCode;
  };
  private _countryId = this.loggedInUserData && this.loggedInUserData.fk_Country_Id;

  //private _baseIp = 'http://159.89.229.238:9000/';
  private _baseIp = 'http://157.230.52.100:7000/';
  private _user = this._baseIp + 'identityapi/v1/user/';
  private _customer = this._baseIp + 'customerapi/v1/';
  private _order = this._baseIp + 'orderApi/' + this._countryCode() + '/v1/';
  private _cart = this._baseIp + 'cartApi/' + this._countryCode() + '/V1/Cart/';
  private _complain = this._baseIp + 'ReviewApi/' + this._countryCode() + '/V1/';
  private _inventory = this._baseIp + 'inventoryapi/' + this._countryCode() + '/v1/';
  private _payment = this._baseIp + 'paymentApi/' + this._countryCode() + '/v1/';
  private _review = this._baseIp + 'reviewApi/' + this._countryCode() + '/v1/';
  private _locationKWD = this._baseIp + 'locationapi/v1/paci/';
  private _locationUAE = this._baseIp + 'locationapi/v1/makani/';
  private _dropup = this._baseIp + 'dropoutapi/v1/';
  private _promo = this._baseIp + 'promocodeapi/' + this._countryCode() + '/v1/';
  private _content = this._baseIp + 'contentapi/v1/';
  private _settings = this._baseIp + 'settingApi/' + this._countryCode() + '/v1/';
  private _faq = this._baseIp + 'contentapi/v1/faq/getbycountry/' + this._countryCode();
  private _faqSaved = this._baseIp + 'contentapi/v1/faq/add';


  constructor() {

  }

  apiUrls() {
    return {
      login: this._user + 'login',
      searchCustomerByPhone: this._user + 'search?searchtoken=',
      getAllCustomers: this._customer + 'customer/GetAllCustomers',
      getAllCustomersByCountryId: this._customer + 'customer/GetAllCustomers/',
      postNewCustomerData: this._customer + 'customer/basicRegister',
      postNewCompanyData: this._customer + 'Company/register',
      getAllCountryUrl: this._customer + 'Country/GetAll',
      getAccountDetailsUrl: this._customer + 'customer/GetById?id=',
      updateCustomerData: this._customer + 'customer/UpdateCustomerUser',
      getOrdersByCustomerId: this._order + 'Order/GetOrdersByCustomer/',
      getOrdersByCreatedUserName: this._order + 'Order/GetOrdersByCreatedUserNamePaginated/',
      getOrdersByCompanyId: this._order + 'Order/GetCompanyOrders/',
      getOrderDetailsByCartIdUrl: this._cart + 'GetCartWithItems/',
      getAllCustomerComplainsUrl: this._complain + 'review/GetComplains/',
      postComplainUrl: this._complain + 'review/SubmitComplainReview',
      getRatingScaleUrl: this._complain + 'RatingScale/GetComplainOptions',
      getAllActiveCategoryUrl: this._inventory + 'LKP_Category/GetAllActive',
      getAllSalesActiveCategoryUrl: this._inventory + 'LKP_Category/GetAllSalesActive',
      getAllActiveItemByCategoryUrl: this._inventory + 'LKP_Item/GetActiveByCategoryId/',
      getItemDetailsByCategoryUrl: this._inventory + 'LKP_Item/GetItemDetailsById/',
      postSubmitCartUrl: this._cart + 'Submit',
      getCurrentCartUrl: this._cart + 'GetCurrentCart/',
      getCurrentSalesCartUrl: this._cart + 'GetCurrentCart/retail/',
      getDeliveryTypes: this._inventory + 'CategoryDeliveryOptions/GetDeliveryOptions/',
      postSubmitOrderUrl: this._order + 'Order/checkout',
      printAllOrdersURL: this._order + 'Order/FilterToBulkPrint',


      updateSalesOrderItemsURL: this._order + 'Order/UpdateOrderSalesItems',
      updateAppOrderItemsURL: this._order + 'Order/UpdateOrderItems',
      postSubmitCompanyOrderUrl: this._order + 'Order/checkoutRetail',
      getPaymentMethods: this._payment + 'PaymentMethod/GetByCountryDelivery?countryId=' + this._countryId + '&deliveryType=',
      getCustomerAddressesUrl: this._customer + 'location/GetByCustomerId?customerId=',
      getDeliveryFeesUrl: this._order + 'DeliveryFees/GetFees',
      getLocationDatesUrl: this._order + 'CategoryLocation/GetLocationsDates',
      getAllGovernorateUrl: this._locationKWD + '/GetAllGovernorates',
      getAreaByGovernorateUrl: this._locationKWD + '/GetAreas?govId=',
      deleteAddressUrl: this._customer + 'Location/DeleteAddress/',
      addAddressUrl: this._customer + 'location/addaddress',
      addCompanyAddressUrl: this._customer + 'CompanyLocation/AddAddress',
      getLocationByPACIUrl: this._locationKWD + 'GetLocationByPaci?paciNumber=',
      getLocationByMakanyUrl: this._locationUAE + 'GetMakaniInfoFromNumber?makaniNumber=',
      getAllCuttingUrl: this._inventory + 'LKP_Cutting/GetAll',
      deleteCuttingUrl: this._inventory + 'LKP_Cutting/Delete/',
      updateCuttingUrl: this._inventory + 'LKP_Cutting/Update',
      addCuttingUrl: this._inventory + 'LKP_Cutting/Add',
      getAllQuantitiesUrl: this._inventory + 'LKP_Quantity/GetAll',
      addQuanityUrl: this._inventory + 'LKP_Quantity/Add',
      deleteQuanityUrl: this._inventory + 'LKP_Quantity/Delete/',
      updateQuanityUrl: this._inventory + 'LKP_Quantity/Update',
      getCategoryTypesUrl: this._inventory + 'LKP_CategoryType/GetAll',
      getAllAppParentUrl: this._inventory + 'LKP_Category/GetAllActiveParents',
      getAllSalesParentUrl: this._inventory + 'LKP_Category/GetAllSalesActiveParents',


      getAllCategoryUrl: this._inventory + 'LKP_Category/GetAll',
      getAllAppActiveCategoryUrl: this._inventory + 'LKP_Category/GetAllActive',
      // getAllSalesActiveCategoryUrl: this._inventory + 'LKP_Category/GetAllSalesActive',
      getAllAppDeactiveCategoryUrl: this._inventory + 'LKP_Category/GetAllDeActive',
      getAllSalesDeactiveCategoryUrl: this._inventory + 'LKP_Category/GetAllSalesDeActive',
      getActiveItemsByCategoryIdUrl: this._inventory + 'LKP_Item/GetActiveByCategoryId/',
      getDeactiveItemsByCategoryIdUrl: this._inventory + 'LKP_Item/GetDeActiveByCategoryId/',
      deleteCategoryUrl: this._inventory + 'LKP_Category/Delete/',
      postNewCategoryUrl: this._inventory + 'LKP_Category/Add',
      updateCategoryUrl: this._inventory + 'LKP_Category/update',
      setCategoryActiveUrl: this._inventory + 'LKP_Category/Activate/',
      setCategoryDeactiveUrl: this._inventory + 'LKP_Category/DeActivate/',
      getAllCategoryConfigUrl: this._inventory + 'LKP_CategoryConfig/GetAll',

      getAllAppActiveSubCategoryUrl: this._inventory + 'LKP_Category/GetAllActiveByParent?parentId=',
      getAllSalesActiveSubCategoryUrl: this._inventory + 'LKP_Category/GetAllSalesActiveByParent?parentId=',




      getAllOrderTypeSortUrl: this._inventory + 'ItemsSortType/GetAll',


      AdddUpdateItemUrl: this._inventory + 'LKP_Item/Submit',
      addItemUrl: this._inventory + 'LKP_Item/Add',
      setItemActiveUrl: this._inventory + 'LKP_Item/Activate/',
      setItemDeactiveUrl: this._inventory + 'LKP_Item/DeActivate/',
      deleteItemUrl: this._inventory + 'LKP_Item/Delete/',
      getTodayOrderState: this._order + 'order/GetTodayOrderStats',
      getCustomersCount: this._customer + 'customer/GetAllCustomersCount',
      getAppReviewStateUrl: this._review + 'review/GetAppReviewStats',
      getAllOrdersUrl: this._order + 'order/GetAllExcludedLastStepPaginated',
      getOrderDetailsByIdUrl: this._order + 'order/get/',
      getAllSystemAccountsUrl: this._user + 'GetEmployees/',
      deleteByUserIdUrl: this._user + 'deletebyid?id=',
      getAllRoles: this._user.replace('user', 'role') + 'getall',
      updateSystemAccountUrl: this._user + 'update',
      addSystemAccountUrl: this._user + 'add',
      resetPasswordUrl: this._user + 'ResetPassword',
      getCustomerServiceOrders: this._order + 'Order/GetCustomerServiceOrdersPaginated',
      getSalesOrders: this._order + 'Order/GetOrdersRetails',
      getProductionOrders: this._order + 'Order/GetProducerOrders',
      getProducerOrders: this._order + 'Order/GetProducerOrdersPaginated/',
      getLogisticsOrders: this._order + 'Order/GetLogisticsOrdersPaginated',
      getAllCompanyUrl: this._customer + 'Company/GetAllCompanies',
      getAllCompanyByCountryIdUrl: this._customer + 'Company/GetAllCompanies/',
      searchUAECompanyUrl: this._customer + 'Company/SearchUAECompanies?searchToken=',
      searchKWCompanyUrl: this._customer + 'Company/SearchKWCompanies?searchToken=',
      getCompanyByIdUrl: this._customer + 'Company/GetById?id=',
      updateCompanyUrl: this._customer + 'Company/UpdateCompanyUser',
      getCompanyAddressUrl: (companyId, countryId) => this._customer + 'CompanyLocation/GetByCompanyId?companyId=' + companyId + '&countryId=' + countryId,
      deleteCompanyAddressUrl: this._customer + 'CompanyLocation/DeleteAddress/',
      filterOrdersUrl: this._order + 'order/Filter',
      getAllOrderStatusUrl: this._order + 'LKP_OrderStatus/GetAll',
      getAllOrderTypeUrl: this._order + 'OrderType/getall',

      getAllPaymentMethodsUrlApp: this._payment + 'PaymentMethod/GetByCountry?countryCode=' + this._countryCode(),
      getAllPaymentMethodsUrlSales: this._payment + 'PaymentMethod/GetByCountryForCompany?countryCode=' + this._countryCode(),
      getAllPaymentMethodsUrl: this._payment + 'PaymentMethod/GetAll',
      getAllDeliveryTypesUrl: this._order + 'DeliveryType/GetAll',
      moveToNextStepUrl: this._order + 'Order/MoveToNextStep/',
      postOrderStatusUrl: this._order + 'Order/ChangeOrderStatus',
      postOrderLocationUrl: this._order + 'Order/UpdateOrderLocation',
      postSMSKWUrl: this._dropup + 'sms/sendkw',
      postSMSUAEUrl: this._dropup + 'sms/senduae',
      postEmailUrl: this._dropup + 'email/send',
      getAllDriversUrl: this._user + 'GetAllDrivers/' + this._countryId,
      assignOrderToDriverUrl: this._order + 'Order/AssignDriverToOrder',
      updateOrderItems: this._order + 'Order/UpdateOrderItems',
      getPromoCodeUrl: this._promo + 'PromoCode/GetAll',
      getDonationDatesUrl: this._order + 'DonationDates/GetAll',
      getPickupDatesUrl: this._order + 'PickUpDates/GetAll',
      getEidDatesUrl: this._order + 'EidDates/GetAll',
      getCityFeesUrl: this._order + 'DeliveryFees/GetAll',
      getDonationAddressUrl: this._order + 'Donation/GetAll',
      getPickupAddressesUrl: this._order + 'PickUp/GetAll',
      getAdsUrl: this._content + 'ad/GetActive?country=',
      getSettingsUrl: this._settings + 'Setting/GetAll',
      getFaqUrl: this._faq,
      getDriverOrdersUrl: this._order + 'order/GetDriverOrdersPaginated/',
      getDriverSalesOrdersUrl: this._order + 'order/GetDriverOrdersPaginated/Retail',
      getSearchAccountUAEUrl: this._customer + 'customer/SearchUAECustomers?searchToken=',
      getSearchAccountKWUrl: this._customer + 'customer/SearchKWCustomers?searchToken=',

      deleteDonationAddressUrl: this._order + 'Donation/Delete/',
      deletePromoCodeUrl: this._promo + 'PromoCode/Delete/',
      deletePickupDateUrl: this._order + 'PickUpDates/Delete/',
      deleteEidDateUrl: this._order + 'EidDates/Delete/',
      deletePickupLocationUrl: this._order + 'PickUp/Delete/',
      deleteDeliveryFeesUrl: this._order + 'DeliveryFees/Delete/',
      deleteDonationDateUrl: this._order + 'DonationDates/Delete/',
      deleteAdsUrl: this._content + 'ad/Archive',

      deleteCompanyUrl: this._customer + 'company/DeleteCompany?id=',


      addDonationAddressUrl: this._order + 'Donation/AddAddress',
      addPickupLocationUrl: this._order + 'PickUp/AddAddress',
      addEidDateUrl: this._order + 'EidDates/Add',
      addPickupDateUrl: this._order + 'PickUpDates/Add',
      addDonationDateUrl: this._order + 'DonationDates/Add',
      addPromoCodeUrl: this._promo + 'PromoCode/Add',
      addDeliveryFeesUrl: this._order + 'DeliveryFees/Submit',
      addAdsUrl: this._content + 'ad/add',
      addFAQUrl: this._faqSaved,

      editDonationAddressUrl: this._order + 'Donation/EditAddress',
      editPickupLocationUrl: this._order + 'PickUp/EditAddress',
      editEidDateUrl: this._order + 'EidDates/Update',
      editPickupDateUrl: this._order + 'PickUpDates/Update',
      editDonationDateUrl: this._order + 'DonationDates/Update',
      editPromoCodeUrl: this._promo + 'PromoCode/Update',
      editDeliveryFeesUrl: this._order + 'DeliveryFees/Update',
      editSettingsUrl: this._settings + 'Setting/Update',
      getComplainsUrl: this._review + 'review/GetComplains',
      getFilteredInvoices: this._order + 'Order/FilterInvoices',
      getMeasurementsUrl: this._baseIp + 'cartApi/uae/V1/MeasurmentType/GetAll',
      submitSalesCart: this._cart + 'Submit/SalesCart',
      paymentMethodsSalesUrl: this._payment + 'PaymentMethod/GetByCountryForCompany?countryCode=',
      contactUsUrl: this._review + 'review/GetContactUSMessages',
      producerOrdersUrl: this._order + 'Order/GetProducerOrders/',
      exportOrdersToExcelUrl: this._order + 'Order/FilterToExcel',
      exportSalesOrdersToExcelUrl: this._order + 'Order/FilterToSalesExcel',
      getAllProducersUrl: this._user + 'GetAllProducers/' + this._countryId,
      postAssignOrderToProducerUrl: this._order + 'Order/AssignProducerToOrder',
      getAllPaymentTypesUrl: this._payment + 'PaymentMethod/GetAllByCountryId/' + this._countryId,
      activatePaymentMethodUrl: this._payment + 'PaymentMethod/Activate/',
      deActivatePaymentMethodUrl: this._payment + 'PaymentMethod/deactivate/',
      lastSevenDaysChartUrl: this._order + 'order/GetOrdersCountByDays/7',
      resetPasswordUsersUrl: this._baseIp + 'identityapi/v1/user/ResetPassword',
      saveDeliveryDateUrl: this._order + 'order/UpdateDeliveryDate',
      sendAnnouncementUrl:this._dropup+'Notification/SendBroadcast',
    };
  }

  jwt() {
    // create authorization header with jwt token
    let currentUser = JSON.parse(localStorage.getItem('currentUser'));
    if (currentUser && currentUser.token) {
      let headers = new Headers({ 'Authorization': 'bearer ' + currentUser.token });
      headers.append('Content-Type', 'application/json');
      return new RequestOptions({ headers: headers });
    }
  }

  jwtCRM() {
    let currentUser = JSON.parse(localStorage.getItem('currentUser'));
    if (currentUser && currentUser.token) {
      let headers = new Headers({ 'Authorization': 'bearer ' + currentUser.token });
      headers.append('Content-Type', 'application/json');
      headers.append('Platform', 'WebCRM');
      return new RequestOptions({ headers: headers });
    }
  }

}
