import {Injectable} from '@angular/core';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {Router} from '@angular/router';
import {Http, RequestOptions, Headers, Response} from '@angular/http';
import {WebService} from './webService';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import {SharedService} from './shared.service';

// import {SharedService} from './shared.service';

@Injectable()
export class AuthService {
  isAuth: BehaviorSubject<boolean> = new BehaviorSubject(false);
  private _options;
  
  constructor(private router: Router, private http: Http, private webService: WebService, private sharedService: SharedService) {
    let headers = new Headers({'Content-Type': 'application/json', 'Access-Control-Allow-Origin': '*'});
    // headers.append();
    this._options = new RequestOptions({headers: headers});
    this._checkIsAuth();
    // this.sharedService.setUserRoleFlag();
  }
  
  logIn(logInData) {
    return new Promise((resolve, reject) => {
      
      return this.http.post(this.webService.apiUrls().login, logInData, this._options)
        .map((res) => {
          console.log(res);
          return res.json();
        })
        .catch((err) => {
          console.log(err);
          alert('Failed to Log in due to ' + err.status);
          return Observable.create((val) => {
          }).subscribe().unsubscribe();
        })
        .subscribe((res) => {
          // console.log(res);
          /*get logged in user country code*/
          this.getCountryCode(res.fk_Country_Id)
            .then((countryCode) => {
              // console.log(countryCode);
              res.country_Code = countryCode;
              if (res.country_Code.toLowerCase() == 'uae') {
                res.currency = 'AED';
              } else if (res.country_Code.toLowerCase() == 'kuwait') {
                res.currency = 'KWD';
              }
              console.log('will save el zft');
              console.log(res.roles[0]);
              this.sharedService.setCurrentUserRole(res.roles[0]);
              window.localStorage.setItem('currentUser', JSON.stringify(res, null, 3));
              this.webService.loggedInUserData = this.currentUserData();
              // this.sharedService.setUserRoleFlag();
              this._setIsAuth(true);
              resolve();
            });
        });
    });
  }
  
  private _setIsAuth(value) {
    this.isAuth.next(value);
  }
  
  getCountryCode(id) {
    return new Promise((resolve, reject) => {
      this.getAllCountries()
        .subscribe((countries) => {
            console.log(countries);
            // res.map((country) => {
            for (let i = 0, l = countries.length; i < l; i++) {
              if (countries[i].id == id) {
                resolve(countries[i].name);
              }
            }
          },
          err => {
            alert('server error');
          });
    });
  }
  
  getAllCountries() {
    return this.http.get(this.webService.apiUrls().getAllCountryUrl, this.webService.jwt()).map((res: Response) => {
      return res.json();
    });
  }
  
  logOut() {
    this._setIsAuth(false);
    window.localStorage.removeItem('currentUser');
    this.sharedService.setCurrentUserRole();
    this.router.navigate(['/login']);
  }
  
  currentUserData() {
    return JSON.parse(window.localStorage.getItem('currentUser'));
  }
  
  private _checkIsAuth() {
    let currentUser = JSON.parse(window.localStorage.getItem('currentUser'));
    if (currentUser) {
      this._setIsAuth(true);
    } else {
      this.logOut();
    }
  }
  
}
