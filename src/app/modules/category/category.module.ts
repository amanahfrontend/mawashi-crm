import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../shared/shared.module';
import { RouterModule, Routes } from '@angular/router';
import { TranslationModule } from '../translation.module';
// import {StockComponent} from '../../pages/stock/stock.component';
import { CategoryComponent } from '../stock/category/category.component';
import { CategoryService } from '../../services/category.service';
// import {loginComponent} from '../../pages/login/login.component';
// import {ActiveCategoryComponent} from './active-category/active-category.component';
// import {DeactiveCategoryComponent} from './deactive-category/deactive-category.component';
import { CategoryTableComponent } from './category-table/category-table.component';
import { AddEditCategoryModalComponent } from './add-edit-category-modal/add-edit-category-modal.component';
import { MatRadioModule } from '@angular/material/radio';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';

const routes: Routes = [
  {
    path: '', component: CategoryComponent
  },
];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
    CommonModule,
    SharedModule,
    TranslationModule,
    MatRadioModule,
    MatProgressSpinnerModule
  ],
  declarations: [CategoryComponent, CategoryTableComponent, AddEditCategoryModalComponent],
  entryComponents: [AddEditCategoryModalComponent],
  providers: [CategoryService]
})
export class CategoryModule {
}
