import {Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {CategoryService} from '../../../services/category.service';
import {MatDialog} from '@angular/material';
import {AddEditCategoryModalComponent} from '../add-edit-category-modal/add-edit-category-modal.component';

// import {Subscription} from 'rxjs/Subscription';

@Component({
  selector: 'app-category-table',
  templateUrl: './category-table.component.html',
  styleUrls: ['./category-table.component.scss']
})
export class CategoryTableComponent implements OnInit, OnDestroy {
  // currentCategoryStateSubscription: Subscription;
  @Input() categoryList: any[];
  @Output() updated = new EventEmitter();
  
  constructor(private categoryService: CategoryService, private dialog: MatDialog) {
  }
  
  ngOnInit() {
  }
  
  ngOnDestroy() {
  }
  
  setCategoryActive(id) {
    this.categoryService.setCategoryActive(id)
      .subscribe((res) => {
        if (res.isSucceeded) {
          this.updated.emit();
        }
      });
  }
  
  setCategoryDeactive(id) {
    this.categoryService.setCategoryDeActive(id)
      .subscribe((res) => {
        if (res.isSucceeded) {
          this.updated.emit();
        }
      });
  }
  
  openAddCategoryModal() {
    console.log('open add modal');
    let dialogRef = this.dialog.open(AddEditCategoryModalComponent, {
      width: '500px',
      height: '600px',
      data: {}
    });
    dialogRef.afterClosed()
      .subscribe((result) => {
        if (result) {
          this.updated.emit();
        }
      });
  }
  
  openEditCategoryModal(category) {
    let dialogRef = this.dialog.open(AddEditCategoryModalComponent, {
      width: '500px',
      height: '600px',
      data: category
    });
    dialogRef.afterClosed()
      .subscribe((result) => {
        if (result) {
          this.updated.emit();
        }
      });
  }
  
  deleteCategory(id) {
    this.categoryService.deleteCategory(id)
      .subscribe((res) => {
          console.log('res');
          this.updated.emit();
        },
        err => {
        
        });
    console.log('delete');
  }
  
}
