import { Component, OnInit } from '@angular/core';
// import {InvoiceService} from '../../../services/invoice.service';
import { SharedService } from '../../../services/shared.service';
import { OrdersService } from '../../../services/ordersService';

@Component({
  selector: 'app-invoices',
  templateUrl: './invoices.component.html',
  styleUrls: ['./invoices.component.scss']
})
export class InvoicesComponent implements OnInit {
  pageSize: number;
  count: number;
  filteredInvoices: any[] = [];
  allInvoices: any[] = [];
  currentFilterTerms: any = {};
  printOrder: any;
  togglePrintBackground: boolean;
  isPrintAll: boolean;
  printData: any[];
  allOrdersForPrint: any;
  countyName: any;
  showAll: boolean = false;
  constructor(private sharedService: SharedService, private ordersService: OrdersService) {
  }

  ngOnInit() {

  }

  pageEvent(e) {
    console.log(e);
    this.currentFilterTerms.pageInfo.pageNo = e.pageIndex + 1;
    this.getInvoices(this.currentFilterTerms);
  }

  /*exportInvoices() {
    let exportData = [];
    exportData.push({
      'Order Code': 'Order Code',
      'Delivery Type': 'Delivery Type',
      'Delivery Date': 'Delivery Date',
      'Payment Message': 'Payment Message',
      'Platform': 'Platform',
      'Price': 'Price',
    });
    this.filteredInvoices.map((item) => {
      exportData.push({
        'Order Code': item.code,
        'Delivery Type': item.deliveryType,
        'Delivery Date': new Date(item.deliveryDate).toDateString(),
        'Payment Message': item.paymentMessage,
        'Platform': item.platform || '-',
        'Price': item.price
      });
    });
    return new Angular2Csv(exportData, 'Invoices', {
      showLabels: true
    });
  }*/


  getInvoices(filterObj) {

    console.log(filterObj);
    this.currentFilterTerms = filterObj;
    this.sharedService.getFilteredInvoices(filterObj)
      .subscribe((res) => {
        console.log(res.data);
        if (res.data != null && res.data != undefined) {
          this.allInvoices = res.data.data;
          this.pageSize = res.data.pageSize;
          this.count = res.data.count;
          this.filteredInvoices = this.allInvoices.slice();
          if (this.filteredInvoices != undefined)
            this.showAll = true;
        }
      });
  }


  printInvoice(order) {
    this.printOrder = order;
    this.togglePrintBackground = true;
    var newWin = window.open('', '_blank', 'top=0,left=0,height=500px,width=500px');
    console.log(this.printOrder)
    this.ordersService.getOrderDetailsByCartId(this.printOrder.fK_Cart_Id)
      .subscribe((res) => {

        this.printOrder.items = res.data.cartItems;

        this.printOrder.items.map((item) => {
          item.price = item.price;
        });
        console.log(this.printOrder);
        this.printOrder.cartPrice = this.printOrder.cartPrice;
        this.printOrder.cartVAT = this.printOrder.cartVAT;
        this.printOrder.price = this.printOrder.price;
        console.log(newWin);

        setTimeout(() => {

          var divToPrint = document.getElementById('invoice-container');
          newWin.document.write(divToPrint.outerHTML);
          newWin.print();
          newWin.close();
          this.togglePrintBackground = false;
        }, 500);
      });
  }

  printAll() {
    this.printData = this.currentFilterTerms.filters;
    let currentUser = JSON.parse(localStorage.getItem('currentUser'));
    this.countyName = currentUser.country_Code;
    this.isPrintAll = true;
    var newWin = window.open('', '_blank', 'top=0,left=0,height=500px,width=500px');
    this.ordersService.printAllOrders(this.printData)
      .subscribe((res) => {
        this.allOrdersForPrint = res.data;
        setTimeout(() => {
          var divToPrint = document.getElementById('invoice-PrintAll');
          newWin.document.write(divToPrint.outerHTML);
          newWin.print();
          newWin.close();
          this.isPrintAll = false;
        }, 500);
      });
  }


}
