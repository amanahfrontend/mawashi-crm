import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
// import {ComplainsComponent} from '../complains/complains/complains.component';
import {RouterModule, Routes} from '@angular/router';
import { InvoicesComponent } from './invoices/invoices.component';
import {SharedModule} from '../shared/shared.module';
import {InvoiceService} from '../../services/invoice.service';
import {TranslationModule} from '../translation.module';


const routes: Routes = [
  {path: '', component: InvoicesComponent},
];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
    CommonModule,
    SharedModule,
    TranslationModule
  ],
  declarations: [InvoicesComponent],
  providers: []
})
export class InvoicesModule { }
