import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {ordersComponent} from '../pages/orders/orders.component';
import {addOrderComponent} from '../pages/orders/addOrder/addOrder.component';
import {TranslationModule} from './translation.module';
import {OrderDetailsComponent} from '../pages/orders/order-details/order-details.component';
import {SharedModule} from './shared/shared.module';
import {OrdersFilterService} from '../services/orders-filter.service';

const routes: Routes = [
  {path: '', component: ordersComponent},
  {path: ':cartId', component: OrderDetailsComponent},
  {path: 'addOrder', component: addOrderComponent},
];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
    CommonModule,
    SharedModule,
    TranslationModule
  ],
  declarations: [ordersComponent, addOrderComponent, OrderDetailsComponent],
  providers: [OrdersFilterService]
})
export class OrdersModule {
}
