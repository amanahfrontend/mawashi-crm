import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
// import {homeComponent} from '../../pages/home/home.component';
import {TranslationModule} from '../translation.module';
import {accountsComponent} from '../../pages/accounts/accounts.component';
import {createAccountComponent} from '../../pages/accounts/createAccount/createAccount.component';
import {accountDetailsComponent} from '../../pages/accounts/accountDetails/accountDetails.component';
import {SharedModule} from '../shared/shared.module';
import {AccountsService} from '../../services/accounts.service';
import {CreateCustomerFormComponent} from './create-customer-form/create-customer-form.component';
import {OrdersTableComponent} from '../shared/orders-table/orders-table.component';
import {ComplainsTableComponent} from './complains-table/complains-table.component';
import {AddComplainComponent} from '../../pages/accounts/complains/addComplain/addComplain.component';
import {CustomerInfoComponent} from '../../pages/accounts/customer-info/customer-info.component';
import { ForgetPasswordModalComponent } from './forget-password-modal/forget-password-modal.component';

const routes: Routes = [
  {path: '', component: accountsComponent},
  {path: 'createAccount', component: createAccountComponent},
  {
    path: ':accountId', component: accountDetailsComponent, children: [
      {path: '', component: CustomerInfoComponent},
      {path: 'orders', component: OrdersTableComponent},
      {path: 'complains', component: ComplainsTableComponent},
    ]
  },
];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
    CommonModule,
    SharedModule,
    TranslationModule
  ],
  declarations: [accountsComponent, createAccountComponent, accountDetailsComponent, CreateCustomerFormComponent, AddComplainComponent, CustomerInfoComponent,ComplainsTableComponent, ForgetPasswordModalComponent],
  entryComponents: [AddComplainComponent, ForgetPasswordModalComponent],
  providers: [AccountsService]
})
export class AccountsModule {
}
