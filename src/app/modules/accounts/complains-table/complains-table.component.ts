import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {AccountsService} from '../../../services/accounts.service';
import {Subscription} from 'rxjs/Subscription';
import {ActivatedRoute, Params} from '@angular/router';
import {AddComplainComponent} from '../../../pages/accounts/complains/addComplain/addComplain.component';
import {MatDialog, MatDialogRef} from '@angular/material';

@Component({
  selector: 'app-complains-table',
  templateUrl: './complains-table.component.html',
  styleUrls: ['./complains-table.component.scss']
})
export class ComplainsTableComponent implements OnInit, OnDestroy {
  @Input() complains: any[] = [];
  getCustomerComplainsSubscription: Subscription;
  addComplainDialog: MatDialogRef<AddComplainComponent>;
  accountId: any;
  pageSize: number = 10;
  count: number;
  
  constructor(private accountService: AccountsService, private activatedRoute: ActivatedRoute, private dialog: MatDialog) {
  }
  
  ngOnInit() {
    this.activatedRoute.params
      .subscribe((params: Params) => {
          this.accountId = params.accountId;
          this.getCustomerComplains();
        },
        err => {
          alert('server error');
        });
  }
  
  ngOnDestroy() {
    this.getCustomerComplainsSubscription.unsubscribe();
  }
  
  pageEvent(e) {
    this.getCustomerComplains(++e.pageIndex);
  }
  
  getCustomerComplains(pageNumber = 1) {
    this.getCustomerComplainsSubscription = this.accountService.getAllCustomerComplains(this.accountId, {
      pageNo: pageNumber,
      pageSize: this.pageSize
    })
      .subscribe((res) => {
          console.log(res);
          console.log(res.data.data);
          this.count = res.data.count;
          this.complains = res.data.data;
        },
        err => {
          alert('Server has error');
        });
  }
  
  addComplain() {
    console.log('add');
    this.addComplainDialog = this.dialog.open(AddComplainComponent, {
      width: '400px',
      data: {accountId: this.accountId}
    });
    this.addComplainDialog.afterClosed().subscribe(result => {
      // this.addComplainDialog = null;
      console.log(result);
    });
  }
}
