import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {Location} from '@angular/common';
import {Subscription} from 'rxjs/Subscription';
import {AccountsService} from '../../../services/accounts.service';
import {UtilitiesService} from '../../../services/utilities.service';
import {ActivatedRoute, Params} from '@angular/router';
import {SharedService} from '../../../services/shared.service';
// import {ActivatedRoute, Params} from '@angular/router';

// declare let $: any;

@Component({
  selector: 'app-create-customer-form',
  templateUrl: './create-customer-form.component.html',
  styleUrls: ['./create-customer-form.component.scss']
})
export class CreateCustomerFormComponent implements OnInit, OnDestroy {
  @Input() accountId: number;
  @Input() type: string;
  customerData: any = {};
  getAllCountriesSubscription: Subscription;
  postCustomerDataSubscription: Subscription;
  allCountries: any[];
  toggleLoading: boolean;
  toggleDisableForm: boolean;
  isExistedAccount: boolean;
  typeApp;
  typeSales;
  phoneRegex;
  passwordRegex;

  constructor(private accountsService: AccountsService, private utilitiesService: UtilitiesService, private activatedRoute: ActivatedRoute, private sharedService: SharedService, private location: Location) {
  }

  back() {
    this.location.back();
  }

  ngOnInit() {
    console.log("company data");
    console.log(this.accountId);
    this.toggleLoading = true;
    this.phoneRegex = this.utilitiesService.phoneRegex;
    this.passwordRegex = this.utilitiesService.passwordRegex;
    this.activatedRoute.params
      .subscribe((params: Params) => {
        if (params.type == 'app') {
          this.typeApp = true;
          this.typeSales = false;
        } else if (params.type == 'sales') {
          this.typeApp = false;
          this.typeSales = true;
        }
      });
    if (this.accountId) {
      this.typeApp = this.type == 'app';
      this.typeSales = this.type == 'sales';
      if (this.typeApp) {
        this.accountsService.getAccountDetails(this.accountId)
          .subscribe((res) => {
              console.log(res);
              this.customerData = res.data;

              this.toggleDisableForm = true;
              this.isExistedAccount = true;
              this.toggleLoading = false;
            },
            err => {
              this.toggleLoading = false;
            });
      } else if (this.typeSales) {
        this.accountsService.getCompanyDetailsById(this.accountId)
          .subscribe((res) => {
              console.log(res);
              this.customerData = res.data;
              console.log(this.customerData);

              this.toggleDisableForm = true;
              this.isExistedAccount = true;
              this.toggleLoading = false;
            },
            err => {
              this.toggleLoading = false;
            });
      }
    }
    else {
      this.customerData = {};
      this.toggleLoading = false;
    }
    this.getAllCountries();
  }

  ngOnDestroy() {
    this.getAllCountriesSubscription && this.getAllCountriesSubscription.unsubscribe();
    this.postCustomerDataSubscription && this.postCustomerDataSubscription.unsubscribe();
  }

  toggleEditMode() {
    this.toggleDisableForm = !this.toggleDisableForm;
  }

  postCustomerData() {
    this.customerData.countryCode = this.sharedService.currentUserData().country_Code;
    this.customerData.fk_Country_Id = this.sharedService.currentUserData().fk_Country_Id;
    console.log(this.customerData);
    this.toggleLoading = true;

    if (this.isExistedAccount) {
      this.customerData.pictureUrl && delete this.customerData.pictureUrl;
      this.customerData.picture && delete this.customerData.picture;

      if (this.typeApp) {
        this.postCustomerDataSubscription = this.accountsService.updateCustomerData(this.customerData)
          .subscribe(() => {
              console.log('success');
              this.toggleLoading = false;
            },
            err => {
              console.log('failed');
              this.toggleLoading = false;
            });
      } else if (this.typeSales) {
        this.accountsService.updateCompany(this.customerData)
          .subscribe((res) => {
            if (res.isSucceeded) {
              console.log('success');
              this.toggleLoading = false;
            }
          });
      }
    } else {
      if (this.typeApp) {
        this.postCustomerDataSubscription = this.accountsService.postNewCustomerData(this.customerData)
          .subscribe(() => {
              console.log('success');
              this.toggleLoading = false;
              this.back();
            },
            err => {
              this.toggleLoading = false;
              console.log(err);
            });
      } else if (this.typeSales) {
        // Remove this section and enable what i remove from html after launch mawashi business isA
        // Start Section
        this.customerData.password = '123456';
        let dummyPhone = new Date();
        this.customerData.phone1 = dummyPhone.getTime();
        this.customerData.email = this.customerData.userName + '@CompanyAlMawashi.com';
        this.customerData.name = this.customerData.userName;
        this.customerData.contactName = this.customerData.userName;
        this.customerData.contactPhone = dummyPhone.getTime();
        this.customerData.contactMail = this.customerData.userName + '@CompanyAlMawashi.com';
        // End Of Section
        console.log(this.customerData);
        this.postCustomerDataSubscription = this.accountsService.postNewCompanyData(this.customerData)
          .subscribe(() => {
              console.log('success');
              this.toggleLoading = false;
              this.back();
            },
            err => {
              this.toggleLoading = false;
              console.log(err);
            });
      }
    }
  }

  getAllCountries() {
    this.getAllCountriesSubscription = this.utilitiesService.getAllCountries()
      .subscribe((res) => {
          this.allCountries = res;
        },
        err => {
          console.log(err);
        });
  }

}
