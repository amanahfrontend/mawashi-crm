import {Component, Inject, Input, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {AccountsService} from '../../../services/accounts.service';

@Component({
  selector: 'app-forget-password-modal',
  templateUrl: './forget-password-modal.component.html',
  styleUrls: ['./forget-password-modal.component.scss']
})

export class ForgetPasswordModalComponent implements OnInit {
  @Input() userName;
  newPassword: string;
  toggleLoading: boolean;
  
  constructor(public dialogRef: MatDialogRef<ForgetPasswordModalComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any, private accountsService: AccountsService) {
  }
  
  ngOnInit() {
  }
  
  closeModal() {
    this.dialogRef.close();
  }
  
  saveNewPassword() {
    this.toggleLoading = true;
    let dataObj = {
      newPassword: this.newPassword,
      username: this.data.userName
    };
    console.log(dataObj);
    this.accountsService.resetPassword(dataObj)
      .subscribe(() => {
          this.toggleLoading = false;
          this.closeModal();
        },
        err => {
          this.toggleLoading = false;
        });
  }
  
}
