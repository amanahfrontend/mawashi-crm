import {Component, OnInit, Input} from '@angular/core';

@Component({
  selector: 'app-form-required-error',
  templateUrl: './form-required-error.component.html',
  styleUrls: ['./form-required-error.component.scss']
})
export class FormRequiredErrorComponent implements OnInit {
  @Input() body = 'Please Fill Required Fields to Save!';
  
  constructor() {
  }
  
  ngOnInit() {
  
  }
  
}
