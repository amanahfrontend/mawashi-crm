import {Component, Inject, OnInit, Input} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material';
// import {AddComplainComponent} from '../../../pages/accounts/complains/addComplain/addComplain.component';
import {OrdersService} from '../../../services/ordersService';
import {PromptDialogComponent} from '../prompt-dialog/prompt-dialog.component';
import {AuthService} from '../../../services/auth.service';

// import {exists} from 'fs';

@Component({
  selector: 'app-create-order-modal',
  templateUrl: './create-order-modal.component.html',
  styleUrls: ['./create-order-modal.component.scss']
})
export class CreateOrderModalComponent implements OnInit {
  toggleLoading: boolean;
  /*cart view variables*/
  cartItems: any = this.data.cartItems;
  activeCategories: any = [];
  activeTab: number;

  /*order view variables*/
  orderObj: any = {};
  deliverTypes = [];
  paymentMethods: any[] = [];
  locations: any[] = [];
  deliveryFees: number = 0;
  currency: any;
  categoryConfigId: any;
  dates: any[];
  typeApp: boolean;
  typeSales: boolean;
  isExistedOrder: boolean;
  toggleError: boolean;
  measurmentTypes: any[];

  constructor(public dialogRef: MatDialogRef<CreateOrderModalComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any, private orderService: OrdersService, private dialog: MatDialog, private authService: AuthService) {
  }

  ngOnInit() {
    this.isExistedOrder = this.data.cartItems[0].fk_Item_Id;
    console.log(this.data);
    console.log('cartItems');
    console.log(this.cartItems);
    if (!this.isExistedOrder) {
      this.toggleLoading = true;
    } else {
      this.collectCartItemsData(this.cartItems, 0);
    }
    this.activeTab = 1;
    // this.orderViewInit();
    this.typeApp = this.data.type == 'app';
    this.typeSales = this.data.type == 'sales';
    this.getAllActiveCategory();
    this.getMeasurements();
  }

  // /***************************/
  // /******* cart view *********/
  // /***************************/


  getAllActiveCategory() {
    if (this.typeApp) {
      this.orderService.getAllActiveCategory()
        .subscribe((res) => {
          console.log(res.data);
          this.activeCategories = res.data;
          this.toggleLoading = false;
        });
    } else if (this.typeSales) {
      this.orderService.getAllSalesActiveCategory()
        .subscribe((res) => {
          console.log(res.data);
          this.activeCategories = res.data;
          this.toggleLoading = false;
        });
    }
  }

  collectCartItemsData(listOfItems, i) {
    // console.log(i);
    // console.log(listOfItems.length - 1);
    if (i <= listOfItems.length - 1) {
      this.getItemsByCategoryId(listOfItems[i])
        .then((items) => {
          listOfItems[i].items = items;
          this.getItemDetails(listOfItems[i])
            .then((itemDetails: any) => {
              listOfItems[i].quantities = itemDetails.quantities;
              listOfItems[i].cuttings = itemDetails.cuttings;
              // console.log(listOfItems[i]);
              this.collectCartItemsData(listOfItems, i + 1);
            });
        });
    } else {

    }
  }

  getItemsByCategoryId(cartItem) {
    return new Promise((resolve, reject) => {
      this.orderService.getActiveItemsByCategoryId(cartItem.categoryId)
        .subscribe((res) => {
            // console.log(res.data);
            const items = res.data;
            cartItem.items = items;
            console.log(this.typeApp);
            console.log('items');
            debugger;
            if (this.typeApp) {
              this.orderService.getDeactiveItemsByCategoryId(cartItem.categoryId)
                .subscribe((response) => {
                    // console.log(res.data);
                    const deactiveItems = response.data;
                    if (deactiveItems && deactiveItems.length > 0) {
                      cartItem.items.push.apply(cartItem.items, deactiveItems);
                    }
                    console.log('shshshshshshh');
                    resolve(items);
                  },
                  err => {
                    reject();
                  });
            } else {
              resolve(items);
            }
          },
          err => {
            reject();
          });
    });
  }

  getItemDetails(cartItem) {
    console.log(cartItem.fk_Item_Id);
    return new Promise((resolve, reject) => {
      this.orderService.getItemDetails(cartItem.fk_Item_Id)
        .subscribe((res) => {
            // console.log(res.data);
            cartItem.cuttings = res.data.cuttings;
            cartItem.quantities = res.data.quantities;
            // console.log(cartItem.quantities);
            if (this.isExistedOrder) {
              resolve({
                quantities: cartItem.quantities,
                cuttings: cartItem.cuttings
              });
            } else {
              cartItem.fK_Quantity_Id = undefined;
              delete cartItem.fK_Cutting_Id;
            }
          },
          err => {
            reject();
          });
    });
  }

  getActiveItemsByCategoryId(cartItem) {
    this.activeCategories.map((category) => {
      if (category.id == cartItem.categoryId) {
        cartItem.fK_CategoryConfig_Id = category.config.id;
        cartItem.options = category.config.options;
        cartItem.acceptMulti = category.config.acceptMulti;
        cartItem.fk_Item_Id = undefined;
        delete cartItem.fK_Option_Id;
        cartItem.fK_Quantity_Id = undefined;
        delete cartItem.fK_Cutting_Id;
      }
    });
    if (!cartItem.acceptMulti && this.cartItems.length > 1 && this.cartItems[0].acceptMulti) {
      this.showPrompt()
        .then(() => {
          this.cartItems = [cartItem];
        })
        .catch(() => {
          this.cartItems.map((item, i) => {
            if (!item.fk_Item_Id) {
              this.cartItems[i] = {};
            }
          });
        });
    }
    else if (cartItem.acceptMulti && this.cartItems.length > 1 && !this.cartItems[0].acceptMulti) {
      this.showPrompt()
        .then(() => {
          this.cartItems = this.cartItems.filter((singleCartItem) => {
            return singleCartItem.acceptMulti;
          });
        })
        .catch(() => {
          this.cartItems.map((item, i) => {
            if (!item.fk_Item_Id) {
              this.cartItems[i] = {};
            }
          });
        });
    }
    else if (!cartItem.acceptMulti && this.cartItems.length > 1 && !this.cartItems[0].acceptMulti && this.cartItems[0].categoryId != cartItem.categoryId) {
      this.showPrompt()
        .then(() => {
          this.cartItems = [cartItem];
        })
        .catch(() => {
          this.cartItems.map((item, i) => {
            if (!item.fk_Item_Id) {
              this.cartItems[i] = {};
            }
          });
        });
    }
    console.log(cartItem);
    if (cartItem.categoryId) {
      this.orderService.getActiveItemsByCategoryId(cartItem.categoryId)
        .subscribe((res) => {
            console.log(res.data);
            cartItem.items = res.data;
            if (this.typeApp) {
              this.orderService.getDeactiveItemsByCategoryId(cartItem.categoryId)
                .subscribe((response) => {
                    // console.log(res.data);
                    const deactiveItems = response.data;
                    if (deactiveItems && deactiveItems.length > 0) {
                      cartItem.items.push.apply(cartItem.items, deactiveItems);
                    }
                  },
                  err => {
                    console.log('error');
                  });
            }
          },
        );
    }
    console.log(cartItem);
  }

  getMeasurements() {
    this.orderService.getMeasurements()
      .subscribe((res) => {
        console.log(res);
        this.measurmentTypes = res;
      });
  }

  addCartItem() {
    let lastCartItem = this.cartItems[this.cartItems.length - 1];
    if (this.typeApp) {
      if (lastCartItem.fK_Quantity_Id && lastCartItem.acceptMulti) {
        this.cartItems.push({});
      } else if (lastCartItem.fK_Quantity_Id && !lastCartItem.acceptMulti) {
        this.cartItems.push({
          categoryId: lastCartItem.categoryId,
          fK_CategoryConfig_Id: lastCartItem.fK_CategoryConfig_Id,
          options: lastCartItem.options,
          acceptMulti: lastCartItem.acceptMulti,
          items: lastCartItem.items,
          quantities: lastCartItem.quantities,
        });
        if (this.cartItems[this.cartItems.length - 1].cuttings) {
          this.cartItems[this.cartItems.length - 1].cuttings = lastCartItem.cuttings;
        }
      }
    } else if (this.typeSales) {
      this.cartItems.push({});
    }
  }

  removeCartItem(index) {
    this.cartItems.splice(index, 1);
  }

  getCurrentCart() {
    return new Promise((resolve, reject) => {
      if (this.typeApp) {
        this.orderService.getCurrentCart(this.data.accountId)
          .subscribe((res) => {
            console.log(res);
            let id;
            if (res.data) {
              id = res.data.id;
            }
            resolve(id);
          });
      } else if (this.typeSales) {
        this.orderService.getCurrentSalesCart(this.data.accountId)
          .subscribe((res) => {
            console.log(res);
            let id;
            if (res.data) {
              id = res.data.id;
            }
            resolve(id);
          });
      }
    });
  }

  saveCart() {
    // console.log(this.cartItems);
    this.toggleLoading = true;
    let cartItemsToPost: any = {
      fK_Customer_Id: this.data.accountId,
      cartItems: [],
      id: null
    };
    // if () {
    //
    // }
    this.cartItems.map((item, i) => {
      if (this.typeApp) {
        cartItemsToPost.cartItems[i] = {
          fk_Item_Id: item.fk_Item_Id,
          fK_Quantity_Id: item.fK_Quantity_Id,
          fK_CategoryConfig_Id: item.fK_CategoryConfig_Id,
          salesPricePerItem: item.salesPricePerItem,
        };
      } else if (this.typeSales) {
        cartItemsToPost.cartItems[i] = {
          fk_Item_Id: item.fk_Item_Id,
          quantity: item.quantity,
          measurmentType: item.measurmentType,
          fK_CategoryConfig_Id: item.fK_CategoryConfig_Id,
          salesPricePerItem: item.salesPricePerItem,
        };
      }

      if (item.fK_Option_Id) {
        cartItemsToPost.cartItems[i].fK_Option_Id = item.fK_Option_Id;
      }
      if (item.fK_Cutting_Id) {
        cartItemsToPost.cartItems[i].fK_Cutting_Id = item.fK_Cutting_Id;
      }
      if (item.price) {
        cartItemsToPost.cartItems[i].price = item.price;
      }
      if (item.itemQuantityByKG) {
        cartItemsToPost.cartItems[i].itemQuantityByKG = item.itemQuantityByKG;
      }
    });
    console.log(cartItemsToPost);
    cartItemsToPost.fK_Customer_Id = this.data.accountId;
    if (this.isExistedOrder) {
      delete cartItemsToPost.fK_Customer_Id;
      cartItemsToPost.fK_Order_Id = this.data.orderId;
      cartItemsToPost.cart = {cartItems: cartItemsToPost.cartItems.slice()};
      delete cartItemsToPost.cartItems;
      console.log(cartItemsToPost);
      this.getCurrentCart()
        .then((cartId) => {
          if (cartId) {
            cartItemsToPost.id = cartId;
          } else {
            delete cartItemsToPost.id;
          }
          console.log(this.typeSales);
          if (this.typeApp) {
            console.log('will post');
            this.orderService.updateCart(cartItemsToPost)
              .subscribe(() => {
                  console.log('success');
                  // this.orderViewInit();
                  this.closeDialog(true);
                },
                err => {

                });
          } else if (this.typeSales) {
            this.updateOrder();

            // this.orderService.submitSalesCart(cartItemsToPost)
            //   .subscribe(() => {
            //     console.log('success');
            //     // this.orderViewInit();
            //     this.closeDialog(true);
            //   });
          }
        });
    }
    else if (!this.isExistedOrder) {
      this.getCurrentCart()
        .then((cartId) => {
          if (cartId) {
            cartItemsToPost.id = cartId;
          } else {
            delete cartItemsToPost.id;
          }
          if (this.typeApp) {
            console.log('will post');
            this.orderService.submitCart(cartItemsToPost)
              .subscribe((res) => {
                  console.log('success');
                  // this.orderViewInit();
                  if (res.isSucceeded) {
                    this.orderViewInit();
                  } else {
                    this.toggleLoading = false;
                    this.toggleError = true;
                  }
                },
                err => {

                });
          } else if (this.typeSales) {
            this.orderService.submitSalesCart(cartItemsToPost)
              .subscribe((res) => {
                console.log('success');
                // this.orderViewInit();
                if (res.isSucceeded) {
                  this.orderViewInit();
                } else {
                  this.toggleLoading = false;
                  this.toggleError = true;
                }
              });
          }
        });
    }
  }

  /*Order view ( Tab 2)*/

  orderViewInit() {
    this.activeTab = 2;
    this.currency = this.authService.currentUserData().currency;
    // this.currency = localStorage.getItem('currentUser').currency;
    this.categoryConfigId = this.cartItems[0].fK_CategoryConfig_Id;
    this.getDeliveryTypes();
    this.getDeliveryFees();
  }

  getDeliveryTypes() {
    // let testCategoryConfig = 5;
    let testCategoryConfig = this.cartItems[0].fK_CategoryConfig_Id;
    this.orderService.getDeliveryTypes(testCategoryConfig)
      .subscribe((res) => {
        console.log(res);
        this.deliverTypes = res.data;
        if (this.typeSales) {
          this.deliverTypes.map((type) => {
            if (type.name.toLowerCase().includes('home')) {
              this.orderObj.fK_DeliveryType_Id = type.id;
            }
          });
          this.getPaymentMethodsAndLocationDate();
        }
        console.log(this.deliverTypes);
        this.toggleLoading = false;
      });
  }

  getPaymentMethodsAndLocationDate() {
    console.log('payment method');
    if (this.typeApp) {
      if (!this.orderObj.paymentTypeCode) {
        this.orderService.getPaymentMethods(this.orderObj.fK_DeliveryType_Id)
          .subscribe((res) => {
            console.log(res);
            this.paymentMethods = res.data;
            if (this.typeApp) {
              this.paymentMethods.map((method) => {
                if (method.code.toLowerCase().includes('cash')) {
                  this.orderObj.paymentTypeCode = method.code;
                }
              });
            }
          });
      }
    } else if (this.typeSales) {
      this.orderService.getSalesPaymentMethod()
        .subscribe((res) => {
          console.log(res);
          this.paymentMethods = res.data;
        });
    }
    this.getLocationDates();
  }

  getLocationDates() {
    // console.log('get addresses');
    if (this.typeApp) {
      let deliveryData = {
        deliveryType: this.orderObj.fK_DeliveryType_Id,
        fK_Customer_Id: this.data.accountId,
        fK_Config_Id: this.categoryConfigId
      };
      this.orderService.getLocationDates(deliveryData)
        .subscribe((res) => {
          console.log(res);
          if (res.data) {
            this.dates = res.data.dates || [];
            this.locations = res.data.locations;
          }
        });
    } else if (this.typeSales) {
      this.orderService.getCompanyAddress(this.data.accountId)
        .subscribe((res) => {
          console.log(res);
          this.locations = res.data;
        });
    }
  }

  getDeliveryFees() {
    this.orderService.getDeliveryFees()
      .subscribe((res) => {
        console.log(res);
        this.dates = [];
        this.deliveryFees = res.data;
      });
  }

  dateFormat(day) {
    let todayDate = new Date(day);
    let dd = todayDate.getDate().toString();
    let mm = (todayDate.getMonth() + 1).toString();

    let yyyy = todayDate.getFullYear();
    if (Number(dd) < 10) {
      dd = '0' + dd.toString();
    }
    if (Number(mm) < 10) {
      mm = '0' + mm.toString();
    }
    return yyyy + '-' + mm + '-' + dd + 'T00:00:00.00';
  }

  submitOrder() {
    this.orderObj.fK_Customer_Id = +this.data.accountId;
    this.orderObj.fK_DeliveryType_Id = +this.orderObj.fK_DeliveryType_Id;
    this.orderObj.fk_Location_Id = +this.orderObj.fk_Location_Id;
    console.log(this.orderObj);
    if (!this.dates.length) {
      this.orderObj.deliveryDate = this.dateFormat(this.orderObj.deliveryDate.toISOString());
    }
    this.getCurrentCart()
      .then((cartId) => {
        this.orderObj.fK_Cart_Id = cartId;
        console.log(this.orderObj);
        if (this.typeApp) {
          this.orderService.submitOrder(this.orderObj)
            .subscribe(() => {
              this.dialogRef.close(true);
            });
        } else if (this.typeSales) {
          let currentUser = JSON.parse(localStorage.getItem('currentUser'));
          this.orderObj.createdUserName = currentUser.name;
          this.orderObj.createdUserId = currentUser.userId;
          this.orderService.submitCompanyOrder(this.orderObj)
            .subscribe(() => {
              this.dialogRef.close(true);
            });
        }
      });
  }

  updateOrder() {
    let updateThisOrder = {
      FK_Order_Id: this.data.orderId,
      cart: {
        cartItems: this.cartItems
      }
    };
    console.log(updateThisOrder);
    if (this.typeSales) {
      this.orderService.updateSalesOrderItems(updateThisOrder)
        .subscribe(res => {
          if (res.isSucceeded) {
            this.dialogRef.close(true);
          } else {
            this.toggleLoading = false;
            this.toggleError = true;
          }
        });
    } else if (this.typeApp) {
      this.orderService.updateAppOrderItems(updateThisOrder)
        .subscribe(res => {
          if (res.isSucceeded) {
            this.dialogRef.close(true);
          } else {
            this.toggleLoading = false;
            this.toggleError = true;
          }
        });
    }
    // this.orderService.getOrdersDetailsById(this.isExistedOrder).subscribe(orderData => {
    //   console.log('update this order');
    //   console.log(orderData);
    // });

    // this.orderObj.fK_Customer_Id = +this.data.accountId;
    // this.orderObj.fK_DeliveryType_Id = +this.orderObj.fK_DeliveryType_Id;
    // this.orderObj.fk_Location_Id = +this.orderObj.fk_Location_Id;
    // console.log(this.orderObj);
    // if (!this.dates.length) {
    //   this.orderObj.deliveryDate = this.dateFormat(this.orderObj.deliveryDate.toISOString());
    // }
    // this.getCurrentCart()
    //   .then((cartId) => {
    //     this.orderObj.fK_Cart_Id = cartId;
    //     console.log(this.orderObj);
    //     if (this.typeApp) {
    //       this.orderService.updateAppOrderItems(this.orderObj)
    //         .subscribe(() => {
    //           this.dialogRef.close(true);
    //         });
    //     } else if (this.typeSales) {
    //       let currentUser = JSON.parse(localStorage.getItem('currentUser'));
    //       this.orderObj.createdUserName = currentUser.name;
    //       this.orderObj.createdUserId = currentUser.userId;
    //       console.log('update this order');
    //       console.log(this.orderObj);

    //     }
    //   });
  }

  showPrompt() {
    return new Promise((resolve, reject) => {
      let dialogRef = this.dialog.open(PromptDialogComponent, {
        data: {
          body: 'Adding This Category will delete the current selected category, please confirm ?',
          accept: 'Reset', cancel: 'No'
        }
      });
      dialogRef.afterClosed()
        .subscribe((result) => {
          console.log(result);
          if (result) {
            resolve();
          } else {
            reject();
          }
        });
    });
  }

  closeDialog(res = false) {
    this.dialogRef.close(res);
  }

}
