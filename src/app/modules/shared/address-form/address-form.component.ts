import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { SharedService } from '../../../services/shared.service';
import { MouseEvent } from '@agm/core';

// import {OrdersService} from '../../../services/ordersService';

@Component({
  selector: 'app-address-form',
  templateUrl: './address-form.component.html',
  styleUrls: ['./address-form.component.scss']
})
export class AddressFormComponent implements OnInit {
  @Input() addresses: any[];
  @Input() isSave: boolean;
  @Input() type: string;
  @Output() deleteAddress = new EventEmitter();
  @Output() submitAddress = new EventEmitter();
  adminType: boolean;
  customerData: any;
  addressPost: any = {};
  showAdress: boolean = false;
  showMap: boolean = false;
  lat: number = 29.378586;
  lng: number = 47.990341;
  selectedMarker: any;
  locationChosen: boolean = false;
  constructor(private sharedService: SharedService) {
  }

  mapClicked($event: MouseEvent) {
    this.lat = $event.coords.lat;
    this.lng = $event.coords.lng;
    this.locationChosen = true;
  }
  ngOnInit() {
    this.adminType = this.type == 'admin';
    this.customerData = this.sharedService.currentUserData();
    if (this.customerData.fk_Country_Id == 1) {
      this.lat = 29.378586;
      this.lng = 47.990341;
    }
    else if (this.customerData.fk_Country_Id == 2) {
      this.lat = 25.276987;
      this.lng = 55.296249;
    }
  }
  checkForPACIorMakany(address) {
    if (address.number && ((this.customerData.country_Code.toLowerCase() == 'uae' && address.number.length >= 10) || (this.customerData.country_Code.toLowerCase() == 'kuwait' && address.number.length >= 8))) {
      address.toggleLoading = true;
      this.sharedService.locationByPACIOrMakany(address.number)
        .subscribe((res) => {
          address.areaName = res.area.name;
          address.blockName = res.block.name;
          address.governorateName = res.governorate.name;
          address.latitude = res.lat;
          address.longitude = res.lng;
          address.toggleLoading = false;
        },
          err => {
            console.log(err);
            address.toggleLoading = false;
          });
    } else {
      address.toggleLoading = false;
    }
  }

  removeAddress(index, id) {
    this.deleteAddress.emit({ index: index, id: id });
  }

  saveAddress() {
    this.addressPost = this.addresses[this.addresses.length - 1];
    if (this.locationChosen) {
      this.addressPost.latitude = this.lat;
      this.addressPost.longitude = this.lng;
    }
    this.submitAddress.emit(this.addressPost);
  }

  showAddress(addressType) {
    if (addressType == "Address") {
      this.showAdress = true;
      this.showMap = false;
    }
    else if (addressType == "map") {
      this.showAdress = false;
      this.showMap = true;
    }

  }

  getAddressLat(address) {
    if (this.locationChosen)
      return this.lat;
    if (address.latitude == undefined) return this.lat;
    else return address.latitude;
  }

  getAddressLng(address) {
    if (this.locationChosen)
      return this.lng;
    if (address.longitude == undefined) return this.lng;
    else return address.longitude;
  }


}
