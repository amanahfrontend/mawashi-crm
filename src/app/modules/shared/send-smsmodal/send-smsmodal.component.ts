import {Component, Inject, OnInit} from '@angular/core';
import {AddEditAdminDatesModalComponent} from '../../administrations/add-edit-admin-dates-modal/add-edit-admin-dates-modal.component';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {SharedService} from '../../../services/shared.service';

@Component({
  selector: 'app-send-smsmodal',
  templateUrl: './send-smsmodal.component.html',
  styleUrls: ['./send-smsmodal.component.scss']
})
export class SendSmsmodalComponent implements OnInit {
  smsMessage: string;
  
  constructor(public dialogRef: MatDialogRef<AddEditAdminDatesModalComponent>, @Inject(MAT_DIALOG_DATA) public data: any, private sharedService: SharedService) {
  }
  
  ngOnInit() {
  }
  
  sendSMS() {
    this.sharedService.getAccountDetails(this.data.fK_Customer_Id)
      .subscribe((res) => {
        let customerDetails = res.data;
        let SMSobj = {
          message: this.smsMessage,
          phoneNumber: customerDetails.phone1
        };
        console.log(SMSobj);
        this.sharedService.postSMS(SMSobj)
          .subscribe((res) => {
            this.closeModal(true);
          });
        console.log(this.smsMessage);
      });
  }
  
  closeModal(res = false) {
    this.dialogRef.close(res);
  }
  
}
