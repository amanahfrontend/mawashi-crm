import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SendSmsmodalComponent } from './send-smsmodal.component';

describe('SendSmsmodalComponent', () => {
  let component: SendSmsmodalComponent;
  let fixture: ComponentFixture<SendSmsmodalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SendSmsmodalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SendSmsmodalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
