import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';

// import {CreateOrderModalComponent} from '../create-order-modal/create-order-modal.component';

@Component({
  selector: 'app-prompt-dialog',
  templateUrl: './prompt-dialog.component.html',
  styleUrls: ['./prompt-dialog.component.scss']
})
export class PromptDialogComponent implements OnInit {
  
  constructor(public dialogRef: MatDialogRef<PromptDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any) {
  }
  
  ngOnInit() {
    console.log(this.data);
  }
  
  close() {
    this.dialogRef.close(false);
  }
  
  accept() {
    this.dialogRef.close(true);
  }
  
}
