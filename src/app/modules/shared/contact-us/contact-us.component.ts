import {Component, OnDestroy, OnInit} from '@angular/core';
import {HomeService} from '../../../services/home.service';
import {Subscription} from 'rxjs/Subscription';

@Component({
  selector: 'app-contact-us',
  templateUrl: './contact-us.component.html',
  styleUrls: ['./contact-us.component.scss']
})
export class ContactUsComponent implements OnInit, OnDestroy {
  contactUsSubscription: Subscription;
  messages: any[];
  pageSize: number = 10;
  count: number;
  
  constructor(private homeService: HomeService) {
  }
  
  ngOnInit() {
    this.getContactUs();
  }
  
  ngOnDestroy() {
    this.contactUsSubscription.unsubscribe();
  }
  
  pageEvent(e) {
    console.log(e);
    this.getContactUs(++e.pageIndex);
  }
  
  getContactUs(pageNumber = 1) {
    this.contactUsSubscription = this.homeService.getContactUs({pageNo: pageNumber, pageSize: this.pageSize})
      .subscribe((res) => {
        console.log(res.data);
        this.count = res.data.count;
        this.messages = res.data.data;
      });
  }
  
}
