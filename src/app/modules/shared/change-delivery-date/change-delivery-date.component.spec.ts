import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChangeDeliveryDateComponent } from './change-delivery-date.component';

describe('ChangeDeliveryDateComponent', () => {
  let component: ChangeDeliveryDateComponent;
  let fixture: ComponentFixture<ChangeDeliveryDateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChangeDeliveryDateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChangeDeliveryDateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
