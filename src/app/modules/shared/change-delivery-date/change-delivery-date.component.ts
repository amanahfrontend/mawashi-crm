import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {HomeService} from '../../../services/home.service';
import {UtilitiesService} from '../../../services/utilities.service';

@Component({
  selector: 'app-change-delivery-date',
  templateUrl: './change-delivery-date.component.html',
  styleUrls: ['./change-delivery-date.component.scss']
})

export class ChangeDeliveryDateComponent implements OnInit {
  toggleLoading: boolean;
  
  constructor(public dialogRef: MatDialogRef<ChangeDeliveryDateComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any, private homeService: HomeService, private utilitiesService: UtilitiesService) {
  }
  
  ngOnInit() {
  }
  
  closeModal() {
    this.dialogRef.close();
  }
  
  saveDeliveryDate() {
    this.toggleLoading = true;
    let deliveryDateObj = Object.assign({}, this.data);
    deliveryDateObj.deliveryDate = this.utilitiesService.convertToLocalUTCLikeFormat(deliveryDateObj.deliveryDate);
    console.log(deliveryDateObj);
    this.homeService.postDeliveryDate(deliveryDateObj)
      .subscribe(() => {
          this.closeModal();
        },
        err => {
          this.toggleLoading = false;
        });
  }
  
}
