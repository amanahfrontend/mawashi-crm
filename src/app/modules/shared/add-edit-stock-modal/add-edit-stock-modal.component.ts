import {Component, OnInit, Inject, Input} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {CuttingService} from '../../../services/cutting.service';
import {QuantitiesService} from '../../../services/quantities.service';
// import {AddEditStockModalComponent} from '../create-order-modal/create-order-modal.component';

// import {Inject} from '@angular/compiler/src/core';

@Component({
  selector: 'app-add-edit-stock-modal',
  templateUrl: './add-edit-stock-modal.component.html',
  styleUrls: ['./add-edit-stock-modal.component.scss']
})
export class AddEditStockModalComponent implements OnInit {
  
  constructor(public dialogRef: MatDialogRef<AddEditStockModalComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any, private cuttingService: CuttingService, private quantityService: QuantitiesService) {
  }
  
  ngOnInit() {
    console.log(this.data);
  }
  
  saveStock(data) {
    console.log(this.data);
    if (this.data.operation == 'edit') {
      data.id = this.data.id;
      console.log('post edits');
      if (this.data.type == 'cutting') {
        this.cuttingService.updateCutting(data)
          .subscribe((res) => {
            console.log(res);
            this.closeDialog(true);
          });
      } else {
        this.quantityService.updateQuantity(data)
          .subscribe((res) => {
            console.log(res);
            this.closeDialog(true);
          });
      }
    } else {
      if (this.data.type == 'cutting') {
        console.log('add cutting');
        data.fK_VAT_Id = 1;
        this.cuttingService.addCutting(data)
          .subscribe((res) => {
            console.log(res);
            this.closeDialog(true);
          });
      } else {
        console.log('add quantity');
        this.quantityService.addQuantity(data)
          .subscribe((res) => {
            console.log(res);
            this.closeDialog(true);
          });
      }
    }
  }
  
  closeDialog(status = false) {
    this.dialogRef.close(status);
  }
  
}
