import {Component, EventEmitter, Input, OnChanges, OnInit, Output} from '@angular/core';
import {ActivatedRoute, Params} from '@angular/router';
import {OrdersService} from '../../../services/ordersService';
import {MatDialog} from '@angular/material';
import {CreateOrderModalComponent} from '../create-order-modal/create-order-modal.component';
import {ChangeStatusModalComponent} from '../../change-status-modal/change-status-modal.component';
// import {AccountsService} from '../../../services/accounts.service';
import {SharedService} from '../../../services/shared.service';
import {UtilitiesService} from '../../../services/utilities.service';
import {SendSmsmodalComponent} from '../send-smsmodal/send-smsmodal.component';
import {ChangeDeliveryDateComponent} from '../change-delivery-date/change-delivery-date.component';


// import {HomeService} from '../../../services/home.service';

@Component({
  selector: 'app-orders-table',
  templateUrl: './orders-table.component.html',
  styleUrls: ['./orders-table.component.css']
})

export class OrdersTableComponent implements OnInit, OnChanges {
  @Input() orders: any[];
  @Input() totalOrdersCount: number;
  allOrders: any[];
  ordersInCurrentPage: any[] = [];
  accountId: any;
  typeApp: boolean;
  type: string;
  typeSale: boolean;
  pageSize: number = 10;
  pageIndex: number = 0;
  @Input() canAddNew: boolean = true;
  @Input() tableType: string = 'view';
  @Output() updated: any = new EventEmitter();
  @Output() pageChanged: any = new EventEmitter();
  printOrder: any;
  currency: string;
  togglePrintBackground: boolean = false;
  // roles variables
  isCustomerServiceRole: boolean;
  isAccountantRole: boolean;
  isProducerRole: boolean;
  isLogistecsRole: boolean;
  isAdminRole: boolean;
  isSuperAdminRole: boolean;
  isSalesRole: boolean;
  isGuestRole: boolean;
  isDriverRole: boolean;
  
  constructor(private activatedRoute: ActivatedRoute, private ordersService: OrdersService, private dialog: MatDialog, private sharedService: SharedService) {
  }
  
  ngOnInit() {
    this.setUserRole();
    this.sharedService.setCurrentUserRole(this.sharedService.currentUserData().roles[0]);
    this.orders = [];
    // this.allOrders = this.orders;
    this.activatedRoute.params
      .subscribe((params: Params) => {
        console.log(params.accountId);
        console.log(params.type);
        if (params.accountId) {
          this.accountId = params.accountId;
          this.type = params.type;
          this.typeApp = params.type == 'app';
          this.typeSale = params.type == 'sales';
          this.getOrdersByCustomerId(this.accountId);
        } else {
          this.type = params.type;
          this.typeApp = params.type == 'app';
          this.typeSale = params.type == 'sales';
        }
      });
    this.currency = this.sharedService.currentUserData().currency;
  }
  
  ngOnChanges() {
    this.ordersInCurrentPage = this.orders;
    // this.pageIndex = 0;
  }
  
  openMap(order) {
    if (order.latitude && order.longitude) {
      window.open('https://www.google.com.eg/maps/search/' + order.latitude + ',' + order.longitude, '_blank');
    }
  }
  
  changeDeliverDate(order) {
    let dialogRef = this.dialog.open(ChangeDeliveryDateComponent,
      {
        data: {
          orderId: order.id,
          deliveryDate: order.deliveryDate
        },
        width: '500px'
      }).afterClosed()
      .subscribe(() => {
        this.updated.emit();
      });
    
  }
  
  setUserRole() {
    console.log(this.sharedService.currentUserRole.value);
    this.sharedService.currentUserRole
      .subscribe((currentUserRole) => {
        console.log(currentUserRole);
        this.isCustomerServiceRole = currentUserRole.toLowerCase() == 'customerservice';
        this.isAccountantRole = currentUserRole.toLowerCase() == 'accountant';
        this.isProducerRole = currentUserRole.toLowerCase() == 'production';
        this.isLogistecsRole = currentUserRole.toLowerCase() == 'logistics';
        console.log(this.isLogistecsRole);
        console.log(this.sharedService.isLogistecsRole);
        this.isAdminRole = currentUserRole.toLowerCase() == 'admin';
        this.isSuperAdminRole = currentUserRole.toLowerCase() == 'superadmin';
        this.isSalesRole = currentUserRole.toLowerCase() == 'sales';
        this.isGuestRole = currentUserRole.toLowerCase() == 'guest';
        this.isDriverRole = currentUserRole.toLowerCase() == 'driver';
        console.log(this.isDriverRole);
      });
  }
  
  getOrdersByCustomerId(accountId) {
    if (accountId) {
      if (this.typeApp) {
        this.ordersService.getOrdersByCustomerId(accountId)
          .subscribe((res) => {
              console.log(res.data);
              this.orders = res.data;
              this.ordersInCurrentPage = this.orders.slice(0, this.pageSize);
            },
            err => {
              alert('server error');
            });
      } else if (this.typeSale) {
        this.ordersService.getOrdersByCompanyId(accountId)
          .subscribe((res) => {
              console.log(res.data);
              this.orders = res.data;
              this.ordersInCurrentPage = this.orders.slice(0, this.pageSize);
            },
            err => {
              alert('server error');
            });
      }
    } else {
    
    }
  }
  
  openCreateOrderModal(items) {
    console.log(items);
    if (items.fK_Cart_Id) {
      // console.log(items.fK_Order_Type);
      if (items.fK_Order_Type_Id == 1) {
        this.type = 'app';
      } else if (items.fK_Order_Type_Id == 2) {
        this.type = 'sales';
      }
      this.ordersService.getOrderDetailsByCartId(items.fK_Cart_Id)
        .subscribe((res) => {
          console.log('Log Cart');
          console.log(res.data.cartItems[0]);
          let cartItems = [];
          res.data.cartItems.map((cartItem) => {
            if (items.fK_Order_Type_Id == 1) {
              cartItems.push({
                categoryId: cartItem.item.fK_Category_Id,
                fK_Option_Id: cartItem.fK_Option_Id,
                fk_Item_Id: cartItem.fk_Item_Id,
                fK_Quantity_Id: cartItem.fK_Quantity_Id,
                fK_Cutting_Id: cartItem.fK_Cutting_Id,
                quantity: cartItem.quantityValue,
                salesPricePerItem: cartItem.salesPricePerItem,
                itemQuantityByKG: cartItem.itemQuantityByKG,
              });
            } else if (items.fK_Order_Type_Id == 2) {
              cartItems.push({
                categoryId: cartItem.item.fK_Category_Id,
                fK_Option_Id: cartItem.fK_Option_Id,
                fk_Item_Id: cartItem.fk_Item_Id,
                fK_Quantity_Id: cartItem.fK_Quantity_Id,
                fK_Cutting_Id: cartItem.fK_Cutting_Id,
                quantity: cartItem.quantityValue,
                salesPricePerItem: cartItem.salesPricePerItem,
                itemQuantityByKG: cartItem.itemQuantityByKG,
                measurmentType: cartItem.measurmentUnit.id
              });
            }
          });
          let modalRef = this.dialog.open(CreateOrderModalComponent, {
            data: {cartItems: cartItems, accountId: items.fK_Customer_Id, type: this.type, orderId: items.id},
            closeOnNavigation: true,
            disableClose: true,
            width: '500px',
            height: '600px'
          });
          modalRef.afterClosed()
            .subscribe((resData) => {
              if (resData) {
                console.log('res');
                console.log(resData);
                // debugger;
                this.getOrdersByCustomerId(this.accountId);
              }
            });
        });
    } else {
      let modalRef = this.dialog.open(CreateOrderModalComponent, {
        data: {cartItems: items, accountId: this.accountId, type: this.type},
        closeOnNavigation: true,
        disableClose: true,
        width: '500px',
        height: '600px'
      });
      modalRef.afterClosed()
        .subscribe((res) => {
          if (res) {
            this.getOrdersByCustomerId(this.accountId);
          }
        });
    }
  }
  
  moveToNextStep(id) {
    this.ordersService.moveToNextStep(id)
      .subscribe((res) => {
        if (res.isSucceeded) {
          this.updated.emit();
        }
      });
  }
  
  openChangeStatusModal(order, type) {
    console.log(type);
    order.type = type;
    let modalRef = this.dialog.open(ChangeStatusModalComponent,
      {
        width: '500px',
        height: '250px',
        data: order
      });
    modalRef.afterClosed()
      .subscribe((result) => {
        if (result) {
          this.updated.emit();
        }
      });
  }
  
  sendSMS(order) {
    let modalRef = this.dialog.open(SendSmsmodalComponent, {
      width: '500px',
      height: '500px',
      data: order
    });
  }
  
  printInvoice(order) {
    this.printOrder = order;
    this.togglePrintBackground = true;
    var newWin = window.open('', '_blank', 'top=0,left=0,height=500px,width=500px');
    
    this.ordersService.getOrderDetailsByCartId(this.printOrder.fK_Cart_Id)
      .subscribe((res) => {
        console.log(res);
        console.log(res.data.cartItems);
        this.printOrder.items = res.data.cartItems;
        this.printOrder.items.map((item) => {
          item.price = item.price;
        });
        console.log(this.printOrder);
        this.printOrder.cartPrice = this.printOrder.cartPrice;
        this.printOrder.cartVAT = this.printOrder.cartVAT;
        this.printOrder.price = this.printOrder.price;
        console.log(newWin);
        
        setTimeout(() => {
          var divToPrint = document.getElementById('invoice-container');
          newWin.document.write(divToPrint.outerHTML);
          newWin.print();
          newWin.close();
          this.togglePrintBackground = false;
        }, 500);
      });
  }
  
  printTicket(order) {
    this.printOrder = order;
    this.togglePrintBackground = true;
    console.log(this.printOrder);
    let url = window.location.host;
    var newWin = window.open('', '_blank', 'top=0,left=0,height=500px,width=500px');
    
    this.ordersService.getOrderDetailsByCartId(this.printOrder.fK_Cart_Id)
      .subscribe((res) => {
        console.log(res);
        console.log(res.data.cartItems);
        this.printOrder.items = res.data.cartItems;
        
        this.printOrder.items.map((item) => {
          item.price = item.price;
        });
        console.log(this.printOrder);
        this.printOrder.cartPrice = this.printOrder.cartPrice;
        this.printOrder.cartVAT = this.printOrder.cartVAT;
        this.printOrder.price = this.printOrder.price;
        
        console.log(newWin);
        
        setTimeout(() => {
          var divToPrint = document.getElementById('ticket-container');
          newWin.document.write(divToPrint.outerHTML);
          newWin.print();
          newWin.close();
          this.togglePrintBackground = false;
        }, 500);
      });
  }
  
  sendEmail(order) {
    this.togglePrintBackground = true;
    this.printOrder = order;
    setTimeout(() => {
      let body = document.getElementById('invoice-container').innerHTML;
      console.log(body);
      let emailObj = {
        subject: 'Mawashi Order Invoice',
        body: body,
        to: [`${order.customerEmail}`]
      };
      console.log(emailObj);
      this.ordersService.postEmail(emailObj)
        .subscribe(() => {
        
        });
    }, 500);
    
  }
  
  pageEvent(e) {
    this.pageChanged.emit({
      pageNo: ++e.pageIndex,
      pageSize: e.pageSize
    });
  }
  
}
