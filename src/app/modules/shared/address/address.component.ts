import { Component, Input, OnInit } from '@angular/core';
import { SharedService } from '../../../services/shared.service';
import { OrdersService } from '../../../services/ordersService';

@Component({
  selector: 'app-address',
  templateUrl: './address.component.html',
  styleUrls: ['./address.component.scss']
})

export class AddressComponent implements OnInit {
  @Input() accountId: number;
  addresses: any[];
  toggleAddButton: boolean = true;
  customerData: any;
  @Input() type: string;
  typeApp: boolean;
  typeSales: boolean;
  isSave: boolean = false;;

  constructor(private sharedService: SharedService, private orderService: OrdersService) {
  }

  ngOnInit() {
    this.typeApp = this.type == 'app';
    this.typeSales = this.type == 'sales';
    this.getAllAddresses();
    this.customerData = this.sharedService.currentUserData();
  }

  getAllAddresses() {
    console.log(this.accountId);
    if (this.typeApp) {
      this.orderService.getAccountAddresses(this.accountId)
        .subscribe((res) => {
          // console.log(res.data);
          this.addresses = res.data;
        });
    } else if (this.typeSales) {
      this.orderService.getCompanyAddress(this.accountId)
        .subscribe((res) => {
          // console.log(res.data);
          this.addresses = res.data;
        });
    }
  }

  removeAddress(index, id) {
    if (id) {
      if (this.typeApp) {
        this.sharedService.removeAddress(id)
          .subscribe(() => {
            this.addresses.splice(index, 1);
          });
      } else if (this.typeSales) {
        this.sharedService.removeCompanyAddress(id)
          .subscribe(() => {
            this.addresses.splice(index, 1);
          });
      }
    } else {
      this.addresses.splice(this.addresses.length - 1, 1);
      this.toggleAddButton = true;
    }
  }

  addAddress() {
    this.addresses.push({ newAddress: true });
    this.toggleAddButton = false;
    //this.isSave = true;
  }

  // checkForPACIorMakany(address) {
  //   if (address.number && ((this.customerData.country_Code.toLowerCase() == 'uae' && address.number.length >= 10) || (this.customerData.country_Code.toLowerCase() == 'kuwait' && address.number.length >= 8))) {
  //     address.toggleLoading = true;
  //     this.sharedService.locationByPACIOrMakany(address.number)
  //       .subscribe((res) => {
  //           console.log(res);
  //           address.areaName = res.area.name;
  //           address.blockName = res.block.name;
  //           address.governorateName = res.governorate.name;
  //           address.latitude = res.lat;
  //           address.longitude = res.long;
  //           address.toggleLoading = false;
  //         },
  //         err => {
  //           console.log(err);
  //           address.toggleLoading = false;
  //         });
  //   } else {
  //     address.toggleLoading = false;
  //   }
  // }

  saveAddress(addressPost) {
    addressPost.fk_Country_Id = this.customerData.fk_Country_Id;
    console.log(addressPost);
    if (this.typeApp) {
      addressPost.fk_Customer_Id = this.accountId;
      this.sharedService.saveAddress(addressPost)
        .subscribe(() => {
          this.toggleAddButton = true;
          this.getAllAddresses();
        });
    } else if (this.typeSales) {
      addressPost.fk_Company_Id = this.accountId;
      addressPost.fk_AddressType_Id = addressPost.fk_Country_Id;
      this.sharedService.saveCompanyAddress(addressPost)
        .subscribe(() => {
          this.toggleAddButton = true;
          this.getAllAddresses();
        });
    }
  }

}
