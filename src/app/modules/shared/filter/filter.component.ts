import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { SharedService } from '../../../services/shared.service';
import { OrdersService } from '../../../services/ordersService';
import { UtilitiesService } from '../../../services/utilities.service';

@Component({
  selector: 'app-filter',
  templateUrl: './filter.component.html',
  styleUrls: ['./filter.component.scss']
})
export class FilterComponent implements OnInit {
  @Input() type;
  @Output() filterTerms = new EventEmitter();
  @Output() resetFilterTerms = new EventEmitter();
  @Input() hideExport: boolean;
  typeApp: boolean;
  typeSales: boolean;
  userCountryId: number;
  filterObj: any;
  filterResetObj: any;
  paymentTypes: any[];
  deliveryTypes: any[];
  allOrderStatus: any[];
  allOrderTypes: any[];
  filterObjView: any = {};
  @Output() exportTrigger = new EventEmitter();

  constructor(private sharedService: SharedService, private ordersService: OrdersService, private utilitiesService: UtilitiesService) {
  }

  ngOnInit() {
    this.userCountryId = this.sharedService.currentUserData().fk_Country_Id;
    this.filterResetObj = {
      pageInfo: {
        pageNo: 1,
        pageSize: 10
      },
      filters: [
        // {
        // propertyName: 'fk_Country_Id', value: this.userCountryId
        // }
      ]
    };
    if (this.type) {
      this.typeApp = this.type == 'app';
      this.typeSales = this.type == 'sales';
      if (this.typeApp) {
        this.filterResetObj.filters.push({ propertyName: 'fK_Order_Type_Id', value: 1 });
      } else if (this.typeSales) {
        this.filterResetObj.filters.push({ propertyName: 'fK_Order_Type_Id', value: 2 });
      }
    }
    this.getAllOrderStatus();
    this.getAllOrderTypes();

    this.getAllDeliveryTypes();
    this.getAllPaymentMethods();
    this.resetFilter();
    this.getFilterOrders();
  }

  resetFilter() {
    this.filterObj = Object.assign({}, this.filterResetObj);
    this.filterObj.filters = this.filterResetObj.filters.slice();
  }

  resetFilterViewValues() {
    this.filterObjView = {};
    this.getFilterOrders();
  }

  exportData() {
    if (this.typeApp) {
      this.ordersService.postExportOrdersToExcel(this.filterObj.filters)
        .subscribe((res) => {
          console.log(res.data);
          window.location.href = res.data;
        });
    } else if (this.typeSales) {
      this.ordersService.postExportSalesOrdersToExcel(this.filterObj.filters)
        .subscribe((res) => {
          console.log(res.data);
          window.location.href = res.data;
        });
    }
  }


  getFilterOrders(pageNumber = 1) {
    this.resetFilter();
    for (let property in this.filterObjView) {
      if (this.filterObjView.hasOwnProperty(property)) {
        console.log(property);
        if (!property.toLowerCase().includes('date')) {
          // console.log('found code');
          let value = this.filterObjView[property];
          console.log(value);
          if (this.filterObjView.fK_DeliveryType_Id) {
            this.filterObj.filters.push({ propertyName: property, value: Number(value) });
          } else {
            this.filterObj.filters.push({ propertyName: property, value: value });
          }
        }
      }
    }

    if (this.filterObjView.deliveryDateFrom && this.filterObjView.deliveryDateTo) {
      this.filterObj.filters.push({
        propertyName: 'deliveryDate',
        value: [this.filterObjView.deliveryDateFrom.toString(), this.filterObjView.deliveryDateTo.toString()]
      });
    }
    if (this.filterObjView.createdDateFrom && this.filterObjView.createdDateTo) {
      this.filterObj.filters.push({
        propertyName: 'createdDate',
        value: [this.filterObjView.createdDateFrom.toString(), this.filterObjView.createdDateTo.toString()]
      });
    }
    this.filterObj.filters.map(filter => {
      if (filter.propertyName == 'createdDate') {
        filter.value[0] = this.utilitiesService.convertToLocalUTCLikeFormat(filter.value[0]);
        filter.value[1] = this.utilitiesService.convertToLocalUTCLikeFormat(filter.value[1]);
      } else if (filter.propertyName == 'deliveryDate') {
        filter.value[0] = this.utilitiesService.convertToLocalUTCLikeFormat(filter.value[0]);
        filter.value[1] = this.utilitiesService.convertToLocalUTCLikeFormat(filter.value[1]);
      }
    });

    console.log('Dates created');
    console.log(this.filterObj.filters);

    this.filterObj.pageInfo.pageNo = pageNumber;
    console.log(this.filterObj);
    this.filterTerms.emit(this.filterObj);
  }


  getAllOrderStatus() {
    this.sharedService.getAllOrderStatus()
      .subscribe((res) => {
        // console.log(res);
        this.allOrderStatus = res.data;
      });
  }

  getAllOrderTypes() {
    this.sharedService.getAllOrderTypes()
      .subscribe((res) => {
        // console.log(res);
        this.allOrderTypes = res;
      });
  }


  getAllDeliveryTypes() {
    this.sharedService.getAllDeliveryTypes()
      .subscribe((res) => {
        // console.log(res);
        this.deliveryTypes = res;
      });
  }

  getAllPaymentMethods() {
    if (this.typeApp) {
      this.sharedService.getAllPaymentMethodsApp()
        .subscribe((res) => {
          // console.log(res);
          this.paymentTypes = res.data;
        });
    } else if (this.typeSales) {
      this.sharedService.getAllPaymentMethodsSales()
        .subscribe((res) => {
          // console.log(res);
          this.paymentTypes = res.data;
        });
    } else {
      this.sharedService.getAllPaymentMethods()
        .subscribe((res) => {
          // console.log(res);
          this.paymentTypes = res.data;
        });
    }

  }

}
