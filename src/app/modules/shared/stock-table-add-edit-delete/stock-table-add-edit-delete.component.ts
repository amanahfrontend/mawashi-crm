import {Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {CuttingService} from '../../../services/cutting.service';
import {Subscription} from 'rxjs/Subscription';
import {MatDialog} from '@angular/material';
import {AddEditStockModalComponent} from '../add-edit-stock-modal/add-edit-stock-modal.component';
import {QuantitiesService} from '../../../services/quantities.service';

@Component({
  selector: 'app-stock-table-add-edit-delete',
  templateUrl: './stock-table-add-edit-delete.component.html',
  styleUrls: ['./stock-table-add-edit-delete.component.scss']
})
export class StockTableAddEditDeleteComponent implements OnInit, OnDestroy {
  @Input() data: any[];
  @Input() type: string;
  @Output() updated = new EventEmitter();
  deleteCuttingSubscription: Subscription;
  deleteQuantitySubscription: Subscription;
  
  constructor(private cuttingService: CuttingService, private dialog: MatDialog, private quantityService: QuantitiesService) {
  }
  
  ngOnInit() {
    console.log(this.data);
  }
  
  ngOnDestroy() {
    this.deleteCuttingSubscription && this.deleteCuttingSubscription.unsubscribe();
  }
  
  addRow() {
    console.log('add data');
    let dialogRef = this.dialog.open(AddEditStockModalComponent, {
      data: {
        type: this.type,
        operation: 'add'
      },
      width: '500px'
    });
    dialogRef.afterClosed()
      .subscribe((result) => {
        console.log(result);
        if (result) {
          this.updated.emit();
        }
      });
  }
  
  editRow(rowData) {
    rowData.type = this.type;
    rowData.operation = 'edit';
    console.log('edit data');
    let dialogRef = this.dialog.open(AddEditStockModalComponent, {
      data: rowData,
      width: '500px'
    });
    dialogRef.afterClosed()
      .subscribe((result) => {
        if (result) {
          this.updated.emit();
        }
      });
  }
  
  deleteRow(id) {
    if (this.type == 'cutting') {
      // console.log('delete');
      this.deleteCuttingSubscription = this.cuttingService.deleteCutting(id)
        .subscribe(() => {
            this.updated.emit();
          },
          err => {
            console.log(err);
          });
    } else if (this.type == 'quantity') {
      this.deleteQuantitySubscription = this.quantityService.deleteQuantity(id)
        .subscribe(() => {
            this.updated.emit();
          },
          err => {
            console.log(err);
          });
    }
  }
  
}
