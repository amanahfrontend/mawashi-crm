import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { OrdersTableComponent } from './orders-table/orders-table.component';
import { RouterModule } from '@angular/router';
import { MatDialogModule } from '@angular/material/dialog';
import { CreateOrderModalComponent } from './create-order-modal/create-order-modal.component';
import { WebService } from '../../services/webService';
import { OrdersService } from '../../services/ordersService';
import { PromptDialogComponent } from './prompt-dialog/prompt-dialog.component';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material';
import { AddressComponent } from './address/address.component';
import { SharedService } from '../../services/shared.service';
import { FormRequiredErrorComponent } from './form-required-error/form-required-error.component';
import { StockTableAddEditDeleteComponent } from './stock-table-add-edit-delete/stock-table-add-edit-delete.component';
import { AddEditStockModalComponent } from './add-edit-stock-modal/add-edit-stock-modal.component';
import { MatPaginatorModule } from '@angular/material/paginator';
import { QRCodeModule } from 'angular2-qrcode';
import { AddressFormComponent } from './address-form/address-form.component';
import { ComplainsTableComponent } from '../accounts/complains-table/complains-table.component';
import { FilterComponent } from './filter/filter.component';
import { SendSmsmodalComponent } from './send-smsmodal/send-smsmodal.component';
import { ResetPasswordModalComponent } from '../system-accounts/reset-password-modal/reset-password-modal.component';
// import { ContactUsComponent } from './contact-us/contact-us.component';
import { AgmCoreModule } from '@agm/core';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RouterModule,
    MatDialogModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatPaginatorModule,
    QRCodeModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyBiBDcDEMFu1Y2piNBoRjsnpDO_sIpOvqE'
    })
  ],
  declarations: [ OrdersTableComponent, CreateOrderModalComponent, PromptDialogComponent, AddressComponent, FormRequiredErrorComponent, StockTableAddEditDeleteComponent, AddEditStockModalComponent, AddressFormComponent, FilterComponent, SendSmsmodalComponent, ResetPasswordModalComponent],
  entryComponents: [
    CreateOrderModalComponent,
    PromptDialogComponent,
    AddEditStockModalComponent,
    SendSmsmodalComponent,
    ResetPasswordModalComponent
  ],
  providers: [WebService, OrdersService, SharedService],
  exports: [
    FormsModule,
    MatDatepickerModule,
    OrdersTableComponent,
    MatNativeDateModule,
    MatPaginatorModule,
    QRCodeModule,
    AddressComponent,
    StockTableAddEditDeleteComponent,
    FormRequiredErrorComponent,
    AddressFormComponent,
    FilterComponent,
    ResetPasswordModalComponent,
    MatDialogModule
    ]
})
export class SharedModule {
}
