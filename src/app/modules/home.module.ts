import {NgModule} from '@angular/core';
// import {CommonModule} from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
// import {accountsComponent} from '../pages/accounts/accounts.component';
import {homeComponent} from '../pages/home/home.component';
import {SharedModule} from './shared/shared.module';
import {TranslationModule} from './translation.module';
import {HomeService} from '../services/home.service';
import {CommonModule} from '@angular/common';
import {ChangeStatusModalComponent} from './change-status-modal/change-status-modal.component';
import {ContactUsComponent} from './shared/contact-us/contact-us.component';
import {ChangeDeliveryDateComponent} from './shared/change-delivery-date/change-delivery-date.component';
// import {FormsModule} from '@angular/forms';

const routes: Routes = [
  {path: '', component: homeComponent},
  {path: 'contact-us', component: ContactUsComponent},
];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
    SharedModule,
    TranslationModule,
    CommonModule
    // FormsModule
  ],
  declarations: [homeComponent, ChangeStatusModalComponent, ContactUsComponent, ChangeDeliveryDateComponent],
  entryComponents: [ChangeStatusModalComponent, ChangeDeliveryDateComponent],
  providers: [HomeService]
})
export class HomeModule {
}
