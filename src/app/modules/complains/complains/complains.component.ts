import {Component, OnInit} from '@angular/core';
import {ComplainsService} from '../../../services/complains.service';
import {Subscription} from 'rxjs/Subscription';

@Component({
  selector: 'app-complains',
  templateUrl: './complains.component.html',
  styleUrls: ['./complains.component.scss']
})
export class ComplainsComponent implements OnInit {
  complainsSubscription: Subscription;
  complains: any[] = [];
  count: any[];
  pageSize: number = 10;
  
  constructor(private complainsService: ComplainsService) {
  }
  
  ngOnInit() {
    this.getAllComplains();
  }
  
  ngOnDestroy() {
    this.complainsSubscription && this.complainsSubscription.unsubscribe();
  }
  
  pageEvent(e) {
    console.log(e);
    this.getAllComplains(++e.pageIndex);
  }
  
  getAllComplains(pageNumber = 1) {
    this.complainsSubscription = this.complainsService.getAllComplains({pageNo: pageNumber, pageSize: this.pageSize})
      .subscribe((res) => {
        console.log(res.data.data);
        this.count = res.data.count;
        this.complains = res.data.data;
      });
  }
  
}
