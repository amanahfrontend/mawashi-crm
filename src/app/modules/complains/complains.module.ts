import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ComplainsComponent } from './complains/complains.component';
import {RouterModule, Routes} from '@angular/router';
import {TranslationModule} from '../translation.module';
import {SharedModule} from '../shared/shared.module';
import {ComplainsService} from '../../services/complains.service';


const routes: Routes = [
  {path: '', component: ComplainsComponent},
];


@NgModule({
  imports: [
    RouterModule.forChild(routes),
    CommonModule,
    SharedModule,
    TranslationModule
  ],
  declarations: [ComplainsComponent],
  providers: [ComplainsService]
})
export class ComplainsModule { }
