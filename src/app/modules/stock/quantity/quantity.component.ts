import {Component, OnInit} from '@angular/core';
import {QuantitiesService} from '../../../services/quantities.service';

@Component({
  selector: 'app-quantity',
  templateUrl: './quantity.component.html',
  styleUrls: ['./quantity.component.scss']
})
export class QuantityComponent implements OnInit {
  quantities: any[] = [];
  
  constructor(private quantityService: QuantitiesService) {
  }
  
  ngOnInit() {
    this.getAllQuantities();
  }
  
  getAllQuantities() {
    this.quantityService.getAllQuantities()
      .subscribe((res) => {
          console.log(res.data);
          this.quantities = res.data;
        },
        err => {
        
        });
  }
}
