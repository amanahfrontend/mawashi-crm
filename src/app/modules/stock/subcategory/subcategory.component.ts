import { Component, OnInit } from '@angular/core';
import { CategoryService } from '../../../services/category.service';
import { Subscription } from 'rxjs/Subscription';
import { ItemService } from '../../../services/item.service';

@Component({
  selector: 'app-subcategory',
  templateUrl: './subcategory.component.html',
  styleUrls: ['./subcategory.component.scss']
})
export class SubCategoryComponent implements OnInit {
  selectedCategory: any;
  selectedState: string;
  selectedType: string;
  itemsList: any[] = [];
  getCategoryTypesSubscription: Subscription;
  categoryList: any[] = [];
  subCategoryList: any[] = [];
  categoryTypes: any[] = [];
  selectedItemState: string;
  toggleNotFoundText: boolean;

  constructor(private categoryService: CategoryService, private itemService: ItemService) {
  }

  ngOnInit() {
    this.getCategoryTypes();
  }


  getSubCategories() {
    if (this.selectedCategory) {
      if (this.selectedType.toLowerCase() == 'app') {
        this.categoryService.getAppActiveSubCategory(this.selectedCategory)
          .subscribe((res) => {
            this.subCategoryList = res.data;
          });
      }
      else {
        this.categoryService.getSalesActiveSubCategory(this.selectedCategory)
          .subscribe((res) => {
            this.subCategoryList = res.data;
          });

      }
    }
  }


  getCategories() {
    if (this.selectedState != undefined) {
      if (this.selectedType.toLowerCase() == 'app') {
        if (this.selectedState.toLowerCase() == 'active') {
          this.categoryService.getAppActiveCategory()
            .subscribe((res) => {
              console.log(res);
              this.categoryList = res.data;
            });
        } else {
          this.categoryService.getAppDeactiveCategory()
            .subscribe((res) => {
              console.log(res);
              this.categoryList = res.data;
            });
        }
      } else {
        if (this.selectedState.toLowerCase() == 'active') {
          this.categoryService.getSalesActiveCategory()
            .subscribe((res) => {
              console.log(res);
              this.categoryList = res.data;
            });
        } else {
          this.categoryService.getSalesDeactiveCategory()
            .subscribe((res) => {
              console.log(res);
              this.categoryList = res.data;
            });
        }

      }

    }
  }

  getCategoryTypes() {
    this.getCategoryTypesSubscription = this.categoryService.getCategoryTypes()
      .subscribe((res) => {
        console.log(res.data);
        this.categoryTypes = res.data;
      });
  }

}
