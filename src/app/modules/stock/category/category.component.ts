import {Component, OnInit} from '@angular/core';
import {CategoryService} from '../../../services/category.service';
import {Subscription} from 'rxjs/Subscription';

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.scss']
})
export class CategoryComponent implements OnInit {
  getCategoryTypesSubscription: Subscription;
  categoryTypes: any[] = [];
  categoryList: any[] = [];
  selectedType: any;
  // selectedTypeName: string;
  selectedState: string;
  
  constructor(private categoryService: CategoryService) {
  }
  
  ngOnInit() {
    this.selectedState = 'active';
    this.getCategoryTypes();
  }
  
  showCategories(options) {
    console.log(options);
    if (options.type.toLowerCase() == 'app') {
      if (options.state.toLowerCase() == 'active') {
        this.categoryService.getAppActiveCategory()
          .subscribe((res) => {
            console.log(res);
            this.categoryList = res.data;
          });
      } else {
        this.categoryService.getAppDeactiveCategory()
          .subscribe((res) => {
            console.log(res);
            this.categoryList = res.data;
          });
      }
    } else {
      if (options.state.toLowerCase() == 'active') {
        this.categoryService.getSalesActiveCategory()
          .subscribe((res) => {
            console.log(res);
            this.categoryList = res.data;
          });
      } else {
        this.categoryService.getSalesDeactiveCategory()
          .subscribe((res) => {
            console.log(res);
            this.categoryList = res.data;
          });
      }
    }
  }
  
  getCategoryTypes() {
    this.getCategoryTypesSubscription = this.categoryService.getCategoryTypes()
      .subscribe((res) => {
        console.log(res.data);
        this.categoryTypes = res.data;
        this.categoryTypes.map((type) => {
          if (type.nameEN.toLowerCase() == 'app') {
            this.selectedType = type.nameEN;
          }
        });
        console.log(this.selectedType);
        this.showCategories({type: this.selectedType, state: this.selectedState});
        // this.selectActiveCategoryByType();
      });
  }
  
}
