import {Component, OnDestroy, OnInit} from '@angular/core';
import {Subscription} from 'rxjs/Subscription';
import {CuttingService} from '../../../services/cutting.service';

@Component({
  selector: 'app-cutting',
  templateUrl: './cutting.component.html',
  styleUrls: ['./cutting.component.scss']
})
export class CuttingComponent implements OnInit, OnDestroy {
  cuttings: any[] = [];
  getAllCuttingsSubscription: Subscription;
  
  constructor(private cuttingService: CuttingService) {
  }
  
  ngOnInit() {
    this.getAllCuttings();
  }
  
  ngOnDestroy() {
    this.getAllCuttingsSubscription.unsubscribe();
  }
  
  getAllCuttings() {
    this.getAllCuttingsSubscription = this.cuttingService.getAllCutting()
      .subscribe((res) => {
          this.cuttings = res.data;
        },
        err => {
          console.log(err);
        });
  }
  
}




