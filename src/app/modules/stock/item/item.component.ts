import {Component, OnInit} from '@angular/core';
import {CategoryService} from '../../../services/category.service';
import {Subscription} from 'rxjs/Subscription';
import {ItemService} from '../../../services/item.service';

@Component({
  selector: 'app-item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.scss']
})
export class ItemComponent implements OnInit {
  selectedCategory: any;
  selectedState: string;
  selectedType: string;
  itemsList: any[] = [];
  getCategoryTypesSubscription: Subscription;
  categoryList: any[] = [];
  categoryTypes: any[] = [];
  selectedItemState: string;
  toggleNotFoundText: boolean;
  
  constructor(private categoryService: CategoryService, private itemService: ItemService) {
  }
  
  ngOnInit() {
    this.getCategoryTypes();
  }
  
  showItems() {
    console.log(this.selectedCategory);
    if (this.selectedCategory) {
      this.toggleNotFoundText = true;
      if (this.selectedItemState == 'active') {
        this.itemService.getAllActiveItems(this.selectedCategory)
          .subscribe((res) => {
              console.log(res);
              this.itemsList = res.data;
            },
            err => {
            
            });
      } else if (this.selectedItemState == 'deactive') {
        this.itemService.getAllDeactiveItems(this.selectedCategory)
          .subscribe((res) => {
              console.log(res);
              this.itemsList = res.data;
            },
            err => {
            
            });
      }
    }
  }
  
  getCategories() {
    if (this.selectedType.toLowerCase() == 'app') {
      if (this.selectedState.toLowerCase() == 'active') {
        this.categoryService.getAppActiveCategory()
          .subscribe((res) => {
            console.log(res);
            this.categoryList = res.data;
          });
      } else {
        this.categoryService.getAppDeactiveCategory()
          .subscribe((res) => {
            console.log(res);
            this.categoryList = res.data;
          });
      }
    } else {
      if (this.selectedState.toLowerCase() == 'active') {
        this.categoryService.getSalesActiveCategory()
          .subscribe((res) => {
            console.log(res);
            this.categoryList = res.data;
          });
      } else {
        this.categoryService.getSalesDeactiveCategory()
          .subscribe((res) => {
            console.log(res);
            this.categoryList = res.data;
          });
      }
    }
  }
  
  getCategoryTypes() {
    this.getCategoryTypesSubscription = this.categoryService.getCategoryTypes()
      .subscribe((res) => {
        console.log(res.data);
        this.categoryTypes = res.data;
      });
  }
  
}
