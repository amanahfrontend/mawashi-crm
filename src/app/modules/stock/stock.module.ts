import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StockComponent } from '../../pages/stock/stock.component';
import { SharedModule } from '../shared/shared.module';
import { RouterModule, Routes } from '@angular/router';
import { TranslationModule } from '../translation.module';
import { CuttingService } from '../../services/cutting.service';
import { QuantitiesService } from '../../services/quantities.service';
import { CategoryService } from '../../services/category.service';

const routes: Routes = [
  {
    path: '', component: StockComponent, children: [
      { path: '', loadChildren: '../category/category.module#CategoryModule' },
      { path: 'subcategory', loadChildren: '../subcategory/subcategory.module#SubCategoryModule' },
      { path: 'item', loadChildren: '../item/item.module#ItemModule' },
      { path: 'cutting', loadChildren: '../cutting/cutting.module#CuttingModule' },
      { path: 'quantity', loadChildren: '../quantity/quantity.module#QuantityModule' },
    ]
  },
];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
    CommonModule,
    SharedModule,
    TranslationModule
  ],
  declarations: [StockComponent],
  providers: [CuttingService, QuantitiesService, CategoryService]
})

export class StockModule {
}
