import {Component, Inject, OnDestroy, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {AddEditCategoryModalComponent} from '../../category/add-edit-category-modal/add-edit-category-modal.component';
import {Subscription} from 'rxjs/Subscription';
import {UtilitiesService} from '../../../services/utilities.service';
import {ItemService} from '../../../services/item.service';
import {CategoryService} from '../../../services/category.service';
import {QuantitiesService} from '../../../services/quantities.service';
import {CuttingService} from '../../../services/cutting.service';

@Component({
  selector: 'app-add-edit-items-modal',
  templateUrl: './add-edit-items-modal.component.html',
  styleUrls: ['./add-edit-items-modal.component.scss']
})
export class AddEditItemsModalComponent implements OnInit, OnDestroy {
  categoryTypes: any[] = [];
  categoryTypesSubscription: Subscription;
  isItemImageIncluded: boolean;
  isExistedItem: boolean;
  selectedFileName: string;
  selectedState: string;
  toggleLoading: boolean;
  failed: boolean;
  categoryList: any[];
  quantities: any[] = [];
  cuttings: any[] = [];
  selectedQuantities: any[] = [];
  selectedCuttings: any[] = [];

  constructor(public dialogRef: MatDialogRef<AddEditCategoryModalComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any, private utilitiesService: UtilitiesService, private itemService: ItemService, private categoryService: CategoryService, private quantityService: QuantitiesService, private cuttingService: CuttingService) {
  }

  ngOnInit() {
    console.log("data for modal");
    console.log(this.data);

    if (JSON.stringify(this.data) == '{}') {
      this.isExistedItem = false;
      this.data.isActive = false;
    } else {
      this.isExistedItem = true;
      this.itemService.getItemDetails(this.data.id)
        .subscribe((res) => {
          let quantities = [];
          res.data.quantities.map((quantity) => {
            console.log(quantity);
            quantities.push({id: quantity.quantity.id, nameEN: quantity.quantity.nameEN});
          });
          this.selectedQuantities = quantities;
          let cuttings = [];
          res.data.cuttings.map((cutting) => {
            console.log(cutting);
            cuttings.push({id: cutting.cutting.id, nameEN: cutting.cutting.nameEN});
          });
          this.selectedCuttings = cuttings;
        });
    }
    this.isItemImageIncluded = this.data.pictureURL;
    this.getCategoryTypes();
    this.getAllQuantities();
    this.getAllCuttings();
  }

  ngOnDestroy() {
    this.categoryTypesSubscription.unsubscribe();
  }

  getAllQuantities() {
    this.quantityService.getAllQuantities()
      .subscribe((res) => {
        console.log(res.data);
        let quantities = [];
        res.data.map((quantity) => {
          quantities.push({nameEN: quantity.nameEN, id: quantity.id});
        });
        this.quantities = quantities;
        console.log(this.quantities);
      });
  }

  getAllCuttings() {
    this.cuttingService.getAllCutting()
      .subscribe((res) => {
        console.log(res.data);
        let cuttings = [];
        res.data.map((cutting) => {
          cuttings.push({nameEN: cutting.nameEN, id: cutting.id});
        });
        this.cuttings = cuttings;
        console.log(this.quantities);
      });
  }

  saveItem() {
    console.log(this.data);
    this.toggleLoading = true;
    let selectedQuantityIds = [];
    let selectedCuttingIds = [];
    this.selectedQuantities.map((quantity) => {
      selectedQuantityIds.push(quantity.id);
    });
    this.selectedCuttings.map((cutting) => {
      selectedCuttingIds.push(cutting.id);
    });
    let ItemToPost: any = {
      item: {
        fK_Category_Id: this.data.fK_Category_Id,
        price: this.data.price,
        nameEN: this.data.nameEN,
        nameAR: this.data.nameAR,
        descriptionEN: this.data.descriptionEN,
        descriptionAR: this.data.descriptionAR,
        fK_CategoryType_Id: this.data.fK_CategoryType_Id,
        code: this.data.code,
        weight: this.data.weight,
        currentStock: this.data.currentStock,
        currentHeadStock: this.data.currentHeadStock,
        currentKgsStock: this.data.currentKgsStock,
        currentPiecesStock: this.data.currentPiecesStock,
        isActive: this.data.isActive,
        file: this.data.file,
        dateFrom: this.data.dateFrom,
        dateTo: this.data.dateTo,
        age: this.data.age
      },
      quantitiesIds: selectedQuantityIds,
      cuttingIds: selectedCuttingIds
    };
    if (this.isExistedItem) {
      ItemToPost.item.id = this.data.id;
    }
    console.log(ItemToPost);
    // if (this.isExistedItem) {
    this.itemService.addUpdateItem(ItemToPost)
      .subscribe((res) => {
        if (res.isSucceeded) {
          this.closeDialog(true);
        }
      });
  }

  selectImage(event) {
    console.log(event.srcElement.files[0]);
    let file = event.srcElement.files[0];
    this.utilitiesService.getBase64FromFile(file)
      .then((object: any) => {
        console.log(object);
        this.data.file = {
          fileContent: object.fileContent
        };
        this.selectedFileName = object.fileName;
        this.isItemImageIncluded = true;
        delete this.data.pictureURL;
      });
  }

  removeSelectedImage() {
    this.selectedFileName = '';
    this.isItemImageIncluded = false;
    delete this.data.pictureURL;
    delete this.data.file;
  }

  getCategoryTypes() {
    this.categoryTypesSubscription = this.categoryService.getCategoryTypes()
      .subscribe((res) => {
          this.categoryTypes = res.data;
        },
        err => {

        });
  }

  getCategories() {
    let selectedTypeName = '';
    this.categoryTypes.map((type) => {
      if (type.id == this.data.fK_CategoryType_Id) {
        selectedTypeName = type.nameEN;
      }
    });
    console.log(selectedTypeName);
    console.log(this.selectedState);
    if (selectedTypeName.toLowerCase() == 'app') {
      if (this.selectedState.toLowerCase() == 'active') {
        this.categoryService.getAppActiveCategory()
          .subscribe((res) => {
            console.log(res);
            this.categoryList = res.data;
          });
      } else {
        this.categoryService.getAppDeactiveCategory()
          .subscribe((res) => {
            console.log(res);
            this.categoryList = res.data;
          });
      }
    } else {
      if (this.selectedState.toLowerCase() == 'active') {
        this.categoryService.getSalesActiveCategory()
          .subscribe((res) => {
            console.log(res);
            this.categoryList = res.data;
          });
      } else {
        this.categoryService.getSalesDeactiveCategory()
          .subscribe((res) => {
            console.log(res);
            this.categoryList = res.data;
          });
      }
    }
  }

  closeDialog(res = false) {
    this.dialogRef.close(res);
  }


}
