import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SharedModule} from '../shared/shared.module';
import {RouterModule, Routes} from '@angular/router';
import {TranslationModule} from '../translation.module';
// import {CategoryComponent} from '../stock/category/category.component';
import {ItemComponent} from '../stock/item/item.component';
import {ItemService} from '../../services/item.service';
import {ItemsTableComponent} from './items-table/items-table.component';
import {AddEditItemsModalComponent} from './add-edit-items-modal/add-edit-items-modal.component';
import {MultiSelectModule} from 'primeng/components/multiselect/multiselect';


const routes: Routes = [
  {path: '', component: ItemComponent},
];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
    CommonModule,
    SharedModule,
    TranslationModule,
    MultiSelectModule
  ],
  declarations: [ItemComponent, ItemsTableComponent, AddEditItemsModalComponent],
  providers: [ItemService],
  entryComponents: [AddEditItemsModalComponent]
})
export class ItemModule {
}
