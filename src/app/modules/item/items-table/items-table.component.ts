import {Component, OnInit, Input, Output, EventEmitter} from '@angular/core';
import {MatDialog} from '@angular/material';
import {AddEditItemsModalComponent} from '../add-edit-items-modal/add-edit-items-modal.component';
import {ItemService} from '../../../services/item.service';

@Component({
  selector: 'app-items-table',
  templateUrl: './items-table.component.html',
  styleUrls: ['./items-table.component.scss']
})
export class ItemsTableComponent implements OnInit {
  @Input() itemList;
  @Input() toggleNotFoundText: boolean;
  @Output() updated = new EventEmitter();
  
  constructor(private dialog: MatDialog, private itemService: ItemService) {
  }
  
  ngOnInit() {
  }
  
  openAddItemModal() {
    console.log('open add modal');
    let dialogRef = this.dialog.open(AddEditItemsModalComponent, {
      width: '500px',
      height: '500px',
      data: {}
    });
    dialogRef.afterClosed()
      .subscribe((res) => {
        if (res) {
          this.updated.emit();
        }
      });
  }
  
  setItemActive(id) {
    this.itemService.setItemActive(id)
      .subscribe((res) => {
        if (res.isSucceeded) {
          this.updated.emit();
        }
      });
  }
  
  setItemDeactive(id) {
    this.itemService.setItemDeactive(id)
      .subscribe((res) => {
        if (res.isSucceeded) {
          this.updated.emit();
        }
      });
  }
  
  openEditItemModal(item) {
    let dialogRef = this.dialog.open(AddEditItemsModalComponent, {
      width: '500px',
      height: '500px',
      data: item
    });
    dialogRef.afterClosed()
      .subscribe((res) => {
        if (res) {
          this.updated.emit();
        }
      });
  }
  
  deleteItem(id) {
    this.itemService.deleteItem(id)
      .subscribe((res) => {
        if (res.isSucceeded) {
          this.updated.emit();
        }
      });
  }
  
}
