import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {TranslationModule} from '../translation.module';
import {SystemAccountsComponent} from './system-accounts/system-accounts.component';
import {SystemAccountsService} from '../../services/system-accounts.service';
import {SharedService} from '../../services/shared.service';
import {ResetPasswordModalComponent} from './reset-password-modal/reset-password-modal.component';
import {EditAccountModalComponent} from './edit-account-modal/edit-account-modal.component';
import {SharedModule} from '../shared/shared.module';
import {MultiSelectModule} from 'primeng/components/multiselect/multiselect';


const routes: Routes = [
  {
    path: '', component: SystemAccountsComponent
  },
];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
    TranslationModule,
    SharedModule,
    MultiSelectModule,
    CommonModule
  ],
  declarations: [SystemAccountsComponent, EditAccountModalComponent],
  providers: [SystemAccountsService, SharedService],
  entryComponents: [EditAccountModalComponent]
})

export class SystemAccountsModule {
}
