import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {AddEditCategoryModalComponent} from '../../category/add-edit-category-modal/add-edit-category-modal.component';
import {SystemAccountsService} from '../../../services/system-accounts.service';
import {SharedService} from '../../../services/shared.service';
import {Subscription} from 'rxjs/Subscription';
import {UtilitiesService} from '../../../services/utilities.service';

@Component({
  selector: 'app-edit-account-modal',
  templateUrl: './edit-account-modal.component.html',
  styleUrls: ['./edit-account-modal.component.scss']
})
export class EditAccountModalComponent implements OnInit {
  getAllRolesSubscription: Subscription;
  roles: any[] = [];
  selectedRoles: any[] = [];
  allCountries: any[] = [];
  failed: boolean;
  selectedFileName: string;
  toggleLoading: boolean;
  passwordRegex;
  emailRegex;
  phoneRegex;
  
  constructor(public dialogRef: MatDialogRef<AddEditCategoryModalComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any, private systemAccountsService: SystemAccountsService, private sharedService: SharedService, private utilitiesService: UtilitiesService) {
  }
  
  ngOnInit() {
    console.log(this.data);
    this.getAllRoles();
    this.getAllCountries();
    if (this.data.roleNames) {
      this.data.roleNames.map((role) => {
        this.selectedRoles.push({name: role});
      });
    }
    this.passwordRegex = /^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{6,16}$/;
    
    this.emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    
    this.phoneRegex = /^[0-9]+$/;
  }
  
  saveUser() {
    this.data.roleNames = [];
    this.toggleLoading = true;
    this.data.fk_Country_Id = this.sharedService.currentUserData().fk_Country_Id;
    this.data.countryCode = this.sharedService.currentUserData().country_Code;
    this.selectedRoles.map((role) => {
      this.data.roleNames.push(role.name);
    });
    console.log(this.data);
    if (this.data.id) {
      console.log('save updated');
      this.systemAccountsService.updateAccount(this.data)
        .subscribe((res) => {
            console.log(res);
            this.closeDialog(true);
            this.toggleLoading = false;
          },
          err => {
            this.failed = true;
            this.toggleLoading = false;
          });
    } else {
      this.systemAccountsService.addNewAccount(this.data)
        .subscribe((res) => {
            console.log(res);
            this.closeDialog(true);
            this.toggleLoading = false;
          },
          err => {
            this.failed = true;
            this.toggleLoading = false;
          });
    }
  }
  
  selectImage(event) {
    console.log(event.srcElement.files[0]);
    let file = event.srcElement.files[0];
    this.utilitiesService.getBase64FromFile(file)
      .then((object: any) => {
        console.log(object);
        this.data.picture = object.fileContent;
        this.data.pictureName = object.fileName;
        this.selectedFileName = object.fileName;
      });
  }
  
  removeSelectedImage() {
    this.selectedFileName = '';
    delete this.data.picture;
    delete this.data.pictureName;
  }
  
  getAllRoles() {
    this.getAllRolesSubscription = this.sharedService.getAllRoles()
      .subscribe((res) => {
        console.log(res.data);
        let data = [];
        res.data.map((role) => {
          data.push({name: role.name});
        });
        this.roles = data;
        console.log(this.roles);
      });
  }
  
  getAllCountries() {
    this.utilitiesService.getAllCountries()
      .subscribe((data) => {
        console.log(data);
        this.allCountries = data;
      });
  }
  
  closeDialog(result = false) {
    this.dialogRef.close(result);
  }
  
}
