import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {SystemAccountsService} from '../../../services/system-accounts.service';

@Component({
  selector: 'app-reset-password-modal',
  templateUrl: './reset-password-modal.component.html',
  styleUrls: ['./reset-password-modal.component.scss']
})
export class ResetPasswordModalComponent implements OnInit {
  newPassword: string;
  failed: boolean;
  passwordRegex = /^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{6,16}$/;
  
  constructor(public dialogRef: MatDialogRef<ResetPasswordModalComponent>, @Inject(MAT_DIALOG_DATA) public data: any, private systemAccountsService: SystemAccountsService) {
  }
  
  ngOnInit() {
    console.log(this.data);
  }
  
  resetPassword() {
    let resetPasswordObjectPost = {
      newPassword: this.newPassword,
      username: this.data.userName
    };
    this.systemAccountsService.resetPassword(resetPasswordObjectPost)
      .subscribe((response) => {
          if (response.isSucceeded) {
            this.closeDialog();
          }
        },
        err => {
          this.failed = true;
        });
  }
  
  closeDialog(result = false) {
    this.dialogRef.close(result);
  }
}
