import {Component, OnDestroy, OnInit} from '@angular/core';
import {SystemAccountsService} from '../../../services/system-accounts.service';
import {Subscription} from 'rxjs/Subscription';
import {MatDialog} from '@angular/material';
// import {FormRequiredErrorComponent} from '../../shared/form-required-error/form-required-error.component';
import {ResetPasswordModalComponent} from '../reset-password-modal/reset-password-modal.component';
import {EditAccountModalComponent} from '../edit-account-modal/edit-account-modal.component';

@Component({
    selector: 'app-system-accounts',
    templateUrl: './system-accounts.component.html',
    styleUrls: ['./system-accounts.component.scss']
})

export class SystemAccountsComponent implements OnInit, OnDestroy {
    accounts: any[] = [];
    filteredAccounts: any[] = [];
    systemAccountsServiceSubscription: Subscription;
    searchText: string = '';
    toggleLoading: boolean;

    constructor(private systemAccountsService: SystemAccountsService, private dialog: MatDialog) {
    }

    ngOnInit() {
        this.getAllSystemAccounts();
    }

    getAllSystemAccounts() {
        this.toggleLoading = true;
        this.systemAccountsServiceSubscription = this.systemAccountsService.getAllSystemAccounts()
            .subscribe((res) => {
                console.log(res.data);
                this.accounts = res.data;
                this.filteredAccounts = this.accounts.slice();
                this.toggleLoading = false;
            });
    }

    resetFilter() {
        this.filteredAccounts = this.accounts.slice();
    }

    ngOnDestroy() {
        this.systemAccountsServiceSubscription.unsubscribe();
    }

    filterAccounts(text) {
        if (text.length >= 4) {
            this.filteredAccounts = [];
            this.accounts.map((account) => {
                if (account.userName.toLowerCase().startsWith(text.toLowerCase())) {
                    this.filteredAccounts.push(account);
                } else if ((account.phone1 && account.phone1.startsWith(text)) || (account.phone2 && account.phone2.startsWith(text))) {
                    this.filteredAccounts.push(account);
                } else {
                    account.roleNames.map((role) => {
                        if (role.toLowerCase().startsWith(text.toLowerCase())) {
                            this.filteredAccounts.push(account);
                        }
                    });
                }
            });
        } else {
            this.filteredAccounts = this.accounts;
        }
    }

    addNewUserModal() {
        let dialogRef = this.dialog.open(EditAccountModalComponent, {
            data: {},
            width: '500px',
            height: '500px'
        });
        dialogRef.afterClosed()
            .subscribe((result) => {
                if (result) {
                    this.getAllSystemAccounts();
                }
            });
    }

    openResetPassword(account) {
        let dialogRef = this.dialog.open(ResetPasswordModalComponent, {
            data: Object.assign({}, account),
            width: '500px',
            height: '250px'
        });
        dialogRef.afterClosed()
            .subscribe((result) => {
                if (result) {
                    this.getAllSystemAccounts();
                }
            });
    }

    editAccount(account) {
        let dialogRef = this.dialog.open(EditAccountModalComponent, {
            data: Object.assign({}, account),
            width: '500px',
            height: '500px'
        });
        dialogRef.afterClosed()
            .subscribe((result) => {
                if (result) {
                    this.getAllSystemAccounts();
                }
            });
    }

    removeAccount(accountId) {
        this.systemAccountsService.deleteByUserId(accountId)
            .subscribe(() => {
                this.getAllSystemAccounts();
            });
    }

}
