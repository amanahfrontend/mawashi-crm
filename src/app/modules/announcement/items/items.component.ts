import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { CategoryService } from '../../../services/category.service';
import { Subscription } from 'rxjs/Subscription';
import { ItemService } from '../../../services/item.service';

@Component({
  selector: 'announcement-items',
  templateUrl: './items.component.html',
  styleUrls: ['./items.component.css']
})
export class ItemsComponent implements OnInit {

  selectedCategory: any;
  selectedState: string;
  selectedType: string;
  itemsList: any[] = [];
  categoryList: any[] = [];
  categoryTypes: any[] = [];
  selectedItemState: string;
  toggleNotFoundText: boolean;
  getCategoryTypesSubscription: Subscription;

  @Output() itemId: EventEmitter<number> = new EventEmitter();
  constructor(private categoryService: CategoryService, private itemService: ItemService) {
  }

  ngOnInit() {
    this.getCategoryTypes();
  }

  getCategories() {
    if (this.selectedType == 'app') {
      if (this.selectedState == 'active') {
        this.categoryService.getAppActiveCategory()
          .subscribe((res) => {
            this.categoryList = res.data;
          });
      } else {
        this.categoryService.getAppDeactiveCategory()
          .subscribe((res) => {
            this.categoryList = res.data;
          });
      }
    } else {
      if (this.selectedState == 'active') {
        this.categoryService.getSalesActiveCategory()
          .subscribe((res) => {
            this.categoryList = res.data;
          });
      } else {
        this.categoryService.getSalesDeactiveCategory()
          .subscribe((res) => {
            this.categoryList = res.data;
          });
      }
    }
  }

  getCategoryTypes() {
    this.getCategoryTypesSubscription = this.categoryService.getCategoryTypes()
      .subscribe((res) => {
        this.categoryTypes = res.data;
      });
  }

  /**
   * 
   * @param event 
   */
  onChange(event) {
    if (this.selectedCategory) {
      this.toggleNotFoundText = true;
      if (this.selectedItemState == 'active') {
        this.itemService.getAllActiveItems(this.selectedCategory)
          .subscribe((res) => {
            this.itemsList = res.data;
          },
            err => {

            });
      } else if (this.selectedItemState == 'deactive') {
        this.itemService.getAllDeactiveItems(this.selectedCategory)
          .subscribe((res) => {
            this.itemsList = res.data;
          },
            err => {

            });
      }
    }
  }

  /**
   * 
   * @param event 
   */
  onChangeCategory(event) {
    this.selectedState = null;
  }

  /**
   * 
   * @param itemId 
   */
  onSelectItem(itemId: number) {
    this.itemId.emit(itemId);
  }
}
