import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AnnouncementService } from './../../services/announcement.service';
import { AuthService } from './../../services/auth.service';
import Swal from 'sweetalert2'
import {
  displayFieldCss,
  isFieldValid,
  validateAllFormFields
} from './../../helpers/validator';


@Component({
  selector: 'pm-announcement',
  templateUrl: './announcement.component.html',
  styleUrls: ['./announcement.component.css']
})
export class AnnouncementComponent implements OnInit {

  form: FormGroup;
  displayCategory: boolean = false;
  displaySubcategory: boolean = false;
  displayItems: boolean = false;

  favoriteType: string;

  title = new FormControl('', Validators.required);
  body = new FormControl('', Validators.required);
  option = new FormControl('', Validators.required);

  selectOptions = [
    {
      value: 'general',
      title: 'General',
    },
    {
      value: 'category',
      title: 'Category',
    },
    {
      value: 'subcategory',
      title: 'Subcategory',
    },
    {
      value: 'items',
      title: 'Items',
    },
  ];

  selectedCategory: number;
  selectedItem: number;
  selectedSubcategory: number;

  constructor(private fb: FormBuilder, private announcementService: AnnouncementService, private authService: AuthService) {
    this.form = this.fb.group({
      title: this.title,
      body: this.body,
      option: this.option
    })
  }

  ngOnInit() {
  }

  /**
   * send notification vai type [general / category / ...]
   * 
   * 
   */
  send() {
    if (this.form.invalid) {
      return;
    }

    let formBody = this.form.value;
    delete formBody['option'];
    formBody['country'] = this.authService.currentUserData().fk_Country_Id;

    switch (this.favoriteType) {
      case 'category':
        Object.assign(formBody, { Data: { categoryId: this.selectedCategory } });
        break;

      case 'items':
        Object.assign(formBody, { Data: { itemId: this.selectedItem } });
        break;

      case 'subcategory':
        Object.assign(formBody, { Data: { subcategoryId: this.selectedItem } });
        break;

      default:
        break;
    }

    this.announcementService.notify(formBody).subscribe(res => {
      this.form.reset();
      Swal.fire({
        position: 'top-end',
        type: 'success',
        title: 'Success sent',
        showConfirmButton: false,
        timer: 2000
      });
    });

  }

  /**
   * 
   * @param event 
   */
  onOptionChange(event) {
    this.favoriteType = event.value;
  }

  /**
   * 
   * @param categoryId 
   */
  onChangeCategoryId(categoryId: number) {
    this.selectedCategory = categoryId;
  }

  /**
   * 
   * @param id 
   */
  onChangeItem(id: number) {
    this.selectedItem = id;
  }

  /**
   * 
   * @param id 
   */
  onChangeSubcategoryId(id: number) {
    this.selectedSubcategory = id;
  }
}
