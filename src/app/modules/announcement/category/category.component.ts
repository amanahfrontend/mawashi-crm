import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { CategoryService } from '../../../services/category.service';
import { Subscription } from 'rxjs/Subscription';

@Component({
  selector: 'announcement-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.scss']
})

export class CategoryComponent implements OnInit {


  selectedType: any;
  selectedState: string;
  categoryTypes: any[] = [];
  categoryList: any[] = [];
  getCategoryTypesSubscription: Subscription;

  @Output() selectedCategoryId: EventEmitter<Number> = new EventEmitter();
  constructor(private categoryService: CategoryService) {
  }

  ngOnInit() {
    this.selectedState = 'active';
    this.getCategoryTypes();
  }

  showCategories(options) {
    if (options.type.toLowerCase() == 'app') {
      if (options.state.toLowerCase() == 'active') {
        this.categoryService.getAppActiveCategory()
          .subscribe((res) => {
            this.categoryList = res.data;
          });
      } else {
        this.categoryService.getAppDeactiveCategory()
          .subscribe((res) => {
            this.categoryList = res.data;
          });
      }
    } else {
      if (options.state.toLowerCase() == 'active') {
        this.categoryService.getSalesActiveCategory()
          .subscribe((res) => {
            this.categoryList = res.data;
          });
      } else {
        this.categoryService.getSalesDeactiveCategory()
          .subscribe((res) => {
            this.categoryList = res.data;
          });
      }
    }
  }

  getCategoryTypes() {
    this.getCategoryTypesSubscription = this.categoryService.getCategoryTypes()
      .subscribe((res) => {
        this.categoryTypes = res.data;
        this.categoryTypes.map((type) => {
          if (type.nameEN.toLowerCase() == 'app') {
            this.selectedType = type.nameEN;
          }
        });
        this.showCategories({ type: this.selectedType, state: this.selectedState });
      });
  }

  /**
   * 
   * @param categoryId 
   */
  onChangeCategory(categoryId: number) {
    this.selectedCategoryId.emit(categoryId)    
  }
}
