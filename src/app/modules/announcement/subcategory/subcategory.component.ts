import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { CategoryService } from '../../../services/category.service';
import { Subscription } from 'rxjs/Subscription';
import { ItemService } from '../../../services/item.service';

@Component({
  selector: 'announcement-subcategory',
  templateUrl: './subcategory.component.html',
  styleUrls: ['./subcategory.component.scss']
})

export class SubcategoryComponent implements OnInit {

  selectedCategory: any;
  selectedState: string;
  selectedType: string;
  itemsList: any[] = [];
  getCategoryTypesSubscription: Subscription;
  categoryList: any[] = [];
  subCategoryList: any[] = [];
  categoryTypes: any[] = [];
  selectedItemState: string;
  toggleNotFoundText: boolean;

  @Output() selectedSubcategoryId: EventEmitter<number> = new EventEmitter();

  constructor(private categoryService: CategoryService, private itemService: ItemService) {
  }

  ngOnInit() {
    this.getCategoryTypes();
  }


  getSubCategories() {
    if (this.selectedCategory) {
      if (this.selectedType.toLowerCase() == 'app') {
        this.categoryService.getAppActiveSubCategory(this.selectedCategory)
          .subscribe((res) => {
            this.subCategoryList = res.data;
          });
      }
      else {
        this.categoryService.getSalesActiveSubCategory(this.selectedCategory)
          .subscribe((res) => {
            this.subCategoryList = res.data;
          });

      }
    }
  }


  getCategories() {
    if (this.selectedState != undefined) {
      if (this.selectedType.toLowerCase() == 'app') {
        if (this.selectedState.toLowerCase() == 'active') {
          this.categoryService.getAppActiveCategory()
            .subscribe((res) => {
              this.categoryList = res.data;
            });
        } else {
          this.categoryService.getAppDeactiveCategory()
            .subscribe((res) => {
              this.categoryList = res.data;
            });
        }
      } else {
        if (this.selectedState.toLowerCase() == 'active') {
          this.categoryService.getSalesActiveCategory()
            .subscribe((res) => {
              this.categoryList = res.data;
            });
        } else {
          this.categoryService.getSalesDeactiveCategory()
            .subscribe((res) => {
              this.categoryList = res.data;
            });
        }

      }

    }
  }

  getCategoryTypes() {
    this.getCategoryTypesSubscription = this.categoryService.getCategoryTypes()
      .subscribe((res) => {
        this.categoryTypes = res.data;
      });
  }

  onChangeSubcategory(subcategoryId: number) {
    this.selectedSubcategoryId.emit(subcategoryId);
  }

}
