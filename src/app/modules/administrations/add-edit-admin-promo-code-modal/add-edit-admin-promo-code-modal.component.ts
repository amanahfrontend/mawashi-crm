import {Component, Inject, OnInit} from '@angular/core';
import {AddEditAdminDatesModalComponent} from '../add-edit-admin-dates-modal/add-edit-admin-dates-modal.component';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {AdministrationsService} from '../../../services/administrations.service';

@Component({
  selector: 'app-add-edit-admin-promo-code-modal',
  templateUrl: './add-edit-admin-promo-code-modal.component.html',
  styleUrls: ['./add-edit-admin-promo-code-modal.component.scss']
})
export class AddEditAdminPromoCodeModalComponent implements OnInit {
  isExisted: boolean;
  
  constructor(public dialogRef: MatDialogRef<AddEditAdminDatesModalComponent>, @Inject(MAT_DIALOG_DATA) public data: any, private administrationsService: AdministrationsService) {
  }
  
  ngOnInit() {
    this.isExisted = !!this.data.id;
  }
  
  savePromo() {
    console.log(this.data.dateFrom);
    if (this.data.startDate) {
      let startDate = new Date(this.data.startDate);
      this.data.startDate = startDate.toISOString();
    }
    if (this.data.expiryDate) {
      let expiryDate = new Date(this.data.expiryDate);
      this.data.expiryDate = expiryDate.toISOString();
    }
    if (this.isExisted) {
      this.administrationsService.editAdministrationItemData(this.data.type, this.data)
        .subscribe(() => {
          this.closeModal(true);
        });
    } else {
      this.administrationsService.addAdministrationItemData(this.data.type, this.data)
        .subscribe(() => {
          this.closeModal(true);
        });
    }
  }
  
  closeModal(res = false) {
    this.dialogRef.close(res);
  }
  
}
