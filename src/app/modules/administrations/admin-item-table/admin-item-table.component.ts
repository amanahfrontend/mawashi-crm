import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {AdministrationsService} from '../../../services/administrations.service';
import {MatDialog} from '@angular/material';
import {AddEditAdminFeesModalComponent} from '../add-edit-admin-fees-modal/add-edit-admin-fees-modal.component';

@Component({
  selector: 'app-admin-item-table',
  templateUrl: './admin-item-table.component.html',
  styleUrls: ['./admin-item-table.component.scss']
})
export class AdminItemTableComponent implements OnInit {
  @Input() items: any[] = [];
  @Input() type: string = '';
  @Output() updated = new EventEmitter();
  
  constructor(private dialog: MatDialog, private administrationsService: AdministrationsService) {
  }
  
  ngOnInit() {
  }
  
  addItem() {
    let dialogRef = this.dialog.open(AddEditAdminFeesModalComponent, {
      width: '500px',
      height: '500px',
      data: {type: this.type}
    });
    dialogRef.afterClosed()
      .subscribe((res) => {
        if (res) {
          this.updated.emit(this.type);
        }
      });
  }
  
  editItem(item) {
    item.type = this.type;
    let dialogRef = this.dialog.open(AddEditAdminFeesModalComponent, {
      width: '500px',
      height: '500px',
      data: item
    });
    dialogRef.afterClosed()
      .subscribe((res) => {
        if (res) {
          this.updated.emit(this.type);
        }
      });
  }
  
  deletePlace(id) {
    this.administrationsService.deleteAdministrationItemData(this.type, id)
      .subscribe(() => {
        this.updated.emit(this.type);
      });
  }
  
}
