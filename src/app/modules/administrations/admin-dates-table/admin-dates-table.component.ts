import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {AdministrationsService} from '../../../services/administrations.service';
import {MatDialog} from '@angular/material';
import {AddEditAdminDatesModalComponent} from '../add-edit-admin-dates-modal/add-edit-admin-dates-modal.component';

@Component({
  selector: 'app-admin-dates-table',
  templateUrl: './admin-dates-table.component.html',
  styleUrls: ['./admin-dates-table.component.scss']
})
export class AdminDatesTableComponent implements OnInit {
  @Input() items: any[] = [];
  @Input() type: any[] = [];
  @Output() updated = new EventEmitter();
  
  constructor(private administrationsService: AdministrationsService, private dialog: MatDialog) {
  }
  
  ngOnInit() {
  }
  
  openAddModal() {
    let dialogRef = this.dialog.open(AddEditAdminDatesModalComponent, {
      width: '500px',
      height: '500px',
      data: {type: this.type}
    });
    dialogRef.afterClosed()
      .subscribe((res) => {
        if (res) {
          this.updated.emit(this.type);
        }
      });
  }
  
  openEditModal(item) {
    item.type = this.type;
    let dialogRef = this.dialog.open(AddEditAdminDatesModalComponent, {
      width: '500px',
      height: '500px',
      data: Object.assign({}, item)
    });
    dialogRef.afterClosed()
      .subscribe((res) => {
        if (res) {
          this.updated.emit(this.type);
        }
      });
  }
  
  deletePlace(id) {
    this.administrationsService.deleteAdministrationItemData(this.type, id)
      .subscribe(() => {
        this.updated.emit(this.type);
      });
  }
  
}
