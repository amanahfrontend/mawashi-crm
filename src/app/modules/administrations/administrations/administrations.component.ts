import { Component, OnInit } from '@angular/core';
import { AdministrationsService } from '../../../services/administrations.service';
import { UtilitiesService } from '../../../services/utilities.service';
import { SharedService } from '../../../services/shared.service';
import { MatDialog } from '@angular/material';

@Component({
  selector: 'app-administrations',
  templateUrl: './administrations.component.html',
  styleUrls: ['./administrations.component.scss']
})

export class AdministrationsComponent implements OnInit {
  administrationsList = [
    { name: 'Charity Places' },
    { name: 'Pickup Places' },
    { name: 'Eid Dates' },
    { name: 'Pickup Dates' },
    { name: 'Donation Dates' },
    { name: 'Promo Code' },
    { name: 'Delivery Fees' },
    { name: 'Ads' },
    { name: 'Settings' },
    { name: 'Payment Method' },
    { name: 'FAQ' }

  ];
  data: any[] = [];
  toggleLoading: boolean;
  currentUserData: any;

  constructor(private administrationService: AdministrationsService, private utilitiesService: UtilitiesService, private sharedService: SharedService, private dialog: MatDialog) {
  }

  ngOnInit() {
    this.getItemData('Charity Places');
    this.currentUserData = this.sharedService.currentUserData();
  }

  getItemData(name) {
    this.toggleLoading = true;
    this.administrationService.getAdministrationItemData(name)
      .subscribe((res) => {
        this.data = res.data;
        console.log(res.data);
        if (name == 'Ads') {
          let dataArray = [];
          this.data.map((item) => {
            let name = item.slice(item.lastIndexOf('/') + 1);
            console.log(name);
            dataArray.push({ name: name, url: item });
          });
          this.data = dataArray;
        }
        console.log(this.data);
        this.toggleLoading = false;
      },
        err => {
          this.toggleLoading = false;
        });
  }

  archieveItem(item) {
    let archieveObjToPost = {
      adFileName: item.name,
      country: this.currentUserData.fk_Country_Id
    };
    console.log(archieveObjToPost);
    this.administrationService.deleteAdministrationItemData('Ads', null, archieveObjToPost)
      .subscribe(() => {
        this.getItemData('Ads');
      });
  }

  addNewAd(event) {
    this.toggleLoading = true;
    console.log(event.srcElement.files[0]);
    let file = event.srcElement.files[0];

    this.utilitiesService.getBase64FromFile(file)
      .then((object: any) => {
        console.log(object);
        let newAd = {
          country: this.currentUserData.fk_Country_Id,
          fileContent: object.fileContent
        };
        console.log(newAd);
        this.administrationService.addAdministrationItemData('Ads', newAd)
          .subscribe(() => {
            this.getItemData('Ads');
          });
      });
  }

  togglePaymentActivations(paymentMethod) {
    this.administrationService.togglePaymentMethodActivation(paymentMethod.isActivated, paymentMethod.id)
      .subscribe((res) => {
        this.getItemData('Payment Method');
      });
  }


}
