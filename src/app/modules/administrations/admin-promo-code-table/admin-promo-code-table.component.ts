import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {AdministrationsService} from '../../../services/administrations.service';
import {MatDialog} from '@angular/material';
import {AddEditAdminPromoCodeModalComponent} from '../add-edit-admin-promo-code-modal/add-edit-admin-promo-code-modal.component';

@Component({
  selector: 'app-admin-promo-code-table',
  templateUrl: './admin-promo-code-table.component.html',
  styleUrls: ['./admin-promo-code-table.component.scss']
})
export class AdminPromoCodeTableComponent implements OnInit {
  @Input() items: any[] = [];
  @Input() type: any[] = [];
  @Output() updated = new EventEmitter();
  
  constructor(private dialog: MatDialog, private administrationsService: AdministrationsService) {
  }
  
  ngOnInit() {
  }
  
  openAddModal() {
    let dialogRef = this.dialog.open(AddEditAdminPromoCodeModalComponent, {
      width: '500px',
      height: '500px',
      data: {type: this.type}
    });
    dialogRef.afterClosed()
      .subscribe((res) => {
        if (res) {
          this.updated.emit(this.type);
        }
      });
  }
  
  openEditModal(item) {
    item.type = this.type;
    let dialogRef = this.dialog.open(AddEditAdminPromoCodeModalComponent, {
      width: '500px',
      height: '500px',
      data: Object.assign({}, item)
    });
    dialogRef.afterClosed()
      .subscribe((res) => {
        if (res) {
          this.updated.emit(this.type);
        }
      });
  }
  
  deletePlace(id) {
    this.administrationsService.deleteAdministrationItemData(this.type, id)
      .subscribe(() => {
        this.updated.emit(this.type);
      });
  }
  
}
