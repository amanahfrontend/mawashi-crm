import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { AdministrationsService } from '../../../services/administrations.service';
import { SharedService } from '../../../services/shared.service';

@Component({
  selector: 'app-add-admin-faq-data',
  templateUrl: './add-admin-faq-data.component.html',
  styleUrls: ['./add-admin-faq-data.component.css']
})
export class AddAdminFaqDataComponent implements OnInit {
  isExisted: boolean;
  currentUser: any;
  constructor(public dialogRef: MatDialogRef<AddAdminFaqDataComponent>, @Inject(MAT_DIALOG_DATA) public data: any, private administrationsService: AdministrationsService, private sharedService: SharedService) {
  }
  ngOnInit() {
    this.currentUser = this.sharedService.currentUserData();
  }

  saveData() {
    // this.data.code = 'UAE';
    if (this.data.type == 'FAQ') {
      this.data.fk_Country_Id = this.currentUser.fk_Country_Id;
    }
    this.administrationsService.addAdministrationItemData(this.data.type, this.data)
      .subscribe(() => {
        this.closeModal(true);
      });

  }

  closeModal(res = false) {
    this.dialogRef.close(res);
  }
}
