import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { AdministrationsService } from '../../../services/administrations.service';
import { MatDialog } from '@angular/material';
import { AddAdminFaqDataComponent } from '../add-admin-faq-data/add-admin-faq-data.component';



@Component({
  selector: 'app-admin-faq-data',
  templateUrl: './admin-faq-data.component.html',
  styleUrls: ['./admin-faq-data.component.css']
})
export class AdminFaqDataComponent implements OnInit {
  @Input() items: any[] = [];
  @Input() type: any[] = [];
  panelOpenState = false;

  @Output() updated = new EventEmitter();
  constructor(private dialog: MatDialog, private administrationsService: AdministrationsService) {
  }
  ngOnInit() {
  }
  addItem() {
    let dialogRef = this.dialog.open(AddAdminFaqDataComponent, {
      width: '500px',
      height: '324px',
      data: { type: this.type }
    });
    dialogRef.afterClosed()
      .subscribe((res) => {
        if (res) {
          this.updated.emit(this.type);
        }
      });
  }

}
