import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {AdministrationsService} from '../../../services/administrations.service';
import {MatDialog} from '@angular/material';
import {AddEditAdminPlaceModalComponent} from '../add-edit-admin-place-modal/add-edit-admin-place-modal.component';

@Component({
  selector: 'app-admin-places-table',
  templateUrl: './admin-places-table.component.html',
  styleUrls: ['./admin-places-table.component.scss']
})

export class AdminPlacesTableComponent implements OnInit {
  @Input() items: any[] = [];
  @Input() type: any[] = [];
  @Output() updated = new EventEmitter();
  
  constructor(private administrationsService: AdministrationsService, private dialog: MatDialog) {
  }
  
  ngOnInit() {
  }
  
  openAddModal() {
    let dialogRef = this.dialog.open(AddEditAdminPlaceModalComponent, {
      width: '500px',
      height: '500px',
      data: {type: this.type}
    });
    dialogRef.afterClosed()
      .subscribe((result) => {
        if (result) {
          this.updated.emit(this.type);
        }
      });
  }
  
  openEditModal(item) {
    item.type = this.type;
    let dialogRef = this.dialog.open(AddEditAdminPlaceModalComponent, {
      width: '500px',
      height: '500px',
      data: Object.assign({}, item)
    });
    dialogRef.afterClosed()
      .subscribe((result) => {
        if (result) {
          this.updated.emit(this.type);
        }
      });
  }
  
  deletePlace(id) {
    this.administrationsService.deleteAdministrationItemData(this.type, id)
      .subscribe(() => {
        this.updated.emit(this.type);
      });
  }
  
}
