import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdministrationsComponent } from './administrations/administrations.component';
import { RouterModule, Routes } from '@angular/router';
// import {StockComponent} from '../../pages/stock/stock.component';
import { SharedModule } from '../shared/shared.module';
import { TranslationModule } from '../translation.module';
import { AdminItemTableComponent } from './admin-item-table/admin-item-table.component';
import { AdministrationsService } from '../../services/administrations.service';
import { AdminPromoCodeTableComponent } from './admin-promo-code-table/admin-promo-code-table.component';
import { AdminDatesTableComponent } from './admin-dates-table/admin-dates-table.component';
import { AdminPlacesTableComponent } from './admin-places-table/admin-places-table.component';
import { AddEditAdminPlaceModalComponent } from './add-edit-admin-place-modal/add-edit-admin-place-modal.component';
import { AddEditAdminDatesModalComponent } from './add-edit-admin-dates-modal/add-edit-admin-dates-modal.component';
import { AddEditAdminPromoCodeModalComponent } from './add-edit-admin-promo-code-modal/add-edit-admin-promo-code-modal.component';
import { AddEditAdminFeesModalComponent } from './add-edit-admin-fees-modal/add-edit-admin-fees-modal.component';

import { MatExpansionModule } from '@angular/material/expansion';
import { AddAdminFaqDataComponent } from './add-admin-faq-data/add-admin-faq-data.component';
import { AdminFaqDataComponent } from './admin-faq-data/admin-faq-data.component';



const routes: Routes = [
  { path: '', component: AdministrationsComponent },
];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
    CommonModule,
    SharedModule,
    TranslationModule,
    MatExpansionModule
  ],
  declarations: [AdministrationsComponent,
    AdminItemTableComponent, AdminPromoCodeTableComponent
    , AdminDatesTableComponent, AdminPlacesTableComponent,
    AddEditAdminPlaceModalComponent, AddEditAdminDatesModalComponent,
    AddEditAdminPromoCodeModalComponent,
    AddEditAdminFeesModalComponent,
    AdminFaqDataComponent,
    AddAdminFaqDataComponent
  ],
  providers: [AdministrationsService],
  entryComponents: [AddEditAdminPlaceModalComponent
    , AddEditAdminDatesModalComponent,
    AddEditAdminPromoCodeModalComponent,
    AddAdminFaqDataComponent,
    AddEditAdminFeesModalComponent]
})
export class AdministrationsModule {
}
