import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
// import {AddEditAdminPlaceModalComponent} from '../add-edit-admin-place-modal/add-edit-admin-place-modal.component';
import {AdministrationsService} from '../../../services/administrations.service';

@Component({
  selector: 'app-add-edit-admin-dates-modal',
  templateUrl: './add-edit-admin-dates-modal.component.html',
  styleUrls: ['./add-edit-admin-dates-modal.component.scss']
})
export class AddEditAdminDatesModalComponent implements OnInit {
  isExisted: boolean;
  
  constructor(public dialogRef: MatDialogRef<AddEditAdminDatesModalComponent>, @Inject(MAT_DIALOG_DATA) public data: any, private administrationsService: AdministrationsService) {
  }
  
  ngOnInit() {
    this.isExisted = !!this.data.id;
  }
  
  saveDate() {
    let date = new Date(this.data.date);
    this.data.date = date.toISOString();
    console.log(this.data);
    if (this.isExisted) {
      this.administrationsService.editAdministrationItemData(this.data.type, this.data)
        .subscribe(() => {
          this.closeModal(true);
        });
    } else {
      this.administrationsService.addAdministrationItemData(this.data.type, this.data)
        .subscribe(() => {
          this.closeModal(true);
        });
    }
  }
  
  closeModal(result = false) {
    this.dialogRef.close(result);
  }
  
}
