import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
// import {SharedService} from '../../../services/shared.service';
import {AdministrationsService} from '../../../services/administrations.service';

// import {AddEditCategoryModalComponent} from '../../category/add-edit-category-modal/add-edit-category-modal.component';

@Component({
  selector: 'app-add-edit-admin-place-modal',
  templateUrl: './add-edit-admin-place-modal.component.html',
  styleUrls: ['./add-edit-admin-place-modal.component.scss']
})
export class AddEditAdminPlaceModalComponent implements OnInit {
  isExisted: boolean;
  
  constructor(public dialogRef: MatDialogRef<AddEditAdminPlaceModalComponent>, @Inject(MAT_DIALOG_DATA) public data: any, private administrationsService: AdministrationsService) {
  }
  
  ngOnInit() {
    this.isExisted = !!this.data.id;
  }
  
  closeDialog(result = false) {
    this.dialogRef.close(result);
  }
  
  saveAddress(address) {
    if (this.isExisted) {
      this.administrationsService.editAdministrationItemData(this.data.type, address)
        .subscribe(() => {
          this.closeDialog(true);
        });
    } else {
      this.administrationsService.addAdministrationItemData(this.data.type, address)
        .subscribe(() => {
          this.closeDialog(true);
        });
    }
  }
  
}
