import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {AddEditAdminDatesModalComponent} from '../add-edit-admin-dates-modal/add-edit-admin-dates-modal.component';
import {AdministrationsService} from '../../../services/administrations.service';
import {SharedService} from '../../../services/shared.service';

@Component({
  selector: 'app-add-edit-admin-fees-modal',
  templateUrl: './add-edit-admin-fees-modal.component.html',
  styleUrls: ['./add-edit-admin-fees-modal.component.scss']
})
export class AddEditAdminFeesModalComponent implements OnInit {
  isExisted: boolean;
  currentUser: any;
  
  constructor(public dialogRef: MatDialogRef<AddEditAdminDatesModalComponent>, @Inject(MAT_DIALOG_DATA) public data: any, private administrationsService: AdministrationsService, private sharedService: SharedService) {
  }
  
  ngOnInit() {
    this.isExisted = !!this.data.id;
    this.currentUser = this.sharedService.currentUserData();
  }
  
  saveData() {
    // this.data.code = 'UAE';
    if (this.data.type == 'Delivery Fees') {
      this.data.country = this.currentUser.country_Code;
    }
    if (this.isExisted) {
      this.administrationsService.editAdministrationItemData(this.data.type, this.data)
        .subscribe(() => {
          this.closeModal(true);
        });
    } else {
      this.administrationsService.addAdministrationItemData(this.data.type, this.data)
        .subscribe(() => {
          this.closeModal(true);
        });
    }
  }
  
  closeModal(res = false) {
    this.dialogRef.close(res);
  }
  
}
