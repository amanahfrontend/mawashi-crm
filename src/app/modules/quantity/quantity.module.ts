import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SharedModule} from '../shared/shared.module';
import {RouterModule, Routes} from '@angular/router';
import {TranslationModule} from '../translation.module';
// import {CuttingComponent} from '../stock/cutting/cutting.component';
import {QuantityComponent} from '../stock/quantity/quantity.component';

const routes: Routes = [
  {path: '', component: QuantityComponent},
];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
    CommonModule,
    SharedModule,
    TranslationModule
  ],
  declarations: [QuantityComponent]
})
export class QuantityModule {
}
