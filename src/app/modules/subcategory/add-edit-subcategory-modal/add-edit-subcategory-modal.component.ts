import { Component, Inject, OnDestroy, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { CategoryService } from '../../../services/category.service';
import { UtilitiesService } from '../../../services/utilities.service';
import { Subscription } from 'rxjs/Subscription';

@Component({
  selector: 'app-add-edit-subcategory-modal',
  templateUrl: './add-edit-subcategory-modal.component.html',
  styleUrls: ['./add-edit-subcategory-modal.component.scss']
})
export class AddEditSubCategoryModalComponent implements OnInit, OnDestroy {
  categoryTypes: any[] = [];
  categoryConfigs: any[] = [];
  allAppParent: any[] = [];
  allSalesParent: any[] = [];


  OrderTypes: any[] = [];
  categoryTypesSubscription: Subscription;
  getAllCategoryConfigSubscription: Subscription;
  OrderTypesSubscription: Subscription;
  isCategoryImageIncluded: boolean;
  isExistedCategory: boolean;
  selectedFileName: string;
  toggleLoading: boolean;
  failed: boolean;
  showParents: boolean = false;
  isAllowed: boolean = false;;
  countyName: string;
  isSponsored: boolean = false;
  constructor(public dialogRef: MatDialogRef<AddEditSubCategoryModalComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any, private categoryService: CategoryService, private utilitiesService: UtilitiesService) {
  }

  ngOnInit() {


    let currentUser = JSON.parse(localStorage.getItem('currentUser'));
    this.countyName = currentUser.country_Code;
    if (this.countyName.toLowerCase() == "uae")
      this.isSponsored = true;
    else if (this.countyName.toLowerCase() == "kw")
      this.isSponsored = false;

    if (this.data.fK_CategoryType_Id == 1)
      this.getAllAppParent();
    else if (this.data.fK_CategoryType_Id == 2)
      this.getAllSalesParent();

    if (this.data.isParent == false && this.data.fK_CategoryType_Id > 0) {
      this.showParents = true;
      this.isAllowed = true;
    }


    if (JSON.stringify(this.data) == '{}') {
      this.isExistedCategory = false;
      this.data.isActive = false;
    } else {
      this.isExistedCategory = true;
      this.data.fK_CategoryType_Id = 1;
    }
    this.isCategoryImageIncluded = this.data.pictureURL;
    this.getCategoryTypes();
    this.getAllCategoryConfig();
    this.getAllOrderType();
  }

  ngOnDestroy() {
    this.categoryTypesSubscription.unsubscribe();
    this.OrderTypesSubscription.unsubscribe();
  }

  saveCategory() {
    if (this.data.fk_Parent_Id > 0)
      this.data.isParent = false;
    else if (this.data.fk_Parent_Id == undefined || this.data.fk_Parent_Id == null)
      this.data.isParent = true;

    this.toggleLoading = true;
    if (this.isExistedCategory) {
      this.categoryService.updateCategory(this.data)
        .subscribe((res) => {
          if (res.isSucceeded) {
            this.closeDialog(true);
          }
        });
    } else {
      this.categoryService.postNewCategory(this.data)
        .subscribe((res) => {
          console.log(res);
          this.toggleLoading = false;
          if (res.isSucceeded) {
            this.closeDialog(true);
          } else {
            this.failed = true;
          }
        });
    }
  }

  selectImage(event) {
    console.log(event.srcElement.files[0]);
    let file = event.srcElement.files[0];
    this.utilitiesService.getBase64FromFile(file)
      .then((object: any) => {
        console.log(object);
        this.data.file = {
          fileContent: object.fileContent
        };
        this.selectedFileName = object.fileName;
        this.isCategoryImageIncluded = true;
        delete this.data.pictureURL;
      });
  }

  removeSelectedImage() {
    this.selectedFileName = '';
    this.isCategoryImageIncluded = false;
    delete this.data.pictureURL;
    delete this.data.file;
  }

  getCategoryTypes() {
    this.categoryTypesSubscription = this.categoryService.getCategoryTypes()
      .subscribe((res) => {
        this.categoryTypes = res.data;
      },
        err => {

        });
  }


  getAllParent(selectedValue) {
    this.isAllowed = false;
    if (selectedValue == 1) {
      this.getAllAppParent();
    }
    if (selectedValue == 2) {
      this.getAllSalesParent();
    }
  }

  getAllAppParent() {
    this.categoryTypesSubscription = this.categoryService.getAllAppParent()
      .subscribe((res) => {
        this.allAppParent = res.data;
        this.isAllowed = true;

      },
        err => {

        });
  }
  isParentShow(parentShowData) {
    if (parentShowData == 'parent')
      this.showParents = false;
    if (parentShowData == 'sub')
      this.showParents = true;

  }
  getAllSalesParent() {
    this.categoryTypesSubscription = this.categoryService.getAllSalesParent()
      .subscribe((res) => {
        this.allAppParent = res.data;
        this.isAllowed = true;

      },
        err => {

        });
  }

  getAllCategoryConfig() {
    this.getAllCategoryConfigSubscription = this.categoryService.getAllCategoryConfig()
      .subscribe((res) => {
        console.log(res);
        this.categoryConfigs = res.data;
      });
  }

  getAllOrderType() {
    this.OrderTypesSubscription = this.categoryService.getAllOrderTypes()
      .subscribe((res) => {
        console.log(res);
        this.OrderTypes = res;
      });
  }

  closeDialog(res = false) {
    this.dialogRef.close();
  }


}
