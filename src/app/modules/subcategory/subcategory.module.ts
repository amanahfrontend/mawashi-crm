import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../shared/shared.module';
import { RouterModule, Routes } from '@angular/router';
import { TranslationModule } from '../translation.module';
// import {CategoryComponent} from '../stock/category/category.component';
import { ItemService } from '../../services/item.service';
import { MultiSelectModule } from 'primeng/components/multiselect/multiselect';
import { AddEditSubCategoryModalComponent } from './add-edit-subcategory-modal/add-edit-subcategory-modal.component';
import { SubCategoryTableComponent } from './subcategory-table/subcategory-table.component';
import { SubCategoryComponent } from '../stock/subcategory/subcategory.component';
import { MatRadioModule } from '@angular/material/radio';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';

const routes: Routes = [
  { path: '', component: SubCategoryComponent },
];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
    CommonModule,
    SharedModule,
    TranslationModule,
    MultiSelectModule,
    MatRadioModule,
    MatProgressSpinnerModule
  ],
  declarations: [SubCategoryComponent, SubCategoryTableComponent, AddEditSubCategoryModalComponent],
  providers: [ItemService],
  entryComponents: [AddEditSubCategoryModalComponent]
})
export class SubCategoryModule {
}
