import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { MatDialog } from '@angular/material';
import { AddEditSubCategoryModalComponent } from '../add-edit-subcategory-modal/add-edit-subcategory-modal.component';
import { CategoryService } from '../../../services/category.service';

@Component({
  selector: 'app-subcategory-table',
  templateUrl: './subcategory-table.component.html',
  styleUrls: ['./subcategory-table.component.scss']
})
export class SubCategoryTableComponent implements OnInit {
  @Input() itemList;
  @Input() toggleNotFoundText: boolean;
  @Output() updated = new EventEmitter();

  constructor(private dialog: MatDialog, private categoryService: CategoryService) {
  }

  ngOnInit() {
  }

  setCategoryActive(id) {
    this.categoryService.setCategoryActive(id)
      .subscribe((res) => {
        if (res.isSucceeded) {
          this.updated.emit();
        }
      });
  }

  setCategoryDeactive(id) {
    this.categoryService.setCategoryDeActive(id)
      .subscribe((res) => {
        if (res.isSucceeded) {
          this.updated.emit();
        }
      });
  }

  openAddCategoryModal() {
    console.log('open add modal');
    let dialogRef = this.dialog.open(AddEditSubCategoryModalComponent, {
      width: '500px',
      height: '600px',
      data: {}
    });
    dialogRef.afterClosed()
      .subscribe((result) => {
        if (result) {
          this.updated.emit();
        }
      });
  }

  openEditCategoryModal(category) {
    let dialogRef = this.dialog.open(AddEditSubCategoryModalComponent, {
      width: '500px',
      height: '600px',
      data: category
    });
    dialogRef.afterClosed()
      .subscribe((result) => {
        if (result) {
          this.updated.emit();
        }
      });
  }

  deleteCategory(id) {
    this.categoryService.deleteCategory(id)
      .subscribe((res) => {
        console.log('res');
        this.updated.emit();
      },
        err => {

        });
    console.log('delete');
  }

}
