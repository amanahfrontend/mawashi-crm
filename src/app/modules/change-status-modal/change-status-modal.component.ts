import {Component, OnInit, Inject} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {SharedService} from '../../services/shared.service';
import {HomeService} from '../../services/home.service';
import {OrdersService} from '../../services/ordersService';


@Component({
  selector: 'app-change-status-modal',
  templateUrl: './change-status-modal.component.html',
  styleUrls: ['./change-status-modal.component.scss']
})

export class ChangeStatusModalComponent implements OnInit {
  allStatus: any[];
  allAddress: any[];
  allDrivers: any[];
  allProducers: any[];
  selectedValue: any;
  
  constructor(public dialogRef: MatDialogRef<ChangeStatusModalComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any, private sharedService: SharedService, private homeService: HomeService, private orderService: OrdersService) {
  }
  
  ngOnInit() {
    console.log(this.data);
    if (this.data.type == 'status') {
      this.selectedValue = this.data.orderStatus.id;
      this.getAllStatus();
    } else if (this.data.type == 'address') {
      this.getAllAddress();
      this.selectedValue = this.data.fk_Location_Id;
    } else if (this.data.type == 'driver') {
      this.getAllDrivers();
      if (this.data.fK_Driver_Id) {
        this.selectedValue = this.data.fK_Driver_Id;
      } else {
        delete this.data.fK_Driver_Id;
      }
      console.log(this.selectedValue);
    } else if (this.data.type == 'producer') {
      this.getAllProducers();
      if (this.data.fK_Producer_Id) {
        this.selectedValue = this.data.fK_Producer_Id;
      }
    }
  }
  
  saveOrder() {
    if (this.data.type == 'status') {
      let changeStatusObjectToPost = {
        fK_Order_Id: this.data.id,
        fK_OrderStatus_Id: this.selectedValue
      };
      this.homeService.postChangeOrderStatus(changeStatusObjectToPost)
        .subscribe((res) => {
          console.log(res);
          if (res.isSucceeded) {
            this.closeModal(true);
          }
        });
    } else if (this.data.type == 'address') {
      let changeLocationObjectToPost = {
        fK_Order_Id: this.data.id,
        fK_Customer_Id: this.data.fK_Customer_Id,
        fK_Location_Id: this.selectedValue
      };
      this.homeService.postChangeOrderLocation(changeLocationObjectToPost)
        .subscribe((res) => {
          console.log(res);
          if (res.isSucceeded) {
            this.closeModal(true);
          }
        });
    }
    else if (this.data.type == 'driver') {
      let driverName;
      this.allDrivers.map((driver) => {
        console.log(driver.name);
        console.log(this.selectedValue);
        if (driver.id == this.selectedValue) {
          driverName = driver.userName;
        }
      });
      let assignDriverObjectToPost = {
        fK_Order_Id: this.data.id,
        fK_Driver_Id: this.selectedValue,
        driverName: driverName
      };
      console.log(assignDriverObjectToPost);
      this.orderService.assignOrderToDriver(assignDriverObjectToPost)
        .subscribe((res) => {
          console.log(res);
          if (res.isSucceeded) {
            this.closeModal(true);
          }
        });
    }
    if (this.data.type == 'producer') {
      let producerName;
      this.allProducers.map((producer) => {
        console.log(producer.name);
        console.log(this.selectedValue);
        if (producer.id == this.selectedValue) {
          producerName = producer.userName;
        }
      });
      let assignOrderToProducerObj = {
        fK_Order_Id: this.data.id,
        fK_Producer_Id: this.selectedValue,
        producerName: producerName
      };
      this.homeService.postAssignOrderToProducer(assignOrderToProducerObj)
        .subscribe((res) => {
          console.log(res);
          if (res.isSucceeded) {
            this.closeModal(true);
          }
        });
    }
    
  }
  
  getAllStatus() {
    this.sharedService.getAllOrderStatus()
      .subscribe((res) => {
        console.log(res);
        this.allStatus = res.data;
      });
  }
  
  getAllAddress() {
    if (this.data.orderType == 1) {
      this.orderService.getAccountAddresses(this.data.fK_Customer_Id)
        .subscribe((res) => {
          console.log(res);
          this.allAddress = res.data;
        });
    } else {
      this.orderService.getCompanyAddress(this.data.fK_Customer_Id)
        .subscribe((res) => {
          console.log(res);
          this.allAddress = res.data;
        });
    }
  }
  
  getAllDrivers() {
    this.sharedService.getAllDrivers()
      .subscribe((res) => {
        console.log(res);
        this.allDrivers = res.data;
      });
  }
  
  getAllProducers() {
    this.sharedService.getAllProducers()
      .subscribe((res) => {
        console.log(res.data);
        this.allProducers = res.data;
      });
  }
  
  closeModal(result = false) {
    this.dialogRef.close(result);
  }
}
