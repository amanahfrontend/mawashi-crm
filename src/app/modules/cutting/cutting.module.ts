import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SharedModule} from '../shared/shared.module';
import {RouterModule, Routes} from '@angular/router';
import {TranslationModule} from '../translation.module';
// import {ItemComponent} from '../stock/item/item.component';
import {CuttingComponent} from '../stock/cutting/cutting.component';
// import {CuttingService} from '../../services/cutting.service';

const routes: Routes = [
  {path: '', component: CuttingComponent},
];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
    CommonModule,
    SharedModule,
    TranslationModule
  ],
  declarations: [CuttingComponent]
})
export class CuttingModule {
}
