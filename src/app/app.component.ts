import {Component, OnInit, ViewChild, AfterViewInit, DoCheck} from '@angular/core';
import {Location, PopStateEvent} from '@angular/common';
import 'rxjs/add/operator/filter';
import {Router, NavigationEnd, NavigationStart, ActivatedRoute} from '@angular/router';
import {Subscription} from 'rxjs/Subscription';
import PerfectScrollbar from 'perfect-scrollbar';
import {TranslateService} from '@ngx-translate/core';
import {AuthService} from './services/auth.service';
import {SharedService} from './services/shared.service';
import {ResetPasswordModalComponent} from './modules/system-accounts/reset-password-modal/reset-password-modal.component';
import {MatDialog} from '@angular/material';

declare const $: any;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html'
})

export class AppComponent implements OnInit, DoCheck {
  toggleSideNav: boolean;
 
  constructor( private _authService: AuthService) {

  }
  
  ngOnInit() {
  }

  
  ngDoCheck() {
    this.toggleSideNav = this._authService.isAuth.value;
  }
  
  
  

}
